﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork
{
    class SettingsCls
    {
        public bool isSaved = true;

        //All Settings
        //*****************************************
        //*****************************************
        private int refreshFrequency = -1;        
        private int deletedOrderDuration = -1;
        private int updatedOrderDuration = -1;
        private int orangeWarning = -1;
        private int redWarning = -1;

        public int GetRefreshFrequency()
        {
            return refreshFrequency;
        }
        public void SetRefreshFrequency(int seconds)
        {
            refreshFrequency = seconds;
            isSaved = false;
        }

        public int GetDeletedOrderDuration()
        {
            return deletedOrderDuration;
        }
        public void SetDeletedOrderDuration(int seconds)
        {
            deletedOrderDuration = seconds;
            isSaved = false;
        }


        public int GetUpdatedOrderDuration()
        {
            return updatedOrderDuration;
        }
        public void SetUpdatedOrderDuration(int seconds)
        {
            updatedOrderDuration = seconds;
            isSaved = false;
        }

        public int GetOrangeWarning()
        {
            return orangeWarning;
        }
        public void SetOrangeWarning(int seconds)
        {
            orangeWarning = seconds;
            isSaved = false;
        }

        public int GetRedWarning()
        {
            return redWarning;
        }
        public void SetRedWarning(int seconds)
        {
            redWarning = seconds;
            isSaved = false;
        }

        public bool AreSettingsSaved()
        {
            if (isSaved)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ResetSettingsCls()
        {
            refreshFrequency = -1;
            deletedOrderDuration = -1;
            updatedOrderDuration = -1;
            orangeWarning = -1;
            redWarning = -1;

            isSaved = true;
        }
    }
}
