﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork
{
    class Order
    {
        public int orderID { get; }
        public int orderNumber { get; }
        public DateTime orderDate { get; }
        public bool isDisabled { get; }
        public bool isUpdated { get; set; }
        public string note { get; set; }
        public List<OrderItem> items { get; }
        public FlowLayoutPanel flpOrder { get; set; }
        public FlowLayoutPanel flpOrderItems { get; set; }


        public Order (int orderID_ , int orderNumber_, DateTime orderDate_, List<OrderItem> items_ ,bool isDisabled_ ,  bool isUpdated_) //For adding to orderQueue
        {
            orderID = orderID_;

            orderNumber = orderNumber_;

            orderDate = orderDate_;

            items = items_;

            isDisabled = isDisabled_;

            isUpdated = isUpdated_;
        }

        public Order (int orderID_, DateTime orderDate_, bool isDisabled_, bool isUpdated_)
        {
            orderID = orderID_;

            orderDate = orderDate_;

            isDisabled = isDisabled_;

            isUpdated = isUpdated_;
        }
    }
}
