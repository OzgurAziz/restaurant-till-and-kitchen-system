﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;

namespace CourseWork
{
    public partial class frmOrders : Form
    {
        Queue<Order> orderQueue = new Queue<Order>();

        //The Dictionary which has the ID as key and it's name and the price as the value
        Dictionary<int, Tuple<string, double>> MenuItemsAndPrices = new Dictionary<int, Tuple<string, double>>();

        //List of IDs of the hidden Orders
        List<int> hiddenOrderIDs = new List<int>();

        //List of IDs of the shown Orders
        List<int> shownOrderIDs = new List<int>();

        //List of IDs of shown Orders that are SELECTED BY THE USER (not all shown orderIDs)
        List<int> shownOrderIDsSelected = new List<int>();

        //List of orders to check if we should update the kitchen view
        List<Order> ordersToCheck = new List<Order>();

        //List of orders that are disabled and going to be deleted in couple more ticks (timer ticks)
        List<Order> ordersToDelete = new List<Order>();

        //List of orders that are updated (graphically shown green) that are supposed to go back to normal color after some ticks
        List<Order> ordersToChangeToNormal = new List<Order>();

        public frmOrders()
        {
            this.WindowState = FormWindowState.Maximized;
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;

            InitializeComponent();
        }        

        private void frmOrders_Load(object sender, EventArgs e)
        {            
            if (Properties.Settings.Default.timerValue * 1000 != 0)
            {
                tmrOrders.Interval = Properties.Settings.Default.timerValue * 1000;
            }
            else
            {
                tmrOrders.Interval = 5000;
            }
            

            this.Anchor = AnchorStyles.None;

            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            dbConnector.Connect();
            string sqlStr = "SELECT menuItemID, Name, Cost" +
                            " FROM[Menu Items]" +
                            " WHERE(isDeleted = false)";

            dr = dbConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                MenuItemsAndPrices.Add(Convert.ToInt32(dr[0]), Tuple.Create(dr[1].ToString(), Convert.ToDouble(dr[2])));
            }

            InitializeOrderQueue();
                     
            AddCompleteButtonToOrders();
            CheckIfEmpty();
        }

        private int GetOrderNumberFromOrderDate(DateTime orderDate)
        {
            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "SELECT COUNT(*)" +
                            " FROM Orders" +
                            " WHERE(DateValue(orderDate) = date())" +
                            " AND(isSaved = False)" +
                            " AND(TimeValue(orderDate) < '" + orderDate.TimeOfDay + "' )";

            int numberOfOrders = -1;
            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                numberOfOrders = Convert.ToInt32(dr[0]) + 1;
            }
            dBConnector.Close();

            return numberOfOrders;
        }        

        private void HideOrders()
        {
            for (int i = 0; i < orderQueue.Count; i++)
            {
                //Check if the orderItems should be HIDDEN
                if (hiddenOrderIDs.Contains(orderQueue.ElementAt(i).orderID))
                {
                    HideOrder(orderQueue.ElementAt(i).orderNumber);
                }
                else
                {
                    ShowOrder(orderQueue.ElementAt(i).orderNumber , false);
                }
            }            
        }

        private void ResizeIfShown(FlowLayoutPanel order, Label orderNumber, Label timerOrder , FlowLayoutPanel flpOrderItems)
        {
            orderNumber.Size = new Size(flpOrderItems.Width, 50); //getting the width here
            orderNumber.TextAlign = ContentAlignment.MiddleLeft;
            order.Controls.Add(orderNumber); //then adding the orderNumber

            timerOrder.Size = new Size(flpOrderItems.Width, 50);
            timerOrder.TextAlign = ContentAlignment.MiddleLeft;
            timerOrder.Dock = DockStyle.Bottom;
        }

        private void ResizeIfHidden(FlowLayoutPanel order , Label orderNumber , Label timerOrder)
        {
            //resizes the order
            if (timerOrder.Text == "CANCELLED")
            {
                orderNumber.Size = new Size(290, 50);                
            }
            else
            {
                orderNumber.Size = new Size(250, 50);
            }
            orderNumber.TextAlign = ContentAlignment.MiddleCenter;
            order.Controls.Add(orderNumber);

            timerOrder.Size = new Size(orderNumber.Width, 50);
            timerOrder.Dock = DockStyle.Bottom;
            timerOrder.TextAlign = ContentAlignment.MiddleCenter;
        }

        private Order DetermineIfOrderShouldBeDeleted(object sender)
        {
            Order orderToDelete = null;            
            try
            {
                orderToDelete = ordersToDelete.Where(x => x.flpOrder == ((sender as Label).Parent as FlowLayoutPanel)).First();
            }
            catch (Exception)
            {
                
            }

            return orderToDelete;
        }

        private Order DetermineIfOrderShouldBeBackToNormal(object sender)
        {
            Order orderToGoBackToNormal = null;
            try
            {
                int desiredOrderNumber = Convert.ToInt32(((sender as Label).Parent as FlowLayoutPanel).Controls.Find("orderNumber", false).First().Text.Replace("Order", ""));
                orderToGoBackToNormal = ordersToChangeToNormal.Where(x => x.orderNumber == desiredOrderNumber).First();
            }
            catch (Exception)
            {

            }

            return orderToGoBackToNormal;
        }

        private void OrderNumber_Click(object sender, EventArgs e)                                                                    
        {
            //Hides the Order or Shows

            Order orderToDelete = null;
            Order orderToGoBackToNormal = null;

            if (Properties.Settings.Default.timerValueForDelete == 0)
            {
                orderToDelete = DetermineIfOrderShouldBeDeleted(sender);
            }
            if (Properties.Settings.Default.timerValueForUpdate == 0)
            {
                orderToGoBackToNormal = DetermineIfOrderShouldBeBackToNormal(sender);
            }                               

            if (orderToDelete != null) //so If order is supposed to be deleted
            {
                if (Properties.Settings.Default.timerValueForDelete == 0)
                {
                    DeleteOrder(ordersToDelete.IndexOf(orderToDelete));
                }                
            }
            else if (orderToGoBackToNormal != null)
            {
                if (Properties.Settings.Default.timerValueForUpdate == 0)
                {
                    ChangeOrderBackToNormal(ordersToChangeToNormal.IndexOf(orderToGoBackToNormal));
                }                
            }
            else //Hide or show order 
            {
                int orderID = Convert.ToInt32(((sender as Label).Parent as FlowLayoutPanel).Name.Replace("flpOrder_", ""));
                int orderNumber = orderQueue.Where(x => x.orderID == orderID).First().orderNumber;

                if (hiddenOrderIDs.Contains(orderID))
                {
                    hiddenOrderIDs.Remove(orderID);

                    shownOrderIDsSelected.Add(orderID);

                    ShowOrder(orderNumber, true);
                }
                else
                {
                    hiddenOrderIDs.Add(orderID);

                    if (shownOrderIDs.Contains(orderID))
                    {
                        shownOrderIDs.Remove(orderID);
                    }
                    else
                    {
                        shownOrderIDsSelected.Remove(orderID);
                    }

                    HideOrder(orderNumber);
                }
            }             
        }

        private void ShowOrder(int orderNumber, bool Clicked)
        {
            //bool Clicked should be true if clicked on the order to show it
            //should be false if the order wasn't clicked
            FlowLayoutPanel flpOrder = null;
            FlowLayoutPanel flpOrderItems = null;
            foreach (Order order in orderQueue)
            {
                if (order.orderNumber == orderNumber)
                {
                    flpOrder = order.flpOrder;
                    flpOrderItems = order.flpOrderItems;
                    break;
                }
            }

            flpOrderItems.Show();

            if (!flpMain.Controls.Contains(flpOrder))
            {
                flpHiddenOrders.Controls.Remove(flpOrder);
                flpMain.Controls.Add(flpOrder);
                SortFlowLayoutPanel(flpOrder, orderNumber, flpMain);                                
            }

            ResizeIfShown(flpOrder, flpOrder.Controls["orderNumber"] as Label,
                              flpOrder.Controls["timerOrder"] as Label,
                              flpOrderItems);
        }

        private void HideOrder(int orderNumber)
        {          
            FlowLayoutPanel flpOrder = null;
            FlowLayoutPanel flpOrderItems = null;
            foreach (Order order in orderQueue)
            {
                if (order.orderNumber == orderNumber)
                {
                    flpOrder = order.flpOrder;
                    flpOrderItems = order.flpOrderItems;
                    break;
                }
            }
            flpOrderItems.Hide();

            if (flpMain.Controls.Contains(flpOrder))
            {
                flpMain.Controls.Remove(flpOrder);
                flpHiddenOrders.Controls.Add(flpOrder);
                SortFlowLayoutPanel(flpOrder, orderNumber, flpHiddenOrders);                
            }

            ResizeIfHidden(flpOrder, flpOrder.Controls["orderNumber"] as Label,
                                     flpOrder.Controls["timerOrder"] as Label);
        }

        private void SortFlowLayoutPanel(Control controlAdded ,int orderNumber , FlowLayoutPanel flpToSort)
        {
            List<Control> controlList = new List<Control>(); //Storing controls which are supposed to be after our added control
            controlList.Add(controlAdded);

            for (int i = 0; i < flpToSort.Controls.Count; i++)
            {
                int currentItemsOrderNumber = Convert.ToInt32(Regex.Match(flpToSort.Controls[i].Controls["orderNumber"].Text, @"(\d)+").Value);
                if (currentItemsOrderNumber > orderNumber)
                {
                    controlList.Add(flpToSort.Controls[i]);                    
                }
            }
           
            for (int i = 0; i < controlList.Count; i++)
            {
                flpToSort.Controls.Remove(controlList[i]);
                flpToSort.Controls.Add(controlList[i]);
            }                
        }        

        private int CalculateTheTimePast(int orderIndex)
        {
            TimeSpan timeDifference = DateTime.Now.Subtract(Convert.ToDateTime(orderQueue.ToList()[orderIndex].orderDate));

            int minsPast = Convert.ToInt32(Math.Floor(timeDifference.TotalMinutes));

            return minsPast;
        }

        private void InitializeOrderQueue()
        {
            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "SELECT orderID, orderDate, isDisabled, isUpdated, [note]" + //Add note as a custom control that resized itself depending on the length of the note
                            " FROM Orders" +
                            " WHERE(isComplete = False) AND(DateValue(orderDate) = date())" +
                            " AND(isSaved = False)" +
                            " ORDER BY orderID";

            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                int orderID = Convert.ToInt32(dr[0]);
                DateTime orderDate = Convert.ToDateTime(dr[1]);
                bool isDisabled = Convert.ToBoolean(dr[2]);
                bool isUpdated = Convert.ToBoolean(dr[3]);
                string note = dr[4].ToString();
                note = note.Replace("@!@", "\n");               

                Order newOrder = GetOrderItems(orderID, orderDate, isDisabled, isUpdated);
                newOrder.note = note;
                orderQueue.Enqueue(newOrder);
            }
            dBConnector.Close();

            UpdateKitchenView();
        }

        private void UpdateOrderNote(Order orderToAddNoteTo)
        {
            Label lblNote = new Label();
            lblNote.Font = new Font("Microsoft Sans Serif", 14);
            lblNote.AutoSize = true;            
            lblNote.MinimumSize = new Size(300, 0);
            lblNote.MaximumSize = new Size(300, 0);
            lblNote.BorderStyle = BorderStyle.FixedSingle;
            lblNote.Text = "NOTE: " + orderToAddNoteTo.note;
            lblNote.Name = "lblNote_" + orderToAddNoteTo.orderID;
            lblNote.Margin = new Padding(4, 3, 4, 3);

            orderToAddNoteTo.flpOrderItems.Controls.Add(lblNote);
        }

        private void UpdateOrderQueue()
        {
            for (int i = 0; i < ordersToCheck.Count; i++)
            {
                bool orderExists = false;
                int existingOrderIndex = -1;
                foreach (Order order in orderQueue)
                {
                    if (order.orderID == ordersToCheck[i].orderID)
                    {
                        existingOrderIndex = orderQueue.ToList().IndexOf(order);
                        orderExists = true;
                        break;
                    }
                }

                //if orderExists and the record of the order has been changed (isDisabled or isUpdated) than we update our orderQueue with the new parameters
                if (orderExists == true)
                {
                    if (orderQueue.ToList()[existingOrderIndex].isDisabled != ordersToCheck[i].isDisabled || orderQueue.ToList()[existingOrderIndex].isUpdated != ordersToCheck[i].isUpdated)
                    {
                        Order newOrder = GetOrderItems(orderQueue.ToList()[existingOrderIndex].orderID, orderQueue.ToList()[existingOrderIndex].orderDate,
                                                       ordersToCheck[i].isDisabled, ordersToCheck[i].isUpdated);                        

                        newOrder.flpOrder = orderQueue.ToList()[existingOrderIndex].flpOrder;
                        newOrder.flpOrderItems = orderQueue.ToList()[existingOrderIndex].flpOrderItems;
                        //Change the note only if they are different
                        if (ordersToCheck[i].note != orderQueue.ToList()[existingOrderIndex].note && ordersToCheck[i].note != "")
                        {
                            newOrder.note = ordersToCheck[i].note;
                        }
                        else
                        {
                            newOrder.note = orderQueue.ToList()[existingOrderIndex].note;
                        }                        
                        newOrder.flpOrder.Tag = 0;

                        orderQueue = RemoveItemAtIndex(orderQueue, existingOrderIndex);
                        orderQueue.Enqueue(newOrder);
                    }                    
                }
                else 
                {
                    Order newOrder = GetOrderItems(ordersToCheck[i].orderID, ordersToCheck[i].orderDate, 
                                                   ordersToCheck[i].isDisabled, ordersToCheck[i].isUpdated);

                    newOrder.note = ordersToCheck[i].note;                                     

                    orderQueue.Enqueue(newOrder);                    
                }                                                             
            }

            ordersToCheck.Clear();

            UpdateKitchenView();
        }

        private Queue<Order> RemoveItemAtIndex(Queue<Order> queue , int index)
        {
            List<Order> tempList = new List<Order>();
            tempList = queue.ToList();

            Queue<Order> newQueue = new Queue<Order>();

            for (int i = 0; i < tempList.Count; i++)
            {
                if (i != index)
                {
                    newQueue.Enqueue(tempList[i]);
                }
            }

            return newQueue;
        }

        private Order GetOrderItems(int orderID , DateTime orderDate , bool isDisabled , bool isUpdated)
        {
            clsDBConnector dBConnector = new clsDBConnector();

            List<OrderItem> orderItems = new List<OrderItem>();

            OleDbDataReader dr2;

            string sqlStr2 = "SELECT [Order Items].orderItemID, [Order Items].menuItemID, [Order Items].Quantity" +
                     " FROM(Orders INNER JOIN" +
                     " [Order Items] ON Orders.orderID = [Order Items].orderID)" +
                     " WHERE([Order Items].orderID = " + orderID + ")" + //Requesting orderItems with the currnet Order ID
                     " ORDER BY[Order Items].orderItemID";
            dBConnector.Connect();
            dr2 = dBConnector.DoSQL(sqlStr2);

            while (dr2.Read())
            {
                List<string> toppings = new List<string>();

                OleDbDataReader dr3;

                string sqlStr3 = "SELECT menuItemID" +
                                 " FROM Toppings" +
                                 " WHERE(orderItemID = " + dr2[0].ToString() + ")"; //Requesting toppings with the current order Item ID

                dr3 = dBConnector.DoSQL(sqlStr3);

                while (dr3.Read())
                {
                    toppings.Add(MenuItemsAndPrices[Convert.ToInt32(dr3[0])].Item1);
                }

                OrderItem newOrderItem = new OrderItem(Convert.ToInt32(dr2[0]), MenuItemsAndPrices[Convert.ToInt32(dr2[1])].Item1, toppings, Convert.ToInt32(dr2[2]));

                orderItems.Add(newOrderItem);
            }
            dBConnector.Close();

            Order newOrder = new Order(Convert.ToInt32(orderID) , GetOrderNumberFromOrderDate(orderDate), Convert.ToDateTime(orderDate), orderItems, isDisabled, isUpdated);

            return newOrder;
        }

        private void UpdateKitchenView()
        {                        
            for (int i = 1; i < orderQueue.Count + 1; i++)
            {
                if (!hiddenOrderIDs.Contains(orderQueue.ToList()[i - 1].orderID) &&
                    !shownOrderIDs.Contains(orderQueue.ToList()[i - 1].orderID) &&
                    !shownOrderIDsSelected.Contains(orderQueue.ToList()[i - 1].orderID))
                {                    
                    FlowLayoutPanel order = new FlowLayoutPanel();
                    order.Parent = flpMain;
                    order.Tag = 0; //Used to count ticks if updated or cancelled
                    order.Name = "flpOrder_" + orderQueue.ToList()[i - 1].orderID;
                    order.Margin = new Padding(5, 5, 0, 5);
                    order.AutoSize = true;
                    order.FlowDirection = FlowDirection.BottomUp;

                    //FlowLayoutPanel
                    FlowLayoutPanel flpOrderItems = new FlowLayoutPanel();
                    flpOrderItems.Name = "flpOrderItems";
                    flpOrderItems.Margin = new Padding(5, 2, 5, 2);
                    flpOrderItems.BackColor = Color.LightGray;
                    flpOrderItems.BorderStyle = BorderStyle.FixedSingle;                    
                    flpOrderItems.FlowDirection = FlowDirection.TopDown;
                    flpOrderItems.AutoSize = true;
                    flpOrderItems.MaximumSize = new Size(0, (flpMain.Height - 120));

                    //Order Number
                    Label orderNumber = new Label();
                    orderNumber.Name = "orderNumber";
                    orderNumber.Text = "Order " + orderQueue.ToList()[i - 1].orderNumber;
                    orderNumber.Parent = order;
                    orderNumber.BackColor = Color.Gray;
                    orderNumber.Margin = new Padding(5, 5, 5, 3);
                    orderNumber.Size = new Size(306, 50);
                    orderNumber.Font = new Font("Microsoft Sans Serif", 36);
                    orderNumber.Click += OrderNumber_Click;

                    //Timer
                    Label timerOrder = new Label();
                    timerOrder.Name = "timerOrder";
                    timerOrder.TabIndex = 0; //So that this is found quickest when the timer needs to be updated
                    timerOrder.Parent = order;
                    timerOrder.BackColor = Color.LightGray;
                    timerOrder.Margin = new Padding(5, 3, 5, 5);
                    timerOrder.Font = new Font("Microsoft Sans Serif", 32);
                    int mins = CalculateTheTimePast(i - 1); //Because the for loop starts with 1  
                    timerOrder.Text = mins + " Mins";

                    //Listing The Items
                    flpOrderItems = DisplayItems(orderQueue.ElementAt(i - 1), flpOrderItems);                    

                    order.Controls.Add(flpOrderItems);

                    flpMain.Controls.Add(order); //order flowlayoutpanel goes in first so that I can get the width of the order    
                    if (orderQueue.Count > 5)
                    {
                        SortFlowLayoutPanel(order, orderQueue.ToList()[i - 1].orderNumber, flpHiddenOrders);
                    }
                    else
                    {
                        SortFlowLayoutPanel(order, orderQueue.ToList()[i - 1].orderNumber, flpMain);
                    }

                    shownOrderIDs.Add(orderQueue.ToList()[i - 1].orderID);

                    orderQueue.ToList()[i - 1].flpOrderItems = flpOrderItems;
                    orderQueue.ToList()[i - 1].flpOrder = order;

                    //Note
                    if (orderQueue.ToList()[i - 1].note != "")
                    {
                        UpdateOrderNote(orderQueue.ToList()[i - 1]);
                    }

                    //Automatically hides more than 6th orders if not selected by the user
                    if (i > 5)
                    {
                        int currentOrderID = orderQueue.ElementAt(i - 1).orderID;

                        if (!shownOrderIDsSelected.Contains(currentOrderID) && !hiddenOrderIDs.Contains(currentOrderID))
                        {
                            hiddenOrderIDs.Add(currentOrderID);
                            shownOrderIDs.Remove(currentOrderID);
                        }
                    }
                }
                else
                {                    
                    //Update the time of orders which are already in the kitchen view
                    if (!orderQueue.ToList()[i - 1].isDisabled)
                    {
                        Label lblTimeOfOrder = orderQueue.ToList()[i - 1].flpOrder.Controls.Find("timerOrder", false).First() as Label;
                        int mins = CalculateTheTimePast(i - 1); //Because the for loop starts with 1
                        lblTimeOfOrder.Text = mins + " Mins";
                    }
                                                 
                }

                if (orderQueue.ToList()[i - 1].isDisabled) //if Cancelled
                {
                    if (Convert.ToInt32(orderQueue.ToList()[i - 1].flpOrder.Tag) == 0)
                    {
                        ChangeColorsOfOrder(orderQueue.ToList()[i - 1], ColorTranslator.FromHtml("#FF7676"), ColorTranslator.FromHtml("#FF9A9A"),
                                        ColorTranslator.FromHtml("#FFAEAE"), ColorTranslator.FromHtml("#C72324"), ColorTranslator.FromHtml("#F03C3D"));                        

                        Label label = orderQueue.ToList()[i - 1].flpOrder.Controls.Find("timerOrder", false).First() as Label;
                        label.Text = "CANCELLED";
                    }

                    if (ordersToDelete.Contains(orderQueue.ToList()[i - 1]) == false)
                    {
                        ordersToDelete.Add(orderQueue.ToList()[i - 1]);
                    }                    
                }
                else if (orderQueue.ToList()[i - 1].isUpdated && !orderQueue.ToList()[i - 1].isDisabled) //if Updated
                {
                    if (Convert.ToInt32(orderQueue.ToList()[i - 1].flpOrder.Tag) == 0) //Only update the whole thing if it hasn't been already updated
                    {
                        RenewUpdatedOrder(orderQueue.ToList()[i - 1]);                        

                        ChangeColorsOfOrder(orderQueue.ToList()[i - 1], ColorTranslator.FromHtml("#75FF7E"), ColorTranslator.FromHtml("#99FF9C"),
                               ColorTranslator.FromHtml("#ADFFB6"), ColorTranslator.FromHtml("#2EBF3C"), ColorTranslator.FromHtml("#48CF51"));

                        Label timeLabel = orderQueue.ToList()[i - 1].flpOrder.Controls.Find("timerOrder", false).First() as Label;
                        timeLabel.Text = "UPDATED";
                    }                    

                    if (!ordersToChangeToNormal.Contains(orderQueue.ToList()[i - 1]))
                    {
                        ordersToChangeToNormal.Add(orderQueue.ToList()[i - 1]);
                    }                    
                }
                else
                {
                    ChangeColorsOfOrder(orderQueue.ToList()[i - 1], DefaultBackColor, Color.LightGray, Color.AliceBlue, Color.Gray, Color.LightGray);
                }                
            }


            //Making the order Orange or Red
            ChangeOrderTimerColor();

            HideOrders();
        }

        private void ChangeOrderTimerColor()
        {
            foreach (Order order in orderQueue.ToList())
            {
                if (!ordersToChangeToNormal.Contains(order) && !ordersToDelete.Contains(order))
                {
                    Label timeLabel = order.flpOrder.Controls.Find("timerOrder", false).First() as Label;
                    int minutesPast = Convert.ToInt32(Regex.Match(timeLabel.Text, @"(\d)+").Value);

                    if (minutesPast >= Properties.Settings.Default.orangeWarningValue && minutesPast < Properties.Settings.Default.redWarningValue)
                    {
                        timeLabel.BackColor = Color.Orange;
                    }

                    if (minutesPast >= Properties.Settings.Default.redWarningValue)
                    {
                        timeLabel.BackColor = Color.Red;
                    }
                }
            }
        }

        private FlowLayoutPanel DisplayItems(Order currentOrder , FlowLayoutPanel flpOrderItems)
        {            
            for (int a = 0; a < currentOrder.items.Count; a++)
            {
                FlowLayoutPanel item = new FlowLayoutPanel();
                item.Name = "flpItem";
                item.Margin = new Padding(4, 3, 4, 3);
                item.BackColor = Color.AliceBlue;
                item.BorderStyle = BorderStyle.FixedSingle;
                item.FlowDirection = FlowDirection.TopDown;
                item.MinimumSize = new Size(300, 0); 
                item.MaximumSize = new Size(300, 0);
                item.AutoSize = true;
                item.Anchor = AnchorStyles.Left;                

                //Menu Item
                Label line = new Label();
                line.Margin = new Padding(0, 2, 0, 2);
                //line.BackColor = Color.Yellow;
                line.Text = currentOrder.items[a].quantity + "x " + (currentOrder.items[a]).menuItem;
                line.Font = new Font("Microsoft Sans Serif", 20);
                line.AutoSize = true;
                line.MinimumSize = new Size(300, 0);
                line.MaximumSize = new Size(300, 0);

                item.Controls.Add(line);

                //Toppings
                List<string> toppingsList = currentOrder.items[a].toppings;
                if (toppingsList.Count != 0)
                {
                    for (int b = 0; b < toppingsList.Count; b++)
                    {
                        Label topping = new Label();
                        topping.Parent = line;
                        topping.Margin = new Padding(2, 2, 2, 2);
                        //topping.BackColor = Color.Green;
                        topping.Size = new Size(300, 20);
                        topping.Font = new Font("Microsoft Sans Serif", 12);
                        topping.Text = "+ " + toppingsList[b];

                        item.Controls.Add(topping);
                    }
                }

                flpOrderItems.Controls.Add(item);
            }

            return flpOrderItems;
        }

        private void RenewUpdatedOrder(Order order)
        {
            //DELETE EVERY ITEM
            FlowLayoutPanel flpOrderItems = order.flpOrder.Controls.Find("flpOrderItems", false).First() as FlowLayoutPanel;

            flpOrderItems.Controls.Clear();            

            //ADD THE NEW ONES            
            flpOrderItems = DisplayItems(order, flpOrderItems);
            if (order.note != null && order.note != "")
            {
                UpdateOrderNote(order);
            }            
        }

        private void ChangeColorsOfOrder(Order order , Color flpOrderColor , Color flpOrderItemsColor , Color orderItemsColor , Color orderNumberColor , Color orderTimeColor)
        {
            order.flpOrder.BackColor = flpOrderColor;
            //order.flpOrder.BorderStyle = BorderStyle.FixedSingle;
            order.flpOrderItems.BackColor = flpOrderItemsColor;
            foreach (Control control in order.flpOrderItems.Controls)
            {
                if (control is Label && control.Name.Contains("lblNote_"))
                {
                    control.BackColor = Color.DarkGray;
                }
                else if (control is Label && control.Name == "lblComplete")
                {
                    control.BackColor = Color.LightCoral;
                }
                else
                {
                    control.BackColor = orderItemsColor;
                }                
            }

            order.flpOrder.Controls.Find("orderNumber", false).First().BackColor = orderNumberColor;
            Label label = order.flpOrder.Controls.Find("timerOrder", false).First() as Label;
            label.BackColor = orderTimeColor;            
        }
             
        private void CheckOrders()
        {
            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;
            string sqlStr = "SELECT orderID, orderDate, isDisabled, isUpdated, [note]"+
                            " FROM Orders"+
                            " WHERE(isComplete = False)" +
                            " AND(isSaved = False)"+
                            " AND(DateValue(orderDate) = date())" +
                            " ORDER BY orderID";
            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                int orderID = Convert.ToInt32(dr[0]);
                DateTime orderDate = Convert.ToDateTime(dr[1]);
                bool isDisabled = Convert.ToBoolean(dr[2]);
                bool isUpdated = Convert.ToBoolean(dr[3]);

                string note = dr[4].ToString();
                note = note.Replace("@!@", "\n");                                 

                Order order = new Order(orderID, orderDate, isDisabled, isUpdated);
                order.note = note;

                ordersToCheck.Add(order);
            }
            dBConnector.Close();

            UpdateOrderQueue();            
        }

        private void tmrOrders_Tick(object sender, EventArgs e)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers(); //Clearing the memory by getting rid of unused Objects

            CheckOrders();
            
            CheckIfOrderShouldBeDeleted();

            CheckIfShouldTurnUpdatedOrderBackToNormal();

            foreach (Order order in ordersToChangeToNormal)
            {
                order.flpOrder.Tag = Convert.ToInt32(order.flpOrder.Tag) + 1;
            }

            foreach (Order order in ordersToDelete)
            {
                order.flpOrder.Tag = Convert.ToInt32(order.flpOrder.Tag) + 1;
            }

            AddCompleteButtonToOrders();

            CheckIfEmpty();
        }

        private void AddCompleteButtonToOrders()
        {
            foreach (Order order in orderQueue.ToList())
            {
                Label lblComplete = null;
                try
                {
                    lblComplete = (order.flpOrderItems.Controls.Find("lblComplete", false).First() as Label);
                }
                catch (Exception) { }

                if (!ordersToChangeToNormal.Contains(order) && !ordersToDelete.Contains(order))
                {                                        
                    if (lblComplete == null)
                    {
                        lblComplete = new Label();
                        lblComplete.Name = "lblComplete";
                        lblComplete.Tag = Convert.ToInt32(order.flpOrder.Name.Replace("flpOrder_", ""));
                        lblComplete.BackColor = Color.LightCoral;
                        lblComplete.Margin = new Padding(4, 3, 4, 5);
                        lblComplete.Font = new Font("Microsoft Sans Serif", 18);
                        lblComplete.Text = "Complete";
                        lblComplete.TextAlign = ContentAlignment.MiddleCenter;
                        lblComplete.Size = new Size(300, 40);
                        lblComplete.BorderStyle = BorderStyle.FixedSingle;                        
                        lblComplete.Click += LblComplete_Click;                        

                        order.flpOrderItems.Controls.Add(lblComplete);
                    }
                }
                else
                {
                    if (lblComplete != null)
                    {
                        order.flpOrderItems.Controls.Remove(lblComplete);
                    }
                }
            }            
        }

        private void LblComplete_Click(object sender, EventArgs e) //**
        {
            clsDBConnector dBConnector = new clsDBConnector();

            DateTime now = new DateTime();
            now = DateTime.Now;

            int orderID = Convert.ToInt32((sender as Label).Tag);

            string dmlStr = "UPDATE Orders" +
                            " SET completeDate = '"+ now +"', isComplete = true" +
                            " WHERE(Orders.orderID = "+ orderID +")";

            dBConnector.Connect();
            dBConnector.DoDML(dmlStr);
            dBConnector.Close();

            (sender as Label).Parent.Parent.Visible = false;
            Order orderCompleted = orderQueue.Where(x => x.orderID == orderID).First();
            orderQueue = RemoveItemAtIndex(orderQueue, orderQueue.ToList().IndexOf(orderCompleted));
        }

        private void CheckIfEmpty()
        {
            if (orderQueue.Count == 0)
            {
                lblNoOrder.Visible = true;
            }
            else
            {
                lblNoOrder.Visible = false;
            }
        }

        private void CheckIfShouldTurnUpdatedOrderBackToNormal()
        {
            clsDBConnector dBConnector = new clsDBConnector();

            for (int i = 0; i < ordersToChangeToNormal.Count; i++)
            {
                if (Properties.Settings.Default.timerValueForUpdate != 0)
                {
                    int numberOfTicksRequired = Properties.Settings.Default.timerValueForUpdate / (tmrOrders.Interval / 1000);
                    if (Convert.ToInt32(ordersToChangeToNormal[i].flpOrder.Tag) >= numberOfTicksRequired)
                    {
                        ChangeOrderBackToNormal(i);                                        
                    }
                }
            }            
        }

        private void ChangeOrderBackToNormal(int i)             
        {
            //Index of the order in ordersToChangeToNormal
            clsDBConnector dBConnector = new clsDBConnector();

            Label lblTimeOfOrder = ordersToChangeToNormal[i].flpOrder.Controls.Find("timerOrder", false).First() as Label;
            int mins = CalculateTheTimePast(orderQueue.ToList().IndexOf(ordersToChangeToNormal[i]));
            lblTimeOfOrder.Text = mins + " Mins";

            ChangeColorsOfOrder(ordersToChangeToNormal[i], DefaultBackColor, Color.LightGray, Color.AliceBlue, Color.Gray, Color.LightGray);

            string dmlStr = "UPDATE Orders" +
                        " SET isUpdated = false" +
                        " WHERE(Orders.orderID = " + ordersToChangeToNormal[i].orderID + ")";
            dBConnector.Connect();
            dBConnector.DoDML(dmlStr);
            dBConnector.Close();

            ordersToChangeToNormal[i].flpOrder.Tag = 0;
            ordersToChangeToNormal[i].isUpdated = false;
            ordersToChangeToNormal.RemoveAt(i);

            ChangeOrderTimerColor();
            AddCompleteButtonToOrders();
        }

        private void DeleteOrder(int i)
        {
            //Index of the order in ordersToDelete
            clsDBConnector dBConnector = new clsDBConnector();            

            //set isComplete to true so that if the software is turned off for some reason, cancelled orders won't appear again
            string dmlStr = "UPDATE Orders" +
                        " SET isComplete = true" +
                        " WHERE(Orders.orderID = " + ordersToDelete[i].orderID + ")";
            dBConnector.Connect();
            dBConnector.DoDML(dmlStr);
            dBConnector.Close();

            int index = orderQueue.ToList().IndexOf(ordersToDelete[i]);

            if (flpMain.Controls.Contains(orderQueue.ToList()[index].flpOrder))
            {
                flpMain.Controls.Remove(orderQueue.ToList()[index].flpOrder);

                if (shownOrderIDs.Contains(orderQueue.ToList()[index].orderID))
                {
                    shownOrderIDs.Remove(orderQueue.ToList()[index].orderID);
                }
                else
                {
                    shownOrderIDsSelected.Remove(orderQueue.ToList()[index].orderID);
                }
            }
            else if (flpHiddenOrders.Controls.Contains(orderQueue.ToList()[index].flpOrder))
            {
                flpHiddenOrders.Controls.Remove(orderQueue.ToList()[index].flpOrder);

                hiddenOrderIDs.Remove(orderQueue.ToList()[index].orderID);
            }

            if (ordersToDelete.Exists(x => x.orderID == orderQueue.ToList()[index].orderID))
            {
                ordersToDelete.Remove(ordersToDelete.Where(x => x.orderID == orderQueue.ToList()[index].orderID).First());
            }

            orderQueue = RemoveItemAtIndex(orderQueue, index);

            ChangeOrderTimerColor();
        }

        private void CheckIfOrderShouldBeDeleted()
        {            
            for (int i = 0; i < ordersToDelete.Count; i++)
            {
                if (Properties.Settings.Default.timerValueForDelete != 0)
                {
                    //If there has been n number of ticks than we delete the order. n is selected by the user.
                    int numberOfTicksRequired = Properties.Settings.Default.timerValueForDelete / (tmrOrders.Interval / 1000);
                    if (Convert.ToInt32(ordersToDelete[i].flpOrder.Tag) >= numberOfTicksRequired)
                    {
                        DeleteOrder(i);
                    }                    
                }             
            }            
        }

        void Form_KeyPress(object sender, KeyPressEventArgs args) //**
        {
            if (args.KeyChar == (char)Keys.Escape)
            {                
                if (Application.OpenForms.Count == 1)
                {
                    SettingsForm newform = new SettingsForm(this, tmrOrders.Interval);
                    newform.Show();
                }
            }
        }

        private void lblSettingsInfo_Click(object sender, EventArgs e) //**
        {
            if (Application.OpenForms.Count == 1)
            {
                SettingsForm newform = new SettingsForm(this, tmrOrders.Interval);
                newform.Show();
            }
        }
    }
}
