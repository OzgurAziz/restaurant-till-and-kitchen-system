﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork
{
    class OrderItem
    {
        public int ID { get; }

        public List<string> toppings { get; }

        public string menuItem { get; }

        public int quantity { get; }

        public OrderItem (int ID_ ,string menuItem_  , List<string> toppings_, int quantity_)
        {
            ID = ID_;

            menuItem = menuItem_;

            toppings = toppings_;

            quantity = quantity_;
        }
    }
}
