﻿namespace CourseWork
{
    partial class frmOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrders));
            this.flpMain = new System.Windows.Forms.FlowLayoutPanel();
            this.tmrOrders = new System.Windows.Forms.Timer(this.components);
            this.flpHiddenOrders = new System.Windows.Forms.FlowLayoutPanel();
            this.lblSettingsInfo = new System.Windows.Forms.Label();
            this.lblNoOrder = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // flpMain
            // 
            this.flpMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpMain.AutoScroll = true;
            this.flpMain.BackColor = System.Drawing.SystemColors.Control;
            this.flpMain.Location = new System.Drawing.Point(13, 25);
            this.flpMain.Name = "flpMain";
            this.flpMain.Size = new System.Drawing.Size(1736, 725);
            this.flpMain.TabIndex = 0;
            this.flpMain.TabStop = true;
            this.flpMain.WrapContents = false;
            // 
            // tmrOrders
            // 
            this.tmrOrders.Enabled = true;
            this.tmrOrders.Interval = 5000;
            this.tmrOrders.Tick += new System.EventHandler(this.tmrOrders_Tick);
            // 
            // flpHiddenOrders
            // 
            this.flpHiddenOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpHiddenOrders.BackColor = System.Drawing.SystemColors.Control;
            this.flpHiddenOrders.Location = new System.Drawing.Point(13, 756);
            this.flpHiddenOrders.Name = "flpHiddenOrders";
            this.flpHiddenOrders.Size = new System.Drawing.Size(1736, 130);
            this.flpHiddenOrders.TabIndex = 1;
            // 
            // lblSettingsInfo
            // 
            this.lblSettingsInfo.AutoSize = true;
            this.lblSettingsInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSettingsInfo.Location = new System.Drawing.Point(10, 7);
            this.lblSettingsInfo.Name = "lblSettingsInfo";
            this.lblSettingsInfo.Size = new System.Drawing.Size(192, 15);
            this.lblSettingsInfo.TabIndex = 2;
            this.lblSettingsInfo.Text = "Click ESC for Settings or Click this.\r\n";
            this.lblSettingsInfo.Click += new System.EventHandler(this.lblSettingsInfo_Click);
            // 
            // lblNoOrder
            // 
            this.lblNoOrder.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblNoOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOrder.ForeColor = System.Drawing.Color.Gray;
            this.lblNoOrder.Location = new System.Drawing.Point(0, 746);
            this.lblNoOrder.Name = "lblNoOrder";
            this.lblNoOrder.Size = new System.Drawing.Size(1761, 152);
            this.lblNoOrder.TabIndex = 4;
            this.lblNoOrder.Text = "No Orders";
            this.lblNoOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1761, 898);
            this.Controls.Add(this.lblNoOrder);
            this.Controls.Add(this.lblSettingsInfo);
            this.Controls.Add(this.flpHiddenOrders);
            this.Controls.Add(this.flpMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.MinimumSize = new System.Drawing.Size(1280, 720);
            this.Name = "frmOrders";
            this.Text = "Orders";
            this.Load += new System.EventHandler(this.frmOrders_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpMain;
        private System.Windows.Forms.FlowLayoutPanel flpHiddenOrders;
        public System.Windows.Forms.Timer tmrOrders;
        private System.Windows.Forms.Label lblSettingsInfo;
        private System.Windows.Forms.Label lblNoOrder;
    }
}

