﻿
namespace CourseWork
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.cmbDeleteDuration = new System.Windows.Forms.ComboBox();
            this.lblDeleteTime = new System.Windows.Forms.Label();
            this.lblUpdateTime = new System.Windows.Forms.Label();
            this.cmbUpdateDuration = new System.Windows.Forms.ComboBox();
            this.lblCustomisations = new System.Windows.Forms.Label();
            this.pnlCustomisations = new System.Windows.Forms.Panel();
            this.lblMins2 = new System.Windows.Forms.Label();
            this.lblMins1 = new System.Windows.Forms.Label();
            this.txtRedWarning = new System.Windows.Forms.TextBox();
            this.lblOrderTimeWarningR = new System.Windows.Forms.Label();
            this.pcbRedWarningInfo = new System.Windows.Forms.PictureBox();
            this.txtOrangeWarning = new System.Windows.Forms.TextBox();
            this.lblOrderTimeWarningO = new System.Windows.Forms.Label();
            this.pcbOrangeWarningInfo = new System.Windows.Forms.PictureBox();
            this.pcbUpdatedOrder = new System.Windows.Forms.PictureBox();
            this.pcbDeletedOrder = new System.Windows.Forms.PictureBox();
            this.pcbRefreshFrequency = new System.Windows.Forms.PictureBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblTimer = new System.Windows.Forms.Label();
            this.cmbTimerValue = new System.Windows.Forms.ComboBox();
            this.pcbSavingIcon = new System.Windows.Forms.PictureBox();
            this.tmrAnimation = new System.Windows.Forms.Timer(this.components);
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblInfo = new System.Windows.Forms.Label();
            this.pnlCustomisations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbRedWarningInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbOrangeWarningInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbUpdatedOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDeletedOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbRefreshFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbSavingIcon)).BeginInit();
            this.pnlInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbDeleteDuration
            // 
            this.cmbDeleteDuration.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDeleteDuration.DropDownHeight = 100;
            this.cmbDeleteDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeleteDuration.DropDownWidth = 120;
            this.cmbDeleteDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDeleteDuration.FormattingEnabled = true;
            this.cmbDeleteDuration.IntegralHeight = false;
            this.cmbDeleteDuration.ItemHeight = 16;
            this.cmbDeleteDuration.Location = new System.Drawing.Point(202, 103);
            this.cmbDeleteDuration.Name = "cmbDeleteDuration";
            this.cmbDeleteDuration.Size = new System.Drawing.Size(96, 24);
            this.cmbDeleteDuration.TabIndex = 0;
            this.cmbDeleteDuration.TabStop = false;
            // 
            // lblDeleteTime
            // 
            this.lblDeleteTime.AutoSize = true;
            this.lblDeleteTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteTime.Location = new System.Drawing.Point(42, 106);
            this.lblDeleteTime.Name = "lblDeleteTime";
            this.lblDeleteTime.Size = new System.Drawing.Size(149, 16);
            this.lblDeleteTime.TabIndex = 1;
            this.lblDeleteTime.Text = "Deleted Order Duration:";
            // 
            // lblUpdateTime
            // 
            this.lblUpdateTime.AutoSize = true;
            this.lblUpdateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateTime.Location = new System.Drawing.Point(42, 159);
            this.lblUpdateTime.Name = "lblUpdateTime";
            this.lblUpdateTime.Size = new System.Drawing.Size(154, 16);
            this.lblUpdateTime.TabIndex = 4;
            this.lblUpdateTime.Text = "Updated Order Duration:";
            // 
            // cmbUpdateDuration
            // 
            this.cmbUpdateDuration.DropDownHeight = 100;
            this.cmbUpdateDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUpdateDuration.DropDownWidth = 120;
            this.cmbUpdateDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUpdateDuration.FormattingEnabled = true;
            this.cmbUpdateDuration.IntegralHeight = false;
            this.cmbUpdateDuration.ItemHeight = 16;
            this.cmbUpdateDuration.Location = new System.Drawing.Point(202, 156);
            this.cmbUpdateDuration.Name = "cmbUpdateDuration";
            this.cmbUpdateDuration.Size = new System.Drawing.Size(96, 24);
            this.cmbUpdateDuration.TabIndex = 3;
            this.cmbUpdateDuration.TabStop = false;
            // 
            // lblCustomisations
            // 
            this.lblCustomisations.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCustomisations.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomisations.Location = new System.Drawing.Point(0, 0);
            this.lblCustomisations.Name = "lblCustomisations";
            this.lblCustomisations.Size = new System.Drawing.Size(313, 25);
            this.lblCustomisations.TabIndex = 6;
            this.lblCustomisations.Text = "Customisations";
            this.lblCustomisations.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlCustomisations
            // 
            this.pnlCustomisations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlCustomisations.Controls.Add(this.pcbSavingIcon);
            this.pnlCustomisations.Controls.Add(this.lblMins2);
            this.pnlCustomisations.Controls.Add(this.lblMins1);
            this.pnlCustomisations.Controls.Add(this.txtRedWarning);
            this.pnlCustomisations.Controls.Add(this.lblOrderTimeWarningR);
            this.pnlCustomisations.Controls.Add(this.pcbRedWarningInfo);
            this.pnlCustomisations.Controls.Add(this.txtOrangeWarning);
            this.pnlCustomisations.Controls.Add(this.lblOrderTimeWarningO);
            this.pnlCustomisations.Controls.Add(this.pcbOrangeWarningInfo);
            this.pnlCustomisations.Controls.Add(this.pcbUpdatedOrder);
            this.pnlCustomisations.Controls.Add(this.pcbDeletedOrder);
            this.pnlCustomisations.Controls.Add(this.cmbUpdateDuration);
            this.pnlCustomisations.Controls.Add(this.pcbRefreshFrequency);
            this.pnlCustomisations.Controls.Add(this.lblUpdateTime);
            this.pnlCustomisations.Controls.Add(this.btnSave);
            this.pnlCustomisations.Controls.Add(this.lblCustomisations);
            this.pnlCustomisations.Controls.Add(this.cmbDeleteDuration);
            this.pnlCustomisations.Controls.Add(this.lblDeleteTime);
            this.pnlCustomisations.Controls.Add(this.lblTimer);
            this.pnlCustomisations.Controls.Add(this.cmbTimerValue);
            this.pnlCustomisations.Location = new System.Drawing.Point(559, 12);
            this.pnlCustomisations.Name = "pnlCustomisations";
            this.pnlCustomisations.Size = new System.Drawing.Size(313, 537);
            this.pnlCustomisations.TabIndex = 7;
            // 
            // lblMins2
            // 
            this.lblMins2.AutoSize = true;
            this.lblMins2.Location = new System.Drawing.Point(275, 267);
            this.lblMins2.Name = "lblMins2";
            this.lblMins2.Size = new System.Drawing.Size(28, 13);
            this.lblMins2.TabIndex = 22;
            this.lblMins2.Text = "mins";
            // 
            // lblMins1
            // 
            this.lblMins1.AutoSize = true;
            this.lblMins1.Location = new System.Drawing.Point(275, 215);
            this.lblMins1.Name = "lblMins1";
            this.lblMins1.Size = new System.Drawing.Size(28, 13);
            this.lblMins1.TabIndex = 21;
            this.lblMins1.Text = "mins";
            // 
            // txtRedWarning
            // 
            this.txtRedWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRedWarning.Location = new System.Drawing.Point(202, 262);
            this.txtRedWarning.Name = "txtRedWarning";
            this.txtRedWarning.Size = new System.Drawing.Size(67, 22);
            this.txtRedWarning.TabIndex = 20;
            this.txtRedWarning.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBox_KeyPress);
            // 
            // lblOrderTimeWarningR
            // 
            this.lblOrderTimeWarningR.AutoSize = true;
            this.lblOrderTimeWarningR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderTimeWarningR.Location = new System.Drawing.Point(42, 265);
            this.lblOrderTimeWarningR.Name = "lblOrderTimeWarningR";
            this.lblOrderTimeWarningR.Size = new System.Drawing.Size(90, 16);
            this.lblOrderTimeWarningR.TabIndex = 19;
            this.lblOrderTimeWarningR.Text = "Red Warning:";
            // 
            // pcbRedWarningInfo
            // 
            this.pcbRedWarningInfo.Image = ((System.Drawing.Image)(resources.GetObject("pcbRedWarningInfo.Image")));
            this.pcbRedWarningInfo.InitialImage = ((System.Drawing.Image)(resources.GetObject("pcbRedWarningInfo.InitialImage")));
            this.pcbRedWarningInfo.Location = new System.Drawing.Point(11, 261);
            this.pcbRedWarningInfo.Name = "pcbRedWarningInfo";
            this.pcbRedWarningInfo.Size = new System.Drawing.Size(25, 25);
            this.pcbRedWarningInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbRedWarningInfo.TabIndex = 18;
            this.pcbRedWarningInfo.TabStop = false;
            this.pcbRedWarningInfo.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // txtOrangeWarning
            // 
            this.txtOrangeWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrangeWarning.Location = new System.Drawing.Point(202, 209);
            this.txtOrangeWarning.Name = "txtOrangeWarning";
            this.txtOrangeWarning.Size = new System.Drawing.Size(67, 22);
            this.txtOrangeWarning.TabIndex = 17;
            this.txtOrangeWarning.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBox_KeyPress);
            // 
            // lblOrderTimeWarningO
            // 
            this.lblOrderTimeWarningO.AutoSize = true;
            this.lblOrderTimeWarningO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderTimeWarningO.Location = new System.Drawing.Point(42, 212);
            this.lblOrderTimeWarningO.Name = "lblOrderTimeWarningO";
            this.lblOrderTimeWarningO.Size = new System.Drawing.Size(109, 16);
            this.lblOrderTimeWarningO.TabIndex = 15;
            this.lblOrderTimeWarningO.Text = "Orange Warning:";
            // 
            // pcbOrangeWarningInfo
            // 
            this.pcbOrangeWarningInfo.Image = ((System.Drawing.Image)(resources.GetObject("pcbOrangeWarningInfo.Image")));
            this.pcbOrangeWarningInfo.InitialImage = ((System.Drawing.Image)(resources.GetObject("pcbOrangeWarningInfo.InitialImage")));
            this.pcbOrangeWarningInfo.Location = new System.Drawing.Point(11, 208);
            this.pcbOrangeWarningInfo.Name = "pcbOrangeWarningInfo";
            this.pcbOrangeWarningInfo.Size = new System.Drawing.Size(25, 25);
            this.pcbOrangeWarningInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbOrangeWarningInfo.TabIndex = 14;
            this.pcbOrangeWarningInfo.TabStop = false;
            this.pcbOrangeWarningInfo.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // pcbUpdatedOrder
            // 
            this.pcbUpdatedOrder.Image = ((System.Drawing.Image)(resources.GetObject("pcbUpdatedOrder.Image")));
            this.pcbUpdatedOrder.InitialImage = ((System.Drawing.Image)(resources.GetObject("pcbUpdatedOrder.InitialImage")));
            this.pcbUpdatedOrder.Location = new System.Drawing.Point(11, 155);
            this.pcbUpdatedOrder.Name = "pcbUpdatedOrder";
            this.pcbUpdatedOrder.Size = new System.Drawing.Size(25, 25);
            this.pcbUpdatedOrder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbUpdatedOrder.TabIndex = 13;
            this.pcbUpdatedOrder.TabStop = false;
            this.pcbUpdatedOrder.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // pcbDeletedOrder
            // 
            this.pcbDeletedOrder.Image = ((System.Drawing.Image)(resources.GetObject("pcbDeletedOrder.Image")));
            this.pcbDeletedOrder.InitialImage = ((System.Drawing.Image)(resources.GetObject("pcbDeletedOrder.InitialImage")));
            this.pcbDeletedOrder.Location = new System.Drawing.Point(11, 102);
            this.pcbDeletedOrder.Name = "pcbDeletedOrder";
            this.pcbDeletedOrder.Size = new System.Drawing.Size(25, 25);
            this.pcbDeletedOrder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbDeletedOrder.TabIndex = 12;
            this.pcbDeletedOrder.TabStop = false;
            this.pcbDeletedOrder.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // pcbRefreshFrequency
            // 
            this.pcbRefreshFrequency.Image = ((System.Drawing.Image)(resources.GetObject("pcbRefreshFrequency.Image")));
            this.pcbRefreshFrequency.InitialImage = ((System.Drawing.Image)(resources.GetObject("pcbRefreshFrequency.InitialImage")));
            this.pcbRefreshFrequency.Location = new System.Drawing.Point(11, 49);
            this.pcbRefreshFrequency.Name = "pcbRefreshFrequency";
            this.pcbRefreshFrequency.Size = new System.Drawing.Size(25, 25);
            this.pcbRefreshFrequency.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbRefreshFrequency.TabIndex = 11;
            this.pcbRefreshFrequency.TabStop = false;
            this.pcbRefreshFrequency.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(58, 484);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(252, 50);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.Location = new System.Drawing.Point(42, 53);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(125, 16);
            this.lblTimer.TabIndex = 8;
            this.lblTimer.Text = "Refresh Frequency:";
            // 
            // cmbTimerValue
            // 
            this.cmbTimerValue.BackColor = System.Drawing.SystemColors.Window;
            this.cmbTimerValue.DropDownHeight = 100;
            this.cmbTimerValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTimerValue.DropDownWidth = 120;
            this.cmbTimerValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTimerValue.FormattingEnabled = true;
            this.cmbTimerValue.IntegralHeight = false;
            this.cmbTimerValue.ItemHeight = 16;
            this.cmbTimerValue.Items.AddRange(new object[] {
            "5 seconds",
            "6 seconds",
            "7 seconds",
            "8 seconds",
            "9 seconds",
            "10 seconds",
            "11 seconds",
            "12 seconds",
            "13 seconds",
            "14 seconds",
            "15 seconds",
            "20 seconds",
            "25 seconds",
            "30 seconds"});
            this.cmbTimerValue.Location = new System.Drawing.Point(202, 50);
            this.cmbTimerValue.Name = "cmbTimerValue";
            this.cmbTimerValue.Size = new System.Drawing.Size(96, 24);
            this.cmbTimerValue.TabIndex = 9;
            this.cmbTimerValue.TabStop = false;
            // 
            // pcbSavingIcon
            // 
            this.pcbSavingIcon.Image = ((System.Drawing.Image)(resources.GetObject("pcbSavingIcon.Image")));
            this.pcbSavingIcon.InitialImage = ((System.Drawing.Image)(resources.GetObject("pcbSavingIcon.InitialImage")));
            this.pcbSavingIcon.Location = new System.Drawing.Point(5, 484);
            this.pcbSavingIcon.Name = "pcbSavingIcon";
            this.pcbSavingIcon.Size = new System.Drawing.Size(50, 50);
            this.pcbSavingIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbSavingIcon.TabIndex = 21;
            this.pcbSavingIcon.TabStop = false;
            this.pcbSavingIcon.Visible = false;
            // 
            // tmrAnimation
            // 
            this.tmrAnimation.Interval = 25;
            this.tmrAnimation.Tick += new System.EventHandler(this.tmrAnimation_Tick);
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlInfo.Controls.Add(this.lblInfo);
            this.pnlInfo.Location = new System.Drawing.Point(12, 12);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(541, 534);
            this.pnlInfo.TabIndex = 8;
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblInfo.Location = new System.Drawing.Point(0, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(541, 534);
            this.lblInfo.TabIndex = 0;
            this.lblInfo.Text = "For Menu Changes and Statistics Use the Till View.";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.pnlCustomisations);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(900, 600);
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.pnlCustomisations.ResumeLayout(false);
            this.pnlCustomisations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbRedWarningInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbOrangeWarningInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbUpdatedOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDeletedOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbRefreshFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbSavingIcon)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbDeleteDuration;
        private System.Windows.Forms.Label lblDeleteTime;
        private System.Windows.Forms.Label lblUpdateTime;
        private System.Windows.Forms.ComboBox cmbUpdateDuration;
        private System.Windows.Forms.Label lblCustomisations;
        private System.Windows.Forms.Panel pnlCustomisations;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbTimerValue;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.PictureBox pcbRefreshFrequency;
        private System.Windows.Forms.PictureBox pcbUpdatedOrder;
        private System.Windows.Forms.PictureBox pcbDeletedOrder;
        private System.Windows.Forms.Label lblOrderTimeWarningO;
        private System.Windows.Forms.PictureBox pcbOrangeWarningInfo;
        private System.Windows.Forms.TextBox txtRedWarning;
        private System.Windows.Forms.Label lblOrderTimeWarningR;
        private System.Windows.Forms.PictureBox pcbRedWarningInfo;
        private System.Windows.Forms.TextBox txtOrangeWarning;
        private System.Windows.Forms.PictureBox pcbSavingIcon;
        private System.Windows.Forms.Timer tmrAnimation;
        private System.Windows.Forms.Label lblMins2;
        private System.Windows.Forms.Label lblMins1;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblInfo;
    }
}