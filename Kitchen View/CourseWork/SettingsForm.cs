﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class SettingsForm : Form
    {
        frmOrders mainForm;
        int timerInterval;

        SettingsCls settings = new SettingsCls();

        public SettingsForm(frmOrders form_, int timerInterval_)
        {
            mainForm = form_;

            timerInterval = timerInterval_;

            mainForm.tmrOrders.Enabled = false;

            InitializeComponent();

            this.FormClosing += SettingsForm_FormClosing;
            this.FormClosed += SettingsForm_FormClosed;
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool isSaved = false;

            if (btnSave.Text == "Save Changes*")
            {
                isSaved = false;
            }
            else
            {
                isSaved = true;
            }

            if (!isSaved) //If customisations are not the same
            {
                DialogResult result =  MessageBox.Show(this, "There are unsaved changes! \nIf you exit now they will not be saved. " +
                    "\n\nWould you still like to exit?", "Warning!", MessageBoxButtons.YesNo);

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }            
        }

        private void SettingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainForm.tmrOrders.Enabled = true;

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            this.TopMost = true;

            UpdateComboOptions(timerInterval);
            SetComboBoxes();
            SetTextBoxes(true);

            AddListeners();            

            btnSave.Text = "Save Changes";
        }

        private void SetComboBoxes()
        {
            //Select TimerValue
            cmbTimerValue.SelectedItem = timerInterval / 1000 + " seconds";

            //Select DeleteDuration
            if (Properties.Settings.Default.timerValueForDelete == 0)
            {
                cmbDeleteDuration.SelectedItem = "Until clicked";
            }
            else
            {
                cmbDeleteDuration.SelectedItem = Properties.Settings.Default.timerValueForDelete + " seconds";
            }

            //Select UpdateDuration
            if (Properties.Settings.Default.timerValueForUpdate == 0)
            {
                cmbUpdateDuration.SelectedItem = "Until clicked";
            }
            else
            {
                cmbUpdateDuration.SelectedItem = Properties.Settings.Default.timerValueForUpdate + " seconds";
            }
        }

        private void UpdateComboOptions(int value) //in milliseconds
        {
            cmbDeleteDuration.Items.Add("Until clicked");
            cmbUpdateDuration.Items.Add("Until clicked");

            for (int i = 1; i < 7; i++)
            {
                cmbDeleteDuration.Items.Add(i * value / 1000 + " seconds");
                cmbUpdateDuration.Items.Add(i * value / 1000 + " seconds");
            }
        }

        private void ComboBoxChanged(object sender, EventArgs args)
        {
            ComboBox senderComboBox = (sender as ComboBox);

            int seconds = -1; 
            Match match = Regex.Match(senderComboBox.GetItemText(senderComboBox.SelectedItem), @"\d+");
            if (match.Success)
            {
                seconds = Convert.ToInt32(match.Value);
            }

            if (senderComboBox.Name == "cmbTimerValue")
            {                
                //When this changes the other combo boxes should change too

                settings.SetRefreshFrequency(seconds);

                cmbDeleteDuration.Items.Clear();
                cmbUpdateDuration.Items.Clear();
                UpdateComboOptions(seconds * 1000);
                cmbDeleteDuration.SelectedItem = "Until clicked";
                cmbUpdateDuration.SelectedItem = "Until clicked";
            }
            else if (senderComboBox.Name == "cmbDeleteDuration")
            {
                if (seconds == -1)
                {
                    settings.SetDeletedOrderDuration(0);
                }
                else
                {
                    settings.SetDeletedOrderDuration(seconds);
                }
            }
            else if (senderComboBox.Name == "cmbUpdateDuration")
            {
                if (seconds == -1)
                {
                    settings.SetUpdatedOrderDuration(0);
                }
                else
                {
                    settings.SetUpdatedOrderDuration(seconds);
                }
            }

            btnSave.Text = "Save Changes*";
        }

        async private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!settings.AreSettingsSaved())
            //{
            //    ChangeTheSettings();

            //    Properties.Settings.Default.Save();

            //    

            //    tmrAnimation.Enabled = true;
            //    pcbSavingIcon.Visible = true;

            //    

            //    btnSave.Text = "Save Changes";

            //    settings.SettingsSaved();
            //}

            if (!settings.AreSettingsSaved())
            {                
                ChangeTheSettings();

                mainForm.tmrOrders.Interval = Properties.Settings.Default.timerValue * 1000;

                tmrAnimation.Enabled = true;
                pcbSavingIcon.Visible = true;

                btnSave.Text = "Save Changes";

                SetTextBoxes(false);

                settings.ResetSettingsCls();
            }
        }

        private void ChangeTheSettings()
        {
            //Set the settings only if it is changed
            if (settings.GetRefreshFrequency() != -1) {
                Properties.Settings.Default.timerValue = settings.GetRefreshFrequency(); }
            if (settings.GetDeletedOrderDuration() != -1) {
                Properties.Settings.Default.timerValueForDelete = settings.GetDeletedOrderDuration(); }
            if (settings.GetUpdatedOrderDuration() != -1) {
                Properties.Settings.Default.timerValueForUpdate = settings.GetUpdatedOrderDuration(); }
            if (settings.GetOrangeWarning() != -1) {
                Properties.Settings.Default.orangeWarningValue = settings.GetOrangeWarning(); }
            if (settings.GetRedWarning() != -1) {
                Properties.Settings.Default.redWarningValue = settings.GetRedWarning(); }
                        
            Properties.Settings.Default.Save();                                                
        }              

        private void SetTextBoxes(bool formJustLoading)
        {
            if (formJustLoading)
            {
                if (Properties.Settings.Default.orangeWarningValue == 0)
                {
                    txtOrangeWarning.Text = "N/A";
                }
                else
                {
                    txtOrangeWarning.Text = Properties.Settings.Default.orangeWarningValue.ToString();
                }

                if (Properties.Settings.Default.redWarningValue == 0)
                {
                    txtRedWarning.Text = "N/A";
                }
                else
                {
                    txtRedWarning.Text = Properties.Settings.Default.redWarningValue.ToString();
                }
            }
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            switch ((sender as PictureBox).Name)
            {
                case "pcbRefreshFrequency":
                    MessageBox.Show(this, "Time between each refresh.", "Refresh Frequency", MessageBoxButtons.OK);
                    break;
                case "pcbDeletedOrder":
                    MessageBox.Show(this, "For how long a cancelled order \nwill be highlighted before it disappears.", "Deleted Order Duration", MessageBoxButtons.OK);
                    break;
                case "pcbUpdatedOrder":
                    MessageBox.Show(this, "For how long an updated order \nwill be highlighted before it changes back to normal.", "Updated Order Duration", MessageBoxButtons.OK);
                    break;
                case "pcbOrangeWarningInfo":
                    MessageBox.Show(this, "The colour of the order turns orange if the order \nhasn't been complete after set amount of time in minutes.\n\n" +
                        "If left empty the order won't change colour.", "Orange Warning", MessageBoxButtons.OK);
                    break;
                case "pcbRedWarningInfo":
                    MessageBox.Show(this, "The colour of the order turns red if the order \nhasn't been complete after set amount of time in minutes.\n\n" +
                        "If left empty the order won't change colour.", "Red Warning", MessageBoxButtons.OK);
                    break;
            }           
        }

        int animationCount = 1;

        private System.Resources.ResourceManager RM = new System.Resources.ResourceManager(typeof(Properties.Resources));

        private void tmrAnimation_Tick(object sender, EventArgs e)
        {            
            if (animationCount <= 33)
            {
                if (animationCount != 25)
                {
                    UpdateTheImageAccordingAnimationCount();
                    animationCount++;
                }
                else if (animationCount == 25)
                {
                    animationCount = 1;
                    UpdateTheImageAccordingAnimationCount();
                    animationCount = 26;
                }                              
            }
            else
            {                
                animationCount = 1;
                tmrAnimation.Enabled = false;
                pcbSavingIcon.Visible = false;
            }

            if (animationCount % 10 == 0)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }            
        }                

        private void UpdateTheImageAccordingAnimationCount()
        {
            if (animationCount != 25)
            {
                pcbSavingIcon.Image = (Image)RM.GetObject("_" + animationCount.ToString());
            }
            else
            {
                pcbSavingIcon.Image = (Image)RM.GetObject("_1");
            }
        }

        private void textBox_Changed(object sender, EventArgs args)
        {
            if ((sender as TextBox).Name == "txtOrangeWarning")
            {
                try
                {
                    settings.SetOrangeWarning(Convert.ToInt32((sender as TextBox).Text));
                }
                catch (Exception)
                {
                    
                }
            }
            else if ((sender as TextBox).Name == "txtRedWarning")
            {
                try
                {
                    settings.SetRedWarning(Convert.ToInt32((sender as TextBox).Text));
                }
                catch (Exception)
                {
                    
                }
            }

            btnSave.Text = "Save Changes*";
        }

        private void AddListeners()
        {
            cmbDeleteDuration.SelectedIndexChanged += new System.EventHandler(ComboBoxChanged);
            cmbUpdateDuration.SelectedIndexChanged += new System.EventHandler(ComboBoxChanged);
            cmbTimerValue.SelectedIndexChanged += new System.EventHandler(ComboBoxChanged);
            txtOrangeWarning.TextChanged += new System.EventHandler(textBox_Changed);
            txtRedWarning.TextChanged += new System.EventHandler(textBox_Changed);
        }

        private void txtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((sender as TextBox).Text.Length == 3)
            {
                if (!char.IsControl(e.KeyChar))
                {
                    e.Handled = true;
                }                
            }
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
