## Name
Restaurant Till and Kitchen System

## Description
This is a program that allows restaurants to manage their business (mainly orders, menu, etc.) and not only that but it provides statistics like their most popular products, income and average time taken for orders.
There are a lot of user settings and customisation options. For more detailed overview you can run it on your machine and explore.
This program was made in 2020.

## Visuals
### Till Main View
![Till Main View](../images/till_main.png?raw=true)

### Kitchen Main View
![Kitchen Main View](../images/kitchen_main.png?raw=true)

### Statistics
![Statistics](../images/stats.png?raw=true)



## Usage
The database is local and when running the absolute path of the database should be updated on both kitchen and till views.

## Authors and acknowledgment
Ozgur Aziz

## License
This is an open source project that can not be used in a business without permission from the author.

## Project status
The project is complete, however I might fix the database path problem in the future.
