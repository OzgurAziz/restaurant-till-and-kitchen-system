﻿namespace Till_Machine
{
    partial class ManageOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageOrders));
            this.flpOrderView = new System.Windows.Forms.FlowLayoutPanel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.lblNoOrdersMessage = new System.Windows.Forms.Label();
            this.pcbRefreshIcon = new System.Windows.Forms.PictureBox();
            this.tmrAnimation = new System.Windows.Forms.Timer(this.components);
            this.lblRefreshInfo = new System.Windows.Forms.Label();
            this.bkwRefresh = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pcbRefreshIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // flpOrderView
            // 
            this.flpOrderView.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.flpOrderView.AutoScroll = true;
            this.flpOrderView.Location = new System.Drawing.Point(12, 76);
            this.flpOrderView.MaximumSize = new System.Drawing.Size(960, 473);
            this.flpOrderView.MinimumSize = new System.Drawing.Size(960, 473);
            this.flpOrderView.Name = "flpOrderView";
            this.flpOrderView.Size = new System.Drawing.Size(960, 473);
            this.flpOrderView.TabIndex = 0;
            // 
            // btnUpdate
            // 
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(12, 12);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(178, 43);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "Update Order";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(794, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(178, 43);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel Order";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnView
            // 
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Location = new System.Drawing.Point(196, 12);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(178, 43);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "View Full Order";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblNoOrdersMessage
            // 
            this.lblNoOrdersMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblNoOrdersMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblNoOrdersMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOrdersMessage.ForeColor = System.Drawing.Color.Gray;
            this.lblNoOrdersMessage.Location = new System.Drawing.Point(0, 447);
            this.lblNoOrdersMessage.MinimumSize = new System.Drawing.Size(325, 75);
            this.lblNoOrdersMessage.Name = "lblNoOrdersMessage";
            this.lblNoOrdersMessage.Size = new System.Drawing.Size(984, 114);
            this.lblNoOrdersMessage.TabIndex = 0;
            this.lblNoOrdersMessage.Text = "No Orders";
            this.lblNoOrdersMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcbRefreshIcon
            // 
            this.pcbRefreshIcon.Image = global::Till_Machine.Properties.Resources.Refresh_Icon;
            this.pcbRefreshIcon.InitialImage = global::Till_Machine.Properties.Resources.Refresh_Icon;
            this.pcbRefreshIcon.Location = new System.Drawing.Point(900, 500);
            this.pcbRefreshIcon.Name = "pcbRefreshIcon";
            this.pcbRefreshIcon.Size = new System.Drawing.Size(50, 50);
            this.pcbRefreshIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbRefreshIcon.TabIndex = 11;
            this.pcbRefreshIcon.TabStop = false;
            this.pcbRefreshIcon.Click += new System.EventHandler(this.pcbRefreshIcon_Click);
            // 
            // tmrAnimation
            // 
            this.tmrAnimation.Interval = 25;
            this.tmrAnimation.Tick += new System.EventHandler(this.tmrAnimation_Tick);
            // 
            // lblRefreshInfo
            // 
            this.lblRefreshInfo.AutoSize = true;
            this.lblRefreshInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRefreshInfo.Location = new System.Drawing.Point(380, 13);
            this.lblRefreshInfo.Name = "lblRefreshInfo";
            this.lblRefreshInfo.Size = new System.Drawing.Size(89, 16);
            this.lblRefreshInfo.TabIndex = 12;
            this.lblRefreshInfo.Text = "Last Refresh: ";
            // 
            // bkwRefresh
            // 
            this.bkwRefresh.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkwRefresh_DoWork);
            this.bkwRefresh.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bkwRefresh_RunWorkerCompleted);
            // 
            // ManageOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.lblRefreshInfo);
            this.Controls.Add(this.pcbRefreshIcon);
            this.Controls.Add(this.lblNoOrdersMessage);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.flpOrderView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1000, 600);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "ManageOrders";
            this.Text = "ManageOrders";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManageOrders_FormClosed);
            this.Load += new System.EventHandler(this.ManageOrders_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcbRefreshIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpOrderView;
        private System.Windows.Forms.Label lblNoOrdersMessage;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.PictureBox pcbRefreshIcon;
        private System.Windows.Forms.Timer tmrAnimation;
        private System.Windows.Forms.Label lblRefreshInfo;
        private System.ComponentModel.BackgroundWorker bkwRefresh;
    }
}