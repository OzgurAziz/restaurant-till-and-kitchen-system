﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Till_Machine
{
    public partial class SavedOrders : Form
    {
        Main mainForm { get; }

        int selectedOrderNumber = -1;

        OrderViewUI orderViewCls;

        ////The Dictionary which has the ID as key and it's name and the price as the value
        //Dictionary<int, Tuple<string, double>> MenuItemsAndPrices = new Dictionary<int, Tuple<string, double>>();

        Queue<Order> SavedOrderQueue = new Queue<Order>();

        public SavedOrders(Main form)
        {
            mainForm = form;

            InitializeComponent();
        }

        private void SavedOrders_Load(object sender, EventArgs e)
        {
            ButtonsEnabled(false, false, false);

            orderViewCls = new OrderViewUI(mainForm.GetMenuItemsAndPrices(), null ,this);            

            GetSavedOrders();

            CheckIfEmpty();
        }

        public void Order_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs) e;

            //Getting clicked flow layout panel
            FlowLayoutPanel flpOrder = new FlowLayoutPanel();

            if ((sender as Control).Name != "flpOrder")
            {
                if ((sender as Control).Parent.Name == "flpOrder")
                {
                    flpOrder = (sender as Control).Parent as FlowLayoutPanel;
                }
                else if ((sender as Control).Parent.Parent.Name == "flpOrder")
                {
                    flpOrder = (sender as Control).Parent.Parent as FlowLayoutPanel;
                }
            }
            else
            {
                flpOrder = (sender as FlowLayoutPanel);
            }

            if (me.Button == System.Windows.Forms.MouseButtons.Left)
            {                                
                int IndexOfClickedOrder = -1;
                var match = SavedOrderQueue.ToList().Where(x => x.flpOrder == flpOrder);
                IndexOfClickedOrder = SavedOrderQueue.ToList().IndexOf(match.ToList().First());

                //Determinin which order is selected
                if (selectedOrderNumber == -1)
                {
                    flpOrder.BackColor = ColorTranslator.FromHtml("#C2D5FF");
                    flpOrder.Controls.Find("orderNumber", false).First().BackColor = ColorTranslator.FromHtml("#6386AD");

                    selectedOrderNumber = IndexOfClickedOrder;

                    if (flpOrder.Controls.Count == 4)
                    {
                        ButtonsEnabled(true, true, true);
                    }
                    else
                    {
                        ButtonsEnabled(true, false, true);
                    }
                }
                else if (selectedOrderNumber == IndexOfClickedOrder)
                {
                    selectedOrderNumber = -1;

                    flpOrder.BackColor = Color.Transparent;
                    flpOrder.Controls.Find("orderNumber", false).First().BackColor = Color.Gray;

                    ButtonsEnabled(false, false, false);
                }
                else
                {
                    Order alreadySelectedOrder = SavedOrderQueue.ElementAt(selectedOrderNumber);

                    alreadySelectedOrder.flpOrder.BackColor = Color.Transparent;
                    alreadySelectedOrder.flpOrder.Controls.Find("orderNumber", false).First().BackColor = Color.Gray;

                    flpOrder.BackColor = ColorTranslator.FromHtml("#C2D5FF");
                    flpOrder.Controls.Find("orderNumber", false).First().BackColor = ColorTranslator.FromHtml("#6386AD");

                    selectedOrderNumber = IndexOfClickedOrder;

                    if (flpOrder.Controls.Count == 4)
                    {
                        ButtonsEnabled(true, true, true);
                    }
                    else
                    {
                        ButtonsEnabled(true, false, true);
                    }
                }
            }
            else if (me.Button == System.Windows.Forms.MouseButtons.Right)
            {
                string nameOfSavedOrder = flpOrder.Controls.Find("orderNumber", false).First().Text;

                if (Application.OpenForms.Count == 2)
                {
                    using (UserInputForm userInputForm = new UserInputForm("Rename Saved Order", "The name can only have letters and numbers!" , false))
                    {
                        var result = userInputForm.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            nameOfSavedOrder = userInputForm.userInput;

                            Order clickedOrder = SavedOrderQueue.ToList().Where(x => x.flpOrder == flpOrder).First();

                            clsDBConnector dBConnector = new clsDBConnector();
                            string dmlStr = "UPDATE Orders"+
                                            " SET savedOrderName = '" + nameOfSavedOrder +"'" +
                                            " WHERE(Orders.orderID = " + clickedOrder.orderID + ")";
                            dBConnector.Connect();
                            dBConnector.DoDML(dmlStr);
                            dBConnector.Close();

                            SavedOrderQueue.Where(x => x.orderID == clickedOrder.orderID).First().flpOrder.Tag = nameOfSavedOrder;
                        }
                    }                                       
                }

                flpOrder.Controls.Find("orderNumber", false).First().Text = nameOfSavedOrder;
            }
                        
        }

        private void ButtonsEnabled(bool reloadButton,bool viewFullOrderButton , bool deleteButtton)
        {
            btnReload.Enabled = reloadButton;

            btnView.Enabled = viewFullOrderButton;

            btnDelete.Enabled = deleteButtton;
        }

        private void GetSavedOrders()
        {
            SavedOrderQueue = orderViewCls.InitializeOrderQueue(true);

            orderViewCls.UpdateFlowLayoutPanel(flpOrderView);

            DateTime now = new DateTime();
            now = DateTime.Now;

            lblRefreshInfo.Text = "Last Refresh: " + now.TimeOfDay.ToString("hh':'mm':'ss");
        }

        private void CheckIfEmpty()
        {
            if (SavedOrderQueue.Count == 0)
            {
                lblNoOrdersMessage.Visible = true;                
            }
            else
            {
                lblNoOrdersMessage.Visible = false;                
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            //reload to flpCurrentOrder
            mainForm.ReloadOrder(SavedOrderQueue.ElementAt(selectedOrderNumber), false);
            this.Close();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            string savedOrderName = SavedOrderQueue.ElementAt(selectedOrderNumber).flpOrder.Controls.Find("orderNumber", false).First().Text;
            ViewFullOrder form = new ViewFullOrder(SavedOrderQueue.ElementAt(selectedOrderNumber), savedOrderName);
            if (Application.OpenForms.Count == 2)
            {
                form.Show();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Set the isDisabled to true in the database
            Order orderToDelete = SavedOrderQueue.ElementAt(selectedOrderNumber);

            clsDBConnector dBConnector = new clsDBConnector();
            string dmlStr = "UPDATE Orders"+
                            " SET isDisabled = True" +
                            " WHERE(Orders.orderID = " + orderToDelete.orderID + ")";
            dBConnector.Connect();
            dBConnector.DoDML(dmlStr);
            dBConnector.Close();

            orderToDelete.flpOrder.Visible = false;
            SavedOrderQueue = RemoveItemAtIndex(SavedOrderQueue, SavedOrderQueue.ToList().IndexOf(orderToDelete));

            selectedOrderNumber = -1;

            ButtonsEnabled(false, false, false);

            GC.Collect();
            GC.WaitForPendingFinalizers();

            CheckIfEmpty();
        }

        private Queue<Order> RemoveItemAtIndex(Queue<Order> queue, int index)
        {
            List<Order> tempList = new List<Order>();
            tempList = queue.ToList();

            Queue<Order> newQueue = new Queue<Order>();

            for (int i = 0; i < tempList.Count; i++)
            {
                if (i != index)
                {
                    newQueue.Enqueue(tempList[i]);
                }
            }

            return newQueue;
        }

        private void pcbRefreshIcon_Click(object sender, EventArgs e)
        {
            if (!bkwRefresh.IsBusy)
            {
                flpOrderView.Controls.Clear();
                flpOrderView.Visible = false;

                bkwRefresh.RunWorkerAsync();
                tmrAnimation.Enabled = true;
            }                                                  
        }

        int animationCount = 1;
        private System.Resources.ResourceManager RM = new System.Resources.ResourceManager(typeof(Properties.Resources));

        private void tmrAnimation_Tick(object sender, EventArgs e)
        {
            orderViewCls.AnimationTick(ref animationCount, pcbRefreshIcon, RM, tmrAnimation, bkwComplete);
        }

        bool bkwComplete = false;

        private void bkwRefresh_DoWork(object sender, DoWorkEventArgs e)
        {
            bkwComplete = false;            

            orderViewCls = new OrderViewUI(mainForm.GetMenuItemsAndPrices(), null, this);
            SavedOrderQueue = orderViewCls.InitializeOrderQueue(true);
        }

        private void bkwRefresh_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            orderViewCls.UpdateFlowLayoutPanel(flpOrderView);
            flpOrderView.Visible = true;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            CheckIfEmpty();

            DateTime now = new DateTime();
            now = DateTime.Now;

            lblRefreshInfo.Text = "Last Refresh: " + now.TimeOfDay.ToString("hh':'mm':'ss");

            bkwComplete = true;
        }

        private void SavedOrders_FormClosed(object sender, FormClosedEventArgs e)
        {            
            this.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
