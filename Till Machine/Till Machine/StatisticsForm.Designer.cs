﻿
namespace Till_Machine
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatisticsForm));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbCompare = new System.Windows.Forms.ComboBox();
            this.lblCompare = new System.Windows.Forms.Label();
            this.lblChartSettings = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtNumberOfItems = new System.Windows.Forms.TextBox();
            this.lblShowItems = new System.Windows.Forms.Label();
            this.cmbTimePeriod = new System.Windows.Forms.ComboBox();
            this.lblTimePeriod = new System.Windows.Forms.Label();
            this.pnlSettings = new System.Windows.Forms.Panel();
            this.lblChart = new System.Windows.Forms.Label();
            this.lblChartPanel = new System.Windows.Forms.Label();
            this.cmbChart = new System.Windows.Forms.ComboBox();
            this.lblBarChart = new System.Windows.Forms.Label();
            this.chrtMain = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.prbLoading = new System.Windows.Forms.ProgressBar();
            this.bkwLoad = new System.ComponentModel.BackgroundWorker();
            this.lblSaveChart = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrtMain)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlMain.Controls.Add(this.panel1);
            this.pnlMain.Controls.Add(this.pnlSettings);
            this.pnlMain.Controls.Add(this.lblBarChart);
            this.pnlMain.Controls.Add(this.chrtMain);
            this.pnlMain.Location = new System.Drawing.Point(12, 12);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(960, 508);
            this.pnlMain.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.cmbCompare);
            this.panel1.Controls.Add(this.lblCompare);
            this.panel1.Controls.Add(this.lblChartSettings);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.txtNumberOfItems);
            this.panel1.Controls.Add(this.lblShowItems);
            this.panel1.Controls.Add(this.cmbTimePeriod);
            this.panel1.Controls.Add(this.lblTimePeriod);
            this.panel1.Location = new System.Drawing.Point(671, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(286, 340);
            this.panel1.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(3, 294);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(280, 43);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save as Image";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbCompare
            // 
            this.cmbCompare.FormattingEnabled = true;
            this.cmbCompare.Items.AddRange(new object[] {
            "Daily",
            "Weekly",
            "Monthly",
            "Yearly"});
            this.cmbCompare.Location = new System.Drawing.Point(103, 105);
            this.cmbCompare.Name = "cmbCompare";
            this.cmbCompare.Size = new System.Drawing.Size(170, 21);
            this.cmbCompare.TabIndex = 8;
            // 
            // lblCompare
            // 
            this.lblCompare.AutoSize = true;
            this.lblCompare.Location = new System.Drawing.Point(45, 108);
            this.lblCompare.Name = "lblCompare";
            this.lblCompare.Size = new System.Drawing.Size(52, 13);
            this.lblCompare.TabIndex = 7;
            this.lblCompare.Text = "Compare:";
            // 
            // lblChartSettings
            // 
            this.lblChartSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblChartSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChartSettings.Location = new System.Drawing.Point(0, 0);
            this.lblChartSettings.Name = "lblChartSettings";
            this.lblChartSettings.Size = new System.Drawing.Size(286, 30);
            this.lblChartSettings.TabIndex = 6;
            this.lblChartSettings.Text = "Chart Settings";
            this.lblChartSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUpdate
            // 
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(103, 132);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(170, 43);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Update Chart";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtNumberOfItems
            // 
            this.txtNumberOfItems.Location = new System.Drawing.Point(103, 52);
            this.txtNumberOfItems.Name = "txtNumberOfItems";
            this.txtNumberOfItems.Size = new System.Drawing.Size(48, 20);
            this.txtNumberOfItems.TabIndex = 3;
            this.txtNumberOfItems.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtShowTop_KeyPress);
            // 
            // lblShowItems
            // 
            this.lblShowItems.AutoSize = true;
            this.lblShowItems.Location = new System.Drawing.Point(10, 55);
            this.lblShowItems.Name = "lblShowItems";
            this.lblShowItems.Size = new System.Drawing.Size(87, 13);
            this.lblShowItems.TabIndex = 2;
            this.lblShowItems.Text = "Number of Items:";
            // 
            // cmbTimePeriod
            // 
            this.cmbTimePeriod.FormattingEnabled = true;
            this.cmbTimePeriod.Items.AddRange(new object[] {
            "1 Week",
            "2 Weeks",
            "1 Month",
            "3 Months",
            "1 Year",
            "2 Years",
            "All"});
            this.cmbTimePeriod.Location = new System.Drawing.Point(103, 78);
            this.cmbTimePeriod.Name = "cmbTimePeriod";
            this.cmbTimePeriod.Size = new System.Drawing.Size(170, 21);
            this.cmbTimePeriod.TabIndex = 1;
            // 
            // lblTimePeriod
            // 
            this.lblTimePeriod.AutoSize = true;
            this.lblTimePeriod.Location = new System.Drawing.Point(31, 81);
            this.lblTimePeriod.Name = "lblTimePeriod";
            this.lblTimePeriod.Size = new System.Drawing.Size(66, 13);
            this.lblTimePeriod.TabIndex = 0;
            this.lblTimePeriod.Text = "Time Period:";
            // 
            // pnlSettings
            // 
            this.pnlSettings.BackColor = System.Drawing.Color.Silver;
            this.pnlSettings.Controls.Add(this.lblChart);
            this.pnlSettings.Controls.Add(this.lblChartPanel);
            this.pnlSettings.Controls.Add(this.cmbChart);
            this.pnlSettings.Location = new System.Drawing.Point(671, 3);
            this.pnlSettings.Name = "pnlSettings";
            this.pnlSettings.Size = new System.Drawing.Size(286, 91);
            this.pnlSettings.TabIndex = 3;
            // 
            // lblChart
            // 
            this.lblChart.AutoSize = true;
            this.lblChart.Location = new System.Drawing.Point(38, 52);
            this.lblChart.Name = "lblChart";
            this.lblChart.Size = new System.Drawing.Size(35, 13);
            this.lblChart.TabIndex = 4;
            this.lblChart.Text = "Chart:";
            // 
            // lblChartPanel
            // 
            this.lblChartPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblChartPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChartPanel.Location = new System.Drawing.Point(0, 0);
            this.lblChartPanel.Name = "lblChartPanel";
            this.lblChartPanel.Size = new System.Drawing.Size(286, 33);
            this.lblChartPanel.TabIndex = 3;
            this.lblChartPanel.Text = "Chart";
            this.lblChartPanel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbChart
            // 
            this.cmbChart.FormattingEnabled = true;
            this.cmbChart.Items.AddRange(new object[] {
            "Popular Items",
            "Income",
            "Order Completion Times"});
            this.cmbChart.Location = new System.Drawing.Point(79, 49);
            this.cmbChart.Name = "cmbChart";
            this.cmbChart.Size = new System.Drawing.Size(170, 21);
            this.cmbChart.TabIndex = 2;
            // 
            // lblBarChart
            // 
            this.lblBarChart.BackColor = System.Drawing.Color.Silver;
            this.lblBarChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBarChart.Location = new System.Drawing.Point(671, 440);
            this.lblBarChart.Name = "lblBarChart";
            this.lblBarChart.Size = new System.Drawing.Size(286, 65);
            this.lblBarChart.TabIndex = 1;
            this.lblBarChart.Text = "Nothing Selected...";
            this.lblBarChart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chrtMain
            // 
            chartArea1.Name = "ChartArea1";
            this.chrtMain.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.White;
            legend1.Name = "Legend1";
            this.chrtMain.Legends.Add(legend1);
            this.chrtMain.Location = new System.Drawing.Point(3, 3);
            this.chrtMain.Name = "chrtMain";
            this.chrtMain.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            this.chrtMain.Size = new System.Drawing.Size(662, 502);
            this.chrtMain.TabIndex = 0;
            this.chrtMain.Text = "Hello";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title1.Name = "title";
            title1.Text = "Chart Title";
            this.chrtMain.Titles.Add(title1);
            this.chrtMain.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chrtMain_MouseClick);
            // 
            // prbLoading
            // 
            this.prbLoading.Location = new System.Drawing.Point(12, 526);
            this.prbLoading.Name = "prbLoading";
            this.prbLoading.Size = new System.Drawing.Size(960, 23);
            this.prbLoading.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.prbLoading.TabIndex = 1;
            // 
            // bkwLoad
            // 
            this.bkwLoad.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkwLoad_DoWork);
            this.bkwLoad.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bkwLoad_ProgressChanged);
            this.bkwLoad.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bkwLoad_RunWorkerCompleted);
            // 
            // lblSaveChart
            // 
            this.lblSaveChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaveChart.ForeColor = System.Drawing.Color.Gray;
            this.lblSaveChart.Location = new System.Drawing.Point(12, -11);
            this.lblSaveChart.Name = "lblSaveChart";
            this.lblSaveChart.Size = new System.Drawing.Size(960, 23);
            this.lblSaveChart.TabIndex = 2;
            this.lblSaveChart.Text = "Saving The Chart...";
            this.lblSaveChart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSaveChart.Visible = false;
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.lblSaveChart);
            this.Controls.Add(this.prbLoading);
            this.Controls.Add(this.pnlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1000, 600);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "StatisticsForm";
            this.Text = "StatisticsForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StatisticsForm_FormClosed);
            this.Load += new System.EventHandler(this.StatisticsForm_Load);
            this.pnlMain.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrtMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.ProgressBar prbLoading;
        private System.ComponentModel.BackgroundWorker bkwLoad;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrtMain;
        private System.Windows.Forms.Label lblBarChart;
        private System.Windows.Forms.Panel pnlSettings;
        private System.Windows.Forms.Label lblChart;
        private System.Windows.Forms.Label lblChartPanel;
        private System.Windows.Forms.ComboBox cmbChart;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbTimePeriod;
        private System.Windows.Forms.Label lblTimePeriod;
        private System.Windows.Forms.TextBox txtNumberOfItems;
        private System.Windows.Forms.Label lblShowItems;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lblChartSettings;
        private System.Windows.Forms.ComboBox cmbCompare;
        private System.Windows.Forms.Label lblCompare;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSaveChart;
    }
}