﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Threading;
using System.Drawing.Imaging;
using System.IO;

namespace Till_Machine
{
    public partial class StatisticsForm : Form
    {
        //The Dictionary which has the menu item as key and it's ID and the price as the value BUT THE DIFFERENCE IS
        //THIS DICTIONARY INCLUDES ALL MENU ITEMS EVEN THE ONES THAT WERE ON THE MENU IN THE PAST
        Dictionary<string, Tuple<int, double>> MenuItemsAndPrices = new Dictionary<string, Tuple<int, double>>();

        //The Dictionary which has the menu item as key and the quantity that is sold to this day as its value
        Dictionary<string, int> popularMenuItems = new Dictionary<string, int>();

        //The Dictionary which has the day/week/month/year as key and price of that order as value
        Dictionary<Tuple<DateTime, DateTime>, double> incomeDataDaily = new Dictionary<Tuple<DateTime, DateTime>, double>();
        Dictionary<Tuple<DateTime,DateTime>, double> incomeDataWeekly = new Dictionary<Tuple<DateTime, DateTime>, double>();
        Dictionary<Tuple<DateTime, DateTime>, double> incomeDataMonthly = new Dictionary<Tuple<DateTime, DateTime>, double>();
        Dictionary<Tuple<DateTime, DateTime>, double> incomeDataYearly = new Dictionary<Tuple<DateTime, DateTime>, double>();

        //The Dictionary which has the the day/week/month/year as key and average order completion time as value
        Dictionary<Tuple<DateTime, DateTime>, double> completionTimeDaily = new Dictionary<Tuple<DateTime, DateTime>, double>();
        Dictionary<Tuple<DateTime, DateTime>, double> completionTimeWeekly = new Dictionary<Tuple<DateTime, DateTime>, double>();
        Dictionary<Tuple<DateTime, DateTime>, double> completionTimeMonthly = new Dictionary<Tuple<DateTime, DateTime>, double>();
        Dictionary<Tuple<DateTime, DateTime>, double> completionTimeYearly = new Dictionary<Tuple<DateTime, DateTime>, double>();

        //Initial Settings of the Chart
        string timePeriod = "1 Week";
        int numberOfItems = 10;
        string compare = "Daily";

        //Individual Settings***
        //Popular Items
        string popularItemsTimePeriod;
        int popularItemsNumberOfItems;
        //Income
        int incomeNumberOfItems;
        string incomeCompare;
        //Order Completion Times
        int completionTimeNumberOfItems;
        string completionTimeCompare;
            
        public StatisticsForm()
        {
            this.TopMost = true;

            InitializeComponent();
        }

        private void StatisticsForm_Load(object sender, EventArgs e)
        {            
            //Individual Settings
            popularItemsTimePeriod = timePeriod;
            popularItemsNumberOfItems = numberOfItems;

            incomeNumberOfItems = numberOfItems;
            incomeCompare = compare;

            completionTimeNumberOfItems = numberOfItems;
            completionTimeCompare = compare;


            //Controls Start
            txtNumberOfItems.Text = numberOfItems.ToString();
            cmbTimePeriod.SelectedItem = timePeriod;
            cmbChart.SelectedItem = "Popular Items";
            cmbChart.SelectedIndexChanged += new System.EventHandler(cmbChart_SelectedIndexChanged);            
            cmbCompare.Enabled = false;

            cmbChart.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbCompare.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbTimePeriod.DropDownStyle = ComboBoxStyle.DropDownList;

            lblSaveChart.Dock = DockStyle.Fill;
            //Controls End

            pnlMain.Visible = false;

            bkwLoad.WorkerReportsProgress = true;

            bkwLoad.RunWorkerAsync();
        }        

        private void LoadTheMenu()
        {
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "SELECT menuItemID, Name, Cost" +
                     " FROM[Menu Items]";

            dbConnector.Connect();
            dr = dbConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                MenuItemsAndPrices.Add(dr[1].ToString(), Tuple.Create(Convert.ToInt32(dr[0]), Convert.ToDouble(dr[2])));
            }
            dbConnector.Close();
        }

        private void GetPopularMenuItems(string timePeriod)
        {
            popularMenuItems.Clear();

            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            DateTime date = GetDateTimeFromTimePeriod(timePeriod);

            string sqlStr = "SELECT [Order Items].Quantity, [Menu Items].Name" +
                            " FROM(([Menu Items] INNER JOIN" +
                            " [Order Items] ON[Menu Items].menuItemID = [Order Items].menuItemID) INNER JOIN" +
                            " Orders ON[Order Items].orderID = Orders.orderID)" +
                            " WHERE(DateValue(Orders.orderDate) >= #" + date.ToString("MM/dd/yyyy") + "#)" +
                            " AND(Orders.completeDate IS NOT NULL)" +
                            " ORDER BY [Menu Items].Name";
            dbConnector.Connect();
            dr = dbConnector.DoSQL(sqlStr);

            bool firstTime = true;
            int menuItemCount = 0;
            string previousMenuItem = "";

            while (dr.Read())
            {
                string currentMenuItem = dr[1].ToString();
                int currentQuantity = Convert.ToInt32(dr[0]);

                if (firstTime)
                {
                    firstTime = false;

                    menuItemCount = currentQuantity;
                }
                else
                {
                    if (currentMenuItem == previousMenuItem)
                    {
                        menuItemCount = menuItemCount + currentQuantity;
                    }
                    else
                    {                        
                        popularMenuItems.Add(previousMenuItem, menuItemCount);
                        menuItemCount = currentQuantity;
                    }
                }                

                previousMenuItem = currentMenuItem;                
            }

            //The last item is stored outside the loop because in the while loop we store the previousMenuItem meaning that
            //the last one is imposible to store inside unless I know the how many rows of data I got in the databaseReader
            popularMenuItems.Add(previousMenuItem, menuItemCount);

            popularMenuItems = popularMenuItems.OrderByDescending(key => key.Value).ToDictionary(x => x.Key, y => y.Value);                      
        }

        private void GetIncomeData()
        {                        
            DateTime now = new DateTime();
            now = DateTime.Now;                        

            GetIncomeDailyData(now);

            bkwLoad.ReportProgress(30);

            GetIncomeWeeklyMonthlyYearlyData(now);            

            bkwLoad.ReportProgress(60);
        }

        private void GetIncomeDailyData(DateTime now)
        {
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            dbConnector.Connect();

            string sqlStr = "";

            List<DateTime> daysNeeded = new List<DateTime>();
            //Upper limit to the number of items to display on the chart                    
            for (int i = 0; i < 30; i++)
            {
                daysNeeded.Add(now.AddDays(-i).Date);
            }

            for (int i = 0; i < daysNeeded.Count; i++)
            {
                sqlStr = "SELECT SUM(Price)" +
                         " FROM Orders" +
                         " WHERE(DateValue(orderDate) = #" + daysNeeded[i].ToString("MM/dd/yyyy") + "#)" +
                         " AND (completeDate IS NOT NULL)";

                dr = dbConnector.DoSQL(sqlStr);
                while (dr.Read())
                {
                    if (dr[0] != DBNull.Value)
                    {
                        incomeDataDaily.Add(Tuple.Create(daysNeeded[i],daysNeeded[i]), Convert.ToDouble(dr[0]));
                    }
                    else
                    {
                        incomeDataDaily.Add(Tuple.Create(daysNeeded[i], daysNeeded[i]), 0);
                    }
                }
            }

            dbConnector.Close();

            incomeDataDaily = incomeDataDaily.Reverse().ToDictionary(x => x.Key, y => y.Value);
        }

        private void GetIncomeWeeklyMonthlyYearlyData(DateTime now)
        {
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            dbConnector.Connect();

            for (int a = 0; a < 3; a++) //3 times because we want weekly monthly and yearly.
            {                                
                List<Tuple<DateTime, DateTime>> datesNeeded = new List<Tuple<DateTime, DateTime>>();
                datesNeeded = GetDatesNeeded(a, now);
                
                for (int i = 0; i < datesNeeded.Count; i++)
                {
                    string sqlStr = "SELECT SUM(Price)" +
                                    " FROM Orders" +
                                    " WHERE(DateValue(orderDate) >= #" + datesNeeded[i].Item1.ToString("MM/dd/yyyy") + "#)" +
                                    " AND(DateValue(orderDate) <= #" + datesNeeded[i].Item2.ToString("MM/dd/yyyy") + "#)" +
                                    " AND (completeDate IS NOT NULL)";

                    dr = dbConnector.DoSQL(sqlStr);
                    while (dr.Read())
                    {
                        if (dr[0] != DBNull.Value)
                        {
                            switch (a)
                            {
                                case 0:
                                    incomeDataWeekly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), Convert.ToDouble(dr[0]));
                                    break;
                                case 1:
                                    incomeDataMonthly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), Convert.ToDouble(dr[0]));
                                    break;
                                case 2:
                                    incomeDataYearly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), Convert.ToDouble(dr[0]));
                                    break;
                            }                            
                        }
                        else
                        {
                            switch (a)
                            {
                                case 0:
                                    incomeDataWeekly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), 0);
                                    break;
                                case 1:
                                    incomeDataMonthly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), 0);
                                    break;
                                case 2:
                                    incomeDataYearly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), 0);
                                    break;
                            }                           
                        }
                    }
                }                
            }

            dbConnector.Close();

            incomeDataWeekly = incomeDataWeekly.Reverse().ToDictionary(x => x.Key, y => y.Value);
            incomeDataMonthly = incomeDataMonthly.Reverse().ToDictionary(x => x.Key, y => y.Value);
        }        

        private void GetOrderCompletionTimes()
        {
            DateTime now = new DateTime();
            now = DateTime.Now;

            GetOrderCompletionTimesDaily(now);

            bkwLoad.ReportProgress(70);

            GerOrderCompletionTimesWeeklyMonthlyYearly(now);

            bkwLoad.ReportProgress(100);
        }

        private void GerOrderCompletionTimesWeeklyMonthlyYearly(DateTime now)
        {
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            dbConnector.Connect();

            for (int a = 0; a < 3; a++) //3 times because we want weekly monthly and yearly.
            {
                List<Tuple<DateTime, DateTime>> datesNeeded = new List<Tuple<DateTime, DateTime>>();
                datesNeeded = GetDatesNeeded(a, now);

                for (int i = 0; i < datesNeeded.Count; i++)
                {
                    string sqlStr = "SELECT orderDate, completeDate" +
                                    " FROM Orders" +
                                    " WHERE(DateValue(orderDate) >= #" + datesNeeded[i].Item1.ToString("MM/dd/yyyy") + "#)" +
                                    " AND(DateValue(orderDate) <= #" + datesNeeded[i].Item2.ToString("MM/dd/yyyy") + "#)" +
                                    " AND (completeDate IS NOT NULL)";
                    List<double> orderTimes = new List<double>();
                    dr = dbConnector.DoSQL(sqlStr);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            orderTimes.Add(Convert.ToDateTime(dr[1]).Subtract(Convert.ToDateTime(dr[0])).TotalMinutes);
                        }
                    }
                    else
                    {
                        orderTimes.Add(0);
                    }

                    double average = GetAverageCompletionTime(orderTimes);

                    switch (a)
                    {
                        case 0:
                            completionTimeWeekly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), average);
                            break;
                        case 1:
                            completionTimeMonthly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), average);
                            break;
                        case 2:
                            completionTimeYearly.Add(Tuple.Create(datesNeeded[i].Item1, datesNeeded[i].Item2), average);
                            break;
                    }
                }
            }

            dbConnector.Close();

            completionTimeWeekly = completionTimeWeekly.Reverse().ToDictionary(x => x.Key, y => y.Value);
            completionTimeMonthly = completionTimeMonthly.Reverse().ToDictionary(x => x.Key, y => y.Value);
        }
        private void GetOrderCompletionTimesDaily(DateTime now)
        {
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            dbConnector.Connect();

            string sqlStr = "";

            List<DateTime> daysNeeded = new List<DateTime>();
            //Upper limit to the number of items to display on the chart                    
            for (int i = 0; i < 30; i++)
            {
                daysNeeded.Add(now.AddDays(-i).Date);
            }

            for (int i = 0; i < daysNeeded.Count; i++)
            {
                sqlStr = "SELECT orderDate, completeDate" +
                         " FROM Orders" +
                         " WHERE(DateValue(orderDate) = #" + daysNeeded[i].ToString("MM/dd/yyyy") + "#)" +
                         " AND (completeDate IS NOT NULL)";

                List<double> orderTimes = new List<double>();
                dr = dbConnector.DoSQL(sqlStr);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        orderTimes.Add(Convert.ToDateTime(dr[1]).Subtract(Convert.ToDateTime(dr[0])).TotalMinutes);
                    }
                }
                else
                {
                    orderTimes.Add(0);
                }
                
                double average = GetAverageCompletionTime(orderTimes);

                completionTimeDaily.Add(Tuple.Create(daysNeeded[i], daysNeeded[i]), average);
            }

            dbConnector.Close();

            completionTimeDaily = completionTimeDaily.Reverse().ToDictionary(x => x.Key, y => y.Value);
        }

        private List<Tuple<DateTime, DateTime>> GetDatesNeeded(int a, DateTime now)
        {
            List<Tuple<DateTime, DateTime>> datesNeeded = new List<Tuple<DateTime, DateTime>>();

            switch (a)
            {
                case 0: //Weekly
                    datesNeeded.Add(FindCurrentWeek(now));
                    for (int i = 1; i < 30; i++)
                    {
                        datesNeeded.Add(Tuple.Create(datesNeeded[i - 1].Item1.AddDays(-7), datesNeeded[i - 1].Item2.AddDays(-7)));
                    }
                    break;
                case 1: //Monthly
                    datesNeeded.Add(FindCurrentMonth(now));
                    for (int i = 1; i < 30; i++)
                    {
                        DateTime startOfMonth = datesNeeded[i - 1].Item1.AddMonths(-1);
                        DateTime endOfMonth = Convert.ToDateTime(DateTime.DaysInMonth(startOfMonth.Year, startOfMonth.Month) + "/" + startOfMonth.Month + "/" + startOfMonth.Year);
                        datesNeeded.Add(Tuple.Create(startOfMonth, endOfMonth));
                    }
                    break;
                case 2: //Yearly
                    datesNeeded.Add(Tuple.Create(Convert.ToDateTime("1/1/" + now.Year), Convert.ToDateTime("31/12/" + now.Year)));
                    for (int i = 1; i < 30; i++)
                    {
                        datesNeeded.Add(Tuple.Create(datesNeeded[i - 1].Item1.AddYears(-1), datesNeeded[i - 1].Item2.AddYears(-1)));
                    }
                    break;
            }

            return datesNeeded;
        }

        private double GetAverageCompletionTime(List<double> orderTimes)
        {
            return orderTimes.Average();
        }

        private Tuple<DateTime,DateTime> FindCurrentMonth(DateTime now)
        {
            DateTime startDate = now.Date;
            DateTime endDate;

            while (startDate.Day != 1)
            {
                startDate = startDate.AddDays(-1);
            }

            endDate = Convert.ToDateTime(DateTime.DaysInMonth(startDate.Year, startDate.Month) + "/" + startDate.Month + "/" + startDate.Year);

            return Tuple.Create(startDate, endDate);
        }

        private Tuple<DateTime,DateTime> FindCurrentWeek(DateTime now)
        {
            DateTime startDate = now.Date;
            DateTime endDate;

            while (Convert.ToInt32(startDate.DayOfWeek) != 1)
            {
                startDate = startDate.AddDays(-1);
            }

            endDate = startDate.AddDays(6);

            return Tuple.Create(startDate, endDate);
        }

        private void bkwLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadTheMenu();

            bkwLoad.ReportProgress(10);            

            GetPopularMenuItems(timePeriod);

            bkwLoad.ReportProgress(20);

            GetIncomeData();            

            GetOrderCompletionTimes();
        }
        
        private void bkwLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            prbLoading.Value = e.ProgressPercentage;
        }

        private void bkwLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DisplayPopularItems(popularItemsNumberOfItems);

            pnlMain.Visible = true;
        }

        private void DisplayPopularItems(int popularItemsNumberOfItems)
        {
            ClearChart();

            chrtMain.Titles["title"].Text = "Popular Menu Items";            

            chrtMain.ChartAreas["ChartArea1"].AxisX.Enabled = AxisEnabled.False;
            chrtMain.Series.Add("Menu Items");

            lblBarChart.Text = "Nothing Selected...";

            //Show the wanted number of items
            int wantedNumber = -1;
            if (popularItemsNumberOfItems > popularMenuItems.Count)
            {
                wantedNumber = popularMenuItems.Count;
            }
            else if (popularItemsNumberOfItems <= popularMenuItems.Count)
            {
                wantedNumber = popularItemsNumberOfItems;
            }

            //Add to the chart
            for (int i = 0; i < wantedNumber; i++)
            {                
                chrtMain.Series["Menu Items"].Points.AddXY(popularMenuItems.Keys.ToList()[i], popularMenuItems.Values.ToList()[i]);
            }

            //Determine Max Y Value (Scale the graph according to the data that is showing)
            double maxYValue = Convert.ToDouble(popularMenuItems.Values.ToList()[0]) + 1;
            while (maxYValue % 5 != 0)
            {
                maxYValue++;
            }                       

            chrtMain.ChartAreas["ChartArea1"].AxisY.Maximum = maxYValue;
            chrtMain.ChartAreas["ChartArea1"].AxisY.Minimum = 0;
        }

        private void DisplayIncomeOrCompletionTimeData(string compare, int numberOfItemsWanted)
        {
            ClearChart();

            string statName = cmbChart.SelectedItem.ToString();

            if (statName == "Income")
            {
                chrtMain.Titles["title"].Text = "Income Data";
            }
            else
            {
                chrtMain.Titles["title"].Text = "Order Completion Averages";
            }            

            chrtMain.ChartAreas["ChartArea1"].AxisX.Enabled = AxisEnabled.True;
            string seriesName = compare.Substring(0, compare.Length - 2).Replace("i", "y");
            chrtMain.Series.Add(seriesName);

            lblBarChart.Text = "Nothing Selected...";

            Dictionary<Tuple<DateTime, DateTime>, double> dictionaryToUse = new Dictionary<Tuple<DateTime, DateTime>, double>();

            switch (compare)
            {
                case "Daily":
                    if (statName == "Income")
                    {
                        dictionaryToUse = incomeDataDaily;
                    }
                    else
                    {
                        dictionaryToUse = completionTimeDaily;
                    }
                    
                    for (int i = 30 - numberOfItemsWanted; i < 30; i++)
                    {
                        chrtMain.Series[seriesName].Points.AddXY(dictionaryToUse.Keys.ToList()[i].Item1.ToString("d") , Math.Round(dictionaryToUse.Values.ToList()[i], 2));
                    }
                    chrtMain.ChartAreas["ChartArea1"].AxisX.Interval = 0;
                    break;
                case "Weekly":
                    if (statName == "Income")
                    {
                        dictionaryToUse = incomeDataWeekly;
                    }
                    else
                    {
                        dictionaryToUse = completionTimeWeekly;
                    }
                    
                    for (int i = 30 - numberOfItemsWanted; i < 30; i++)
                    {
                        chrtMain.Series[seriesName].Points.AddXY(dictionaryToUse.Keys.ToList()[i].Item1.ToString("d") + " - " + dictionaryToUse.Keys.ToList()[i].Item2.ToString("d"), Math.Round(dictionaryToUse.Values.ToList()[i] , 2));
                    }
                    chrtMain.ChartAreas["ChartArea1"].AxisX.Interval = 0;
                    break;
                case "Monthly":
                    if (statName == "Income")
                    {
                        dictionaryToUse = incomeDataMonthly;
                    }
                    else
                    {
                        dictionaryToUse = completionTimeMonthly;
                    }
                    
                    for (int i = 30 - numberOfItemsWanted; i < 30; i++)
                    {                        
                        chrtMain.Series[seriesName].Points.AddXY(dictionaryToUse.Keys.ToList()[i].Item1.ToString("MMMM") , Math.Round(dictionaryToUse.Values.ToList()[i], 2));
                    }
                    chrtMain.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                    break;
                case "Yearly":
                    if (statName == "Income")
                    {
                        dictionaryToUse = incomeDataYearly;
                    }
                    else
                    {
                        dictionaryToUse = completionTimeYearly;
                    }
                    
                    for (int i = 0; i < numberOfItemsWanted; i++)
                    {                        
                        chrtMain.Series[seriesName].Points.AddXY(dictionaryToUse.Keys.ToList()[i].Item1.Year, Math.Round(dictionaryToUse.Values.ToList()[i], 2));
                    }
                    chrtMain.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                    break;
            }

            //Determine Max Y Value (Scale the graph according to the data that is showing)
            double maxYValue = (int)Convert.ToDouble(dictionaryToUse.Values.Max() + 1);
            while (Convert.ToInt32(maxYValue) % 5 != 0)
            {
                maxYValue++;
            }

            chrtMain.ChartAreas["ChartArea1"].AxisY.Maximum = maxYValue;
            chrtMain.ChartAreas["ChartArea1"].AxisY.Minimum = 0;            
        }
        
        DataPoint previousClickedDataPoint = null;

        private void chrtMain_MouseClick(object sender, MouseEventArgs e)
        {
            int index = -1;
            var chartClick = chrtMain.HitTest(e.X, e.Y);

            if (chartClick.ChartElementType == ChartElementType.DataPoint)
            {
                if (previousClickedDataPoint != null)
                {
                    previousClickedDataPoint.Color = chrtMain.Series[0].Color;
                }

                DataPoint dataPoint = (DataPoint)chartClick.Object;
                dataPoint.Color = Color.Red;
                index = chartClick.PointIndex;

                previousClickedDataPoint = dataPoint;
            }

            switch (cmbChart.SelectedItem)
            {
                case "Popular Items":                                                            
                    if (index != -1)
                    {
                        lblBarChart.Text = popularMenuItems.Keys.ToList()[index] + "\n" + popularMenuItems.Values.ToList()[index];
                    }
                    break;
                case "Income":
                    if (index != -1)
                    {
                        index = index + (30 - incomeNumberOfItems);
                        switch (cmbCompare.SelectedItem)
                        {
                            case "Daily":
                                lblBarChart.Text = "£" + incomeDataDaily.Values.ToList()[index].ToString("N2") + "\n" + incomeDataDaily.Keys.ToList()[index].Item1.ToString("d");
                                break;
                            case "Weekly":
                                lblBarChart.Text = "£" + incomeDataWeekly.Values.ToList()[index].ToString("N2") + "\n" + incomeDataWeekly.Keys.ToList()[index].Item1.ToString("d") +
                                                   " - " + incomeDataWeekly.Keys.ToList()[index].Item2.ToString("d");
                                break;
                            case "Monthly":
                                lblBarChart.Text = "£" + incomeDataMonthly.Values.ToList()[index].ToString("N2") + "\n" + incomeDataMonthly.Keys.ToList()[index].Item1.ToString("MMMM");
                                break;
                            case "Yearly":
                                index = index - (30 - incomeNumberOfItems);
                                lblBarChart.Text = "£" + incomeDataYearly.Values.ToList()[index].ToString("N2") + "\n" + incomeDataYearly.Keys.ToList()[index].Item1.Year;
                                break;
                        }                        
                    }                    
                    break;                                        
                case "Order Completion Times":
                    if (index != -1)
                    {
                        index = index + (30 - completionTimeNumberOfItems);
                        switch (cmbCompare.SelectedItem)
                        {
                            case "Daily":
                                lblBarChart.Text = Convert.ToInt32(completionTimeDaily.Values.ToList()[index]) + " mins\n" + completionTimeDaily.Keys.ToList()[index].Item1.ToString("d");
                                break;
                            case "Weekly":
                                lblBarChart.Text = Convert.ToInt32(completionTimeWeekly.Values.ToList()[index]) + " mins\n" + completionTimeWeekly.Keys.ToList()[index].Item1.ToString("d") +
                                                   " - " + completionTimeWeekly.Keys.ToList()[index].Item2.ToString("d");
                                break;
                            case "Monthly":
                                lblBarChart.Text = Convert.ToInt32(completionTimeMonthly.Values.ToList()[index]) + " mins\n" + completionTimeMonthly.Keys.ToList()[index].Item1.ToString("MMMM");
                                break;
                            case "Yearly":
                                index = index - (30 - completionTimeNumberOfItems);
                                lblBarChart.Text = Convert.ToInt32(completionTimeYearly.Values.ToList()[index]) + " mins\n" + completionTimeYearly.Keys.ToList()[index].Item1.Year;
                                break;
                        }
                    }
                    break;
            }                      
        }

        private void txtShowTop_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((sender as TextBox).Text.Length == 2)
            {
                if (!char.IsControl(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void cmbChart_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigureSettingsDependingOnChart();
            switch ((sender as ComboBox).SelectedItem)
            {
                case "Popular Items":                    
                    DisplayPopularItems(popularItemsNumberOfItems);
                    break;
                case "Income":                    
                    DisplayIncomeOrCompletionTimeData(incomeCompare, incomeNumberOfItems);
                    break;
                case "Order Completion Times":
                    DisplayIncomeOrCompletionTimeData(completionTimeCompare, completionTimeNumberOfItems);
                    break;                                    
            }
        }        

        private void ConfigureSettingsDependingOnChart()
        {
            switch (cmbChart.SelectedItem)
            {
                case "Popular Items":                    
                    cmbCompare.Enabled = false; 

                    cmbTimePeriod.Enabled = true;
                    cmbTimePeriod.SelectedItem = popularItemsTimePeriod; 

                    txtNumberOfItems.Enabled = true;
                    txtNumberOfItems.Text = popularItemsNumberOfItems.ToString();
                    break;
                case "Income":
                    cmbCompare.Enabled = true;
                    cmbCompare.SelectedItem = incomeCompare;

                    cmbTimePeriod.Enabled = false;                    
                    
                    txtNumberOfItems.Enabled = true;
                    txtNumberOfItems.Text = incomeNumberOfItems.ToString();
                    break;
                case "Order Completion Times":                    
                    cmbCompare.Enabled = true;
                    cmbCompare.SelectedItem = completionTimeCompare;

                    cmbTimePeriod.Enabled = false;

                    txtNumberOfItems.Enabled = true;
                    txtNumberOfItems.Text = completionTimeNumberOfItems.ToString();
                    break;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ClearChart();

            if (Convert.ToInt32(txtNumberOfItems.Text) > 30)
            {
                txtNumberOfItems.Text = "30";                
            }

            switch (cmbChart.SelectedItem)
            {
                case "Popular Items":
                    //Set the current settings
                    popularItemsTimePeriod = cmbTimePeriod.SelectedItem.ToString();
                    popularItemsNumberOfItems = Convert.ToInt32(txtNumberOfItems.Text);       
                    //************************
                    GetPopularMenuItems(popularItemsTimePeriod);
                    DisplayPopularItems(popularItemsNumberOfItems);
                    break;
                case "Income":
                    //Set the current settings
                    incomeCompare = cmbCompare.SelectedItem.ToString();
                    incomeNumberOfItems = Convert.ToInt32(txtNumberOfItems.Text);
                    //************************
                    DisplayIncomeOrCompletionTimeData(incomeCompare, incomeNumberOfItems);
                    break;
                case "Order Completion Times":
                    //Set the current settings
                    completionTimeCompare = cmbCompare.SelectedItem.ToString();
                    completionTimeNumberOfItems = Convert.ToInt32(txtNumberOfItems.Text);
                    //************************
                    DisplayIncomeOrCompletionTimeData(completionTimeCompare, completionTimeNumberOfItems);
                    break;
            }
        }

        private DateTime GetDateTimeFromTimePeriod(string timePeriod)
        {
            DateTime now = new DateTime();
            now = DateTime.Now;

            DateTime resultDate = new DateTime();
            switch (timePeriod)
            {
                case "1 Week":
                    resultDate = now.AddDays(-7);
                    break;
                case "2 Weeks":
                    resultDate = now.AddDays(-14);
                    break;
                case "1 Month":
                    resultDate = now.AddMonths(-1);
                    break;
                case "2 Months":
                    resultDate = now.AddMonths(-2);
                    break;
                case "All":
                    resultDate = Convert.ToDateTime("01/01/0001");
                    break;
            }

            return resultDate;
        }

        private void ClearChart()
        {
            chrtMain.Series.Clear();            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Adjusting the chart for saving as image
            bool wasEnabled = false;
            double previousInterval = -1;
            Tuple<int, int> previousSize = new Tuple<int, int>(-1,-1);
            int previousXAxisFontSize = -1;
            pnlMain.Visible = false;            
            AdjustChartForImage(ref wasEnabled, ref previousInterval, ref previousSize, ref previousXAxisFontSize);            
            lblSaveChart.Visible = true;

            string fileName = cmbChart.SelectedItem.ToString().Replace(" ", "");            
            if (cmbChart.SelectedItem.ToString() == "Popular Items")
            {                
                fileName = fileName + "_Top" + popularItemsNumberOfItems + ".jpg";                                                
            }
            else if (cmbChart.SelectedItem.ToString() == "Income")
            {
                fileName = fileName + "_Top" + incomeNumberOfItems + "_" + incomeCompare + ".jpg";                
            }

            Bitmap chartImage = new Bitmap(chrtMain.Width, chrtMain.Height);
            chrtMain.DrawToBitmap(chartImage,new Rectangle(new Point(0,0) , new Size(chrtMain.Width, chrtMain.Height)));            

            //chrtMain.SaveImage(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), ChartImageFormat.Png);            

            SaveFileDialog sfDialog = new SaveFileDialog();
            sfDialog.Filter = "JPeg Image|*.jpg|All files|*.*";
            sfDialog.Title = "Save The Chart";
            sfDialog.FileName = fileName;
            sfDialog.ShowDialog();

            if (sfDialog.FileName != "")
            {
                FileStream fs = (FileStream)sfDialog.OpenFile();

                switch (sfDialog.FilterIndex)
                {
                    case 1:
                        chartImage.Save(fs, ImageFormat.Jpeg);
                        break;
                    case 2:
                        chartImage.Save(fs, ImageFormat.Bmp);
                        break;
                }
                fs.Close();
            }

            //Changing chart back to normal            
            ChangeChartBackToNormal(wasEnabled, previousInterval, previousSize, previousXAxisFontSize);
            pnlMain.Visible = true;
            lblSaveChart.Visible = false;                        
        }

        private void ChangeChartBackToNormal(bool wasEnabled, double previousInterval, Tuple<int, int> previousSize, int previousXAxisFontSize)
        {
            chrtMain.ChartAreas["ChartArea1"].AxisX.LabelAutoFitMaxFontSize = previousXAxisFontSize;

            //The Size of Chart
            chrtMain.Size = new Size(previousSize.Item1, previousSize.Item2);

            //X Axis
            if (wasEnabled)
            {
                chrtMain.ChartAreas["ChartArea1"].AxisX.Enabled = AxisEnabled.True;
            }
            else
            {
                chrtMain.ChartAreas["ChartArea1"].AxisX.Enabled = AxisEnabled.False;
            }
            chrtMain.ChartAreas["ChartArea1"].AxisX.Interval = previousInterval;
        }

        private void AdjustChartForImage(ref bool wasEnabled, ref double previousInterval, ref Tuple<int, int> previousSize, ref int previousXAxisFontSize)
        {
            previousXAxisFontSize = chrtMain.ChartAreas["ChartArea1"].AxisX.LabelAutoFitMaxFontSize;

            chrtMain.ChartAreas["ChartArea1"].AxisX.LabelAutoFitMaxFontSize = 25;

            //The Size of chart
            previousSize = Tuple.Create(chrtMain.Width, chrtMain.Height);

            chrtMain.Size = new Size(chrtMain.Width * 2, chrtMain.Height * 2);
            
            //X Axis
            if (chrtMain.ChartAreas["ChartArea1"].AxisX.Enabled == AxisEnabled.True)
            {
                wasEnabled = true;
            }
            else
            {
                wasEnabled = false;
            }
            previousInterval = chrtMain.ChartAreas["ChartArea1"].AxisX.Interval;

            chrtMain.ChartAreas["ChartArea1"].AxisX.Enabled = AxisEnabled.True;
            chrtMain.ChartAreas["ChartArea1"].AxisX.Interval = 1;            
        }

        private void StatisticsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
