﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Till_Machine
{
    class OrderViewUI
    {
        Queue<Order> orderQueue = new Queue<Order>();

        //The Dictionary which has the ID as key and it's name and the price as the value
        Dictionary<int, Tuple<string, double>> MenuItemsAndPrices = new Dictionary<int, Tuple<string, double>>();

        ManageOrders ManageOrders;
        SavedOrders SavedOrders;

        bool toShowSavedOrders;

        public OrderViewUI(Dictionary<int, Tuple<string, double>> MenuItemsAndPrices_ , ManageOrders manageOrders_, SavedOrders savedOrders_)
        {
            MenuItemsAndPrices = MenuItemsAndPrices_;

            ManageOrders = manageOrders_;
            SavedOrders = savedOrders_;
        }

        private void DeclareEvent(Control control)
        {
            if (!toShowSavedOrders) //Not Saved
            {
                control.Click += ManageOrders.Order_Click;
            }
            else
            {
                control.Click += SavedOrders.Order_Click;
            }
        }

        public void UpdateFlowLayoutPanel(FlowLayoutPanel flpToUpdate)
        {
            for (int i = 1; i < orderQueue.Count + 1; i++)
            {
                int intOrderNumber = GetOrderNumberFromOrderDate(orderQueue.ToList()[i - 1].orderDate);
                string savedOrderName = "";

                FlowLayoutPanel order = new FlowLayoutPanel();
                order.Parent = flpToUpdate;
                order.Name = "flpOrder";
                if (!toShowSavedOrders)
                {
                    order.Tag = intOrderNumber;                                          
                }
                else
                {
                    savedOrderName = GetNameOfSavedOrder(orderQueue.ToList()[i - 1].orderID);
                    order.Tag = savedOrderName;
                }
                order.Margin = new Padding(10, 2, 10, 2);
                order.MaximumSize = new Size(210, 0);
                order.WrapContents = false;
                order.AutoSize = true;
                order.FlowDirection = FlowDirection.TopDown;
                DeclareEvent(order);

                orderQueue.ElementAt(i - 1).flpOrder = order;

                Label orderNumber = new Label();
                orderNumber.Name = "orderNumber";
                if (!toShowSavedOrders)
                {
                    orderNumber.Text = "Order " + intOrderNumber;//
                    orderNumber.Font = new Font("Microsoft Sans Serif", 32);
                }
                else
                {                    
                    orderNumber.Font = new Font("Microsoft Sans Serif", 28);
                    orderNumber.Text = savedOrderName;
                }                
                orderNumber.TextAlign = ContentAlignment.MiddleCenter;
                orderNumber.Parent = order;
                orderNumber.BackColor = Color.Gray;
                orderNumber.Margin = new Padding(2, 2, 2, 2);
                orderNumber.Size = new Size(200, 46);
                
                DeclareEvent(orderNumber);

                order.Controls.Add(orderNumber);

                //Listing The Items
                Order currentOrder = orderQueue.ElementAt(i - 1);
                int numberOfItemsToShow = 0;
                bool moreItems = false;
                if (currentOrder.items.Count > 2)
                {
                    numberOfItemsToShow = 2;
                    moreItems = true;

                }
                else
                {
                    numberOfItemsToShow = currentOrder.items.Count;
                }

                for (int a = 0; a < numberOfItemsToShow; a++)
                {
                    FlowLayoutPanel item = new FlowLayoutPanel();
                    item.Name = "orderItems";
                    item.Parent = order;
                    item.Margin = new Padding(2, 2, 2, 2);
                    item.BackColor = Color.Transparent;
                    item.BorderStyle = BorderStyle.FixedSingle;
                    item.FlowDirection = FlowDirection.TopDown;
                    item.MinimumSize = new Size(200, 0);
                    item.MaximumSize = new Size(200, 0);
                    item.AutoSize = true;
                    item.Anchor = AnchorStyles.Left;
                    DeclareEvent(item);

                    //Menu Item
                    Label line = new Label();
                    line.Margin = new Padding(0, 2, 0, 2);
                    line.Tag = "OrderItem";
                    line.Parent = item;
                    line.Text = currentOrder.items[a].quantity + "x " + (currentOrder.items[a]).menuItem;
                    line.Font = new Font("Microsoft Sans Serif", 20);
                    line.AutoSize = true;
                    line.MinimumSize = new Size(200, 0);
                    DeclareEvent(line);

                    item.Controls.Add(line);

                    //Toppings
                    List<string> toppingsList = currentOrder.items[a].toppings;
                    if (toppingsList.Count != 0)
                    {
                        for (int b = 0; b < toppingsList.Count; b++)
                        {
                            Label topping = new Label();
                            topping.Parent = item;
                            topping.Name = "Topping";
                            topping.Margin = new Padding(0, 2, 0, 2);
                            //topping.BackColor = Color.Green;
                            topping.Size = new Size(200, 20);
                            topping.Font = new Font("Microsoft Sans Serif", 12);
                            topping.Text = "+ " + toppingsList[b];
                            DeclareEvent(topping);

                            item.Controls.Add(topping);
                        }
                    }

                    order.Controls.Add(item);
                }

                if (moreItems) //If some items are not shown
                {
                    Label moreItemsUI = new Label();
                    moreItemsUI.Size = new Size(200, 22);
                    moreItemsUI.Font = new Font("Microsoft Sans Serif", 23);
                    moreItemsUI.BackColor = Color.Transparent;
                    moreItemsUI.Margin = new Padding(2, 2, 2, 2);
                    moreItemsUI.Text = "•••";
                    moreItemsUI.TextAlign = ContentAlignment.TopCenter;
                    DeclareEvent(moreItemsUI);

                    order.Controls.Add(moreItemsUI);
                }

                if (!toShowSavedOrders)
                {
                    //Timer
                    Label timerOrder = new Label();
                    timerOrder.Name = "timerOrder";
                    timerOrder.TabIndex = 0; //So that this is found quickest when the timer needs to be updated
                    timerOrder.Parent = order;
                    timerOrder.BackColor = Color.LightGray;
                    timerOrder.Margin = new Padding(2, 2, 2, 2);
                    timerOrder.Size = new Size(200, 46);
                    timerOrder.Font = new Font("Microsoft Sans Serif", 28);
                    int mins = CalculateTheTimePast(orderQueue, i - 1); //Because the for loop starts with 1  
                    timerOrder.Text = mins + " Mins";
                    timerOrder.TextAlign = ContentAlignment.MiddleLeft;
                    DeclareEvent(timerOrder);

                    order.Controls.Add(timerOrder);                    
                }

                if (i == 4)
                {
                    flpToUpdate.SetFlowBreak(order, true);
                }
            }
        }

        private string GetNameOfSavedOrder(int orderID)
        {
            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "SELECT savedOrderName" +
                            " FROM Orders" +
                            " WHERE(orderID = " + orderID + ")";
            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            string nameOfSavedOrder = "";
            while (dr.Read())
            {
                nameOfSavedOrder = dr[0].ToString();
            }
            dBConnector.Close();

            return nameOfSavedOrder;
        }

        private int GetOrderNumberFromOrderDate(DateTime orderDate)
        {
            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "SELECT COUNT(*)" +
                            " FROM Orders" +
                            " WHERE(DateValue(orderDate) = date())" +
                            " AND(isSaved = False)" +
                            " AND(TimeValue(orderDate) < '" + orderDate.TimeOfDay +"' )";

            int numberOfOrders = -1;
            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                numberOfOrders = Convert.ToInt32(dr[0]) + 1;
            }
            dBConnector.Close();

            return numberOfOrders;
        }

        public Queue<Order> InitializeOrderQueue(bool needSavedOrders)
        {
            toShowSavedOrders = needSavedOrders;

            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "";

            if (needSavedOrders)
            {
                sqlStr = "SELECT orderID, orderDate, isDisabled, isUpdated" +
                            " FROM Orders" +
                            " WHERE(isComplete = False) " +                            
                            " AND(isSaved = " + needSavedOrders + ")" +
                            " AND(isDisabled = False)" +
                            " ORDER BY orderDate";
            }
            else
            {
                sqlStr = "SELECT orderID, orderDate, isDisabled, isUpdated" +
                            " FROM Orders" +
                            " WHERE(isComplete = False) " +
                            " AND(DateValue(orderDate) = date())" +
                            " AND(isSaved = " + needSavedOrders + ")" +
                            " ORDER BY orderDate";
            }            

            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                int orderID = Convert.ToInt32(dr[0]);
                DateTime orderDate = Convert.ToDateTime(dr[1]);
                bool isDisabled = Convert.ToBoolean(dr[2]);
                bool isUpdated = Convert.ToBoolean(dr[3]);

                Order order = GetOrderItems(orderID, orderDate, isDisabled, isUpdated);
                orderQueue.Enqueue(order);
            }
            dBConnector.Close();

            return orderQueue;
        }

        private Order GetOrderItems(int orderID, DateTime orderDate, bool isDisabled, bool isUpdated)
        {
            clsDBConnector dBConnector = new clsDBConnector();

            List<OrderItem> orderItems = new List<OrderItem>();

            OleDbDataReader dr2;

            string sqlStr2 = "SELECT [Order Items].orderItemID, [Order Items].menuItemID, [Order Items].Quantity" +
                        " FROM(Orders INNER JOIN" +
                        " [Order Items] ON Orders.orderID = [Order Items].orderID)" +
                        " WHERE([Order Items].orderID = " + orderID + ")" + //Requesting orderItems with the currnet Order ID
                        " ORDER BY[Order Items].orderItemID";
            dBConnector.Connect();
            dr2 = dBConnector.DoSQL(sqlStr2);

            while (dr2.Read())
            {
                List<string> toppings = new List<string>();

                OleDbDataReader dr3;

                string sqlStr3 = "SELECT menuItemID" +
                                    " FROM Toppings" +
                                    " WHERE(orderItemID = " + dr2[0].ToString() + ")"; //Requesting toppings with the current order Item ID

                dr3 = dBConnector.DoSQL(sqlStr3);

                while (dr3.Read())
                {
                    toppings.Add(MenuItemsAndPrices[Convert.ToInt32(dr3[0])].Item1);
                }

                OrderItem newOrderItem = new OrderItem(Convert.ToInt32(dr2[0]), MenuItemsAndPrices[Convert.ToInt32(dr2[1])].Item1, toppings, Convert.ToInt32(dr2[2]));

                orderItems.Add(newOrderItem);
            }
            dBConnector.Close();

            Order newOrder = new Order(Convert.ToInt32(orderID), Convert.ToDateTime(orderDate), orderItems, isDisabled, isUpdated);

            return newOrder;
        }

        private int CalculateTheTimePast(Queue<Order> orderQueue, int orderIndex)
        {
            TimeSpan timeDifference = DateTime.Now.Subtract(Convert.ToDateTime(orderQueue.ToList()[orderIndex].orderDate));

            int minsPast = Convert.ToInt32(Math.Floor(timeDifference.TotalMinutes));

            return minsPast;
        }

        public void AnimationTick(ref int animationCount, PictureBox pcbRefreshIcon, System.Resources.ResourceManager RM, Timer tmrAnimation, bool bkwComplete)
        {
            if (animationCount <= 33)
            {
                if (bkwComplete == false && animationCount < 25)
                {
                    UpdateTheImageAccordingAnimationCount(animationCount,pcbRefreshIcon,RM);
                    animationCount++;
                }
                else if (bkwComplete == false && animationCount == 25)
                {
                    UpdateTheImageAccordingAnimationCount(animationCount, pcbRefreshIcon, RM);
                    animationCount = 2;
                }
                else if (bkwComplete == true)
                {
                    UpdateTheImageAccordingAnimationCount(animationCount, pcbRefreshIcon, RM);
                    animationCount++;
                }
            }
            else
            {
                if (animationCount != 99)
                {
                    animationCount = 99;
                    tmrAnimation.Interval = 1000;
                    pcbRefreshIcon.Visible = false;
                }
                else
                {
                    animationCount = 1;
                    tmrAnimation.Interval = 25;
                    tmrAnimation.Enabled = false;
                    pcbRefreshIcon.Image = (Image)RM.GetObject("Refresh_Icon");
                    pcbRefreshIcon.Visible = true;
                }
            }

            if (animationCount % 10 == 0)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private void UpdateTheImageAccordingAnimationCount(int animationCount, PictureBox pcbRefreshIcon, System.Resources.ResourceManager RM)
        {
            if (animationCount != 25)
            {
                pcbRefreshIcon.Image = (Image)RM.GetObject("_" + animationCount.ToString());
            }
            else
            {
                pcbRefreshIcon.Image = (Image)RM.GetObject("_1");
            }
        }
    }    
}
