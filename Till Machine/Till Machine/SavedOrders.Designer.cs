﻿
namespace Till_Machine
{
    partial class SavedOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SavedOrders));
            this.btnReload = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.flpOrderView = new System.Windows.Forms.FlowLayoutPanel();
            this.lblNoOrdersMessage = new System.Windows.Forms.Label();
            this.btnView = new System.Windows.Forms.Button();
            this.lblInformation = new System.Windows.Forms.Label();
            this.pcbRefreshIcon = new System.Windows.Forms.PictureBox();
            this.tmrAnimation = new System.Windows.Forms.Timer(this.components);
            this.lblRefreshInfo = new System.Windows.Forms.Label();
            this.bkwRefresh = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pcbRefreshIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReload
            // 
            this.btnReload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReload.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReload.Location = new System.Drawing.Point(12, 12);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(178, 43);
            this.btnReload.TabIndex = 2;
            this.btnReload.Text = "Reload Order";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(794, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(178, 43);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete Order";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // flpOrderView
            // 
            this.flpOrderView.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.flpOrderView.AutoScroll = true;
            this.flpOrderView.Location = new System.Drawing.Point(12, 76);
            this.flpOrderView.MaximumSize = new System.Drawing.Size(960, 473);
            this.flpOrderView.MinimumSize = new System.Drawing.Size(960, 473);
            this.flpOrderView.Name = "flpOrderView";
            this.flpOrderView.Size = new System.Drawing.Size(960, 473);
            this.flpOrderView.TabIndex = 5;
            // 
            // lblNoOrdersMessage
            // 
            this.lblNoOrdersMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblNoOrdersMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblNoOrdersMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOrdersMessage.ForeColor = System.Drawing.Color.Gray;
            this.lblNoOrdersMessage.Location = new System.Drawing.Point(0, 447);
            this.lblNoOrdersMessage.MinimumSize = new System.Drawing.Size(325, 75);
            this.lblNoOrdersMessage.Name = "lblNoOrdersMessage";
            this.lblNoOrdersMessage.Size = new System.Drawing.Size(984, 114);
            this.lblNoOrdersMessage.TabIndex = 6;
            this.lblNoOrdersMessage.Text = "No Saved Orders";
            this.lblNoOrdersMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnView
            // 
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Location = new System.Drawing.Point(196, 12);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(178, 43);
            this.btnView.TabIndex = 7;
            this.btnView.Text = "View Full Order";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblInformation
            // 
            this.lblInformation.AutoSize = true;
            this.lblInformation.Location = new System.Drawing.Point(12, 60);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(195, 13);
            this.lblInformation.TabIndex = 8;
            this.lblInformation.Text = "Right click an order to change its name.";
            // 
            // pcbRefreshIcon
            // 
            this.pcbRefreshIcon.Image = global::Till_Machine.Properties.Resources.Refresh_Icon;
            this.pcbRefreshIcon.InitialImage = global::Till_Machine.Properties.Resources.Refresh_Icon;
            this.pcbRefreshIcon.Location = new System.Drawing.Point(890, 500);
            this.pcbRefreshIcon.Name = "pcbRefreshIcon";
            this.pcbRefreshIcon.Size = new System.Drawing.Size(50, 50);
            this.pcbRefreshIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbRefreshIcon.TabIndex = 10;
            this.pcbRefreshIcon.TabStop = false;
            this.pcbRefreshIcon.Click += new System.EventHandler(this.pcbRefreshIcon_Click);
            // 
            // tmrAnimation
            // 
            this.tmrAnimation.Interval = 25;
            this.tmrAnimation.Tick += new System.EventHandler(this.tmrAnimation_Tick);
            // 
            // lblRefreshInfo
            // 
            this.lblRefreshInfo.AutoSize = true;
            this.lblRefreshInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRefreshInfo.Location = new System.Drawing.Point(380, 12);
            this.lblRefreshInfo.Name = "lblRefreshInfo";
            this.lblRefreshInfo.Size = new System.Drawing.Size(89, 16);
            this.lblRefreshInfo.TabIndex = 11;
            this.lblRefreshInfo.Text = "Last Refresh: ";
            // 
            // bkwRefresh
            // 
            this.bkwRefresh.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkwRefresh_DoWork);
            this.bkwRefresh.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bkwRefresh_RunWorkerCompleted);
            // 
            // SavedOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.lblRefreshInfo);
            this.Controls.Add(this.pcbRefreshIcon);
            this.Controls.Add(this.lblInformation);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.lblNoOrdersMessage);
            this.Controls.Add(this.flpOrderView);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnReload);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1000, 600);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "SavedOrders";
            this.Text = "SavedOrders";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SavedOrders_FormClosed);
            this.Load += new System.EventHandler(this.SavedOrders_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcbRefreshIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.FlowLayoutPanel flpOrderView;
        private System.Windows.Forms.Label lblNoOrdersMessage;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Label lblInformation;
        private System.Windows.Forms.PictureBox pcbRefreshIcon;
        private System.Windows.Forms.Timer tmrAnimation;
        private System.Windows.Forms.Label lblRefreshInfo;
        private System.ComponentModel.BackgroundWorker bkwRefresh;
    }
}