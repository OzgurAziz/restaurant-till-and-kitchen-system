﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Till_Machine
{
    public partial class SettingsForm : Form
    {
        Main mainForm;

        SettingsCls settings = new SettingsCls(Properties.Settings.Default.askNote, Properties.Settings.Default.AccentColor);

        bool menuChanged = false;

        public SettingsForm(Main mainForm_)
        {
            mainForm = mainForm_;

            InitializeComponent();

            this.FormClosing += SettingsForm_FormClosing;
            this.FormClosed += SettingsForm_FormClosed;
        }

        private void SettingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainForm.Enabled = true;

            if (menuChanged)
            {
                mainForm.MenuChanged(this);
            }

            this.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool isSaved = settings.AreSettingsSaved();            

            if (!isSaved) //If customisations are not the same
            {
                DialogResult result = MessageBox.Show(this, "There are unsaved changes! \nIf you exit now they will not be saved. " +
                    "\n\nWould you still like to exit?", "Warning!", MessageBoxButtons.YesNo);

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            mainForm.Enabled = false;
            
            UpdateCustomisations();
            AddListeners();            

            btnSave.Text = "Save Changes";

            InitializeMenuCategoryAndOptions();
            DisplayMenuCategories();
            CheckIfThereAreCurrentOrders();
            ConfigureMenuButtons();
            if (menuCanChange == false)
            {
                lblSubtextMenu.Visible = true;
            }

            //Instantiate Menu Lock
            if (Properties.Settings.Default.menuLocked)
            {
                btnMenuLock.Text = "Unlock Menu";
                pcbMenuLock.Image = Properties.Resources.Locked_Icon;
            }
            else
            {
                btnMenuLock.Text = "Lock Menu";
                pcbMenuLock.Image = Properties.Resources.Unlocked_Icon;
            }
        }

        private void AddListeners()
        {
            cmbAskNote.SelectedIndexChanged += new System.EventHandler(SettingsChanged);
            cmbAccentColor.SelectedIndexChanged += new System.EventHandler(SettingsChanged);
        }

        bool menuCanChange = true; //set to false if there are current uncompleted orders today

        private void CheckIfThereAreCurrentOrders()
        {
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "SELECT COUNT(orderID) AS Expr1" +
                            " FROM Orders" +
                            " WHERE(isComplete = false)" +
                            " AND(isSaved = false)" +
                            " AND (DateValue(orderDate) = '" + DateTime.Now.Date.ToString("d") + "')";

            dbConnector.Connect();
            dr = dbConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                if (Convert.ToInt32(dr[0]) > 0)
                {
                    menuCanChange = false;
                }
            }

            sqlStr = "SELECT COUNT(orderID) AS Expr1" +
                            " FROM Orders" +
                            " WHERE(isComplete = false)" +
                            " AND(isSaved = true)" +
                            " AND(isDisabled = false)";            
            dr = dbConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                if (Convert.ToInt32(dr[0]) > 0)
                {
                    menuCanChange = false;
                }
            }
            dbConnector.Close();
        }

        //the category name is stored as key and the items are stored as list as a value
        Dictionary<string, List<string>> menuCategoryAndOptions = new Dictionary<string, List<string>>();

        //the menu item (menu option) stored as key and price as value
        Dictionary<string, double> menuOptionsAndPrices = new Dictionary<string, double>();

        List<SpecialOffer> specialOffers = new List<SpecialOffer>();

        string currentSelectedCategory = "";
        string currentSelectedOption = "";

        private void InitializeMenuCategoryAndOptions()
        {
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;


            //Categories
            string sqlStr = "SELECT Categories.Category" +
                            " FROM(Categories)" +
                            " WHERE isDeleted = false" +
                            " ORDER BY (categoryID)";

            dbConnector.Connect();
            dr = dbConnector.DoSQL(sqlStr);

            while (dr.Read())
            {
                menuCategoryAndOptions.Add(dr[0].ToString(), new List<string>());                
            }

            //Menu Items
            sqlStr = "SELECT Categories.Category, [Menu Items].Name , [Menu Items].Cost" +
                            " FROM(Categories INNER JOIN" +
                            " [Menu Items] ON Categories.categoryID = [Menu Items].categoryID)" +
                            " WHERE Categories.isDeleted = false" +
                            " AND [Menu Items].isDeleted = false";
            dr = dbConnector.DoSQL(sqlStr);

            while (dr.Read())
            {
                menuCategoryAndOptions[dr[0].ToString()].Add(dr[1].ToString());

                menuOptionsAndPrices.Add(dr[1].ToString(), Convert.ToDouble(dr[2]));
            }

            //Special Offers
            sqlStr = "SELECT Name, Cost, startDate, endDate"+
                    " FROM[Menu Items]" +
                    " WHERE(isDeleted = false) AND(startDate IS NOT NULL) AND(endDate IS NOT NULL)";
            dr = dbConnector.DoSQL(sqlStr);

            while (dr.Read())
            {
                specialOffers.Add(new SpecialOffer(dr[0].ToString(), Convert.ToDouble(dr[1]),
                                                   Convert.ToDateTime(dr[2]), Convert.ToDateTime(dr[3])));
            }

            dbConnector.Close();
        }    
        
        private void ConfigureMenuButtons()
        {
            if (Properties.Settings.Default.menuLocked || menuCanChange == false)
            {
                btnAddCategory.Enabled = false;
                btnDeleteCategory.Enabled = false;

                btnAddOption.Enabled = false;
                btnDeleteOption.Enabled = false;
                btnUpdatePrice.Enabled = false;
            }
            else
            {
                btnAddCategory.Enabled = true;

                if (currentSelectedCategory != "")
                {
                    if (currentSelectedCategory == "Topping" || currentSelectedCategory == "Special Offers")
                    {
                        btnDeleteCategory.Enabled = false;
                    }
                    else
                    {
                        btnDeleteCategory.Enabled = true;
                    }
                    btnAddOption.Enabled = true;
                }
                else
                {
                    btnDeleteCategory.Enabled = false;
                    btnAddOption.Enabled = false;
                }

                if (currentSelectedCategory != "Special Offers")
                {
                    if (currentSelectedOption != "")
                    {
                        btnDeleteOption.Enabled = true;
                        btnUpdatePrice.Enabled = true;
                    }
                    else
                    {
                        btnDeleteOption.Enabled = false;
                        btnUpdatePrice.Enabled = false;
                    }
                }
                else
                {
                    if (currentSelectedOption != "")
                    {
                        btnDeleteOption.Enabled = true;                        
                    }
                    else
                    {
                        btnDeleteOption.Enabled = false;                        
                    }

                    btnUpdatePrice.Enabled = false;
                }
                
            }

            //Display the prices no matter what
            if (currentSelectedOption != "")
            {
                if (currentSelectedCategory == "Special Offers")
                {
                    SpecialOffer spo = specialOffers.Where(x => x.name == currentSelectedOption).First();
                    lblPrice.Text = "Price: £" + menuOptionsAndPrices[currentSelectedOption].ToString("N2") +
                                    "\n" + spo.startDate.ToString("d") +  " - " + spo.endDate.ToString("d");
                }
                else
                {
                    lblPrice.Text = "Price: £" + menuOptionsAndPrices[currentSelectedOption].ToString("N2");                    
                }                
            }
            else
            {
                lblPrice.Text = "Nothing Selected";
            }

            if (currentSelectedCategory == "Special Offers")
            {
                pnlSpecialOffers.Visible = true;
            }
            else
            {
                pnlSpecialOffers.Visible = false;
            }
        }

        private void DisplayMenuCategories()
        {
            flpCategories.Controls.Clear();

            foreach (string category in menuCategoryAndOptions.Keys)
            {
                Label lblCategory = new Label();
                lblCategory.Size = new Size(97, 40);
                lblCategory.Font = new Font("Microsoft Sans Serif", 12);
                lblCategory.Name = category;
                lblCategory.Text = category;
                lblCategory.TextAlign = ContentAlignment.MiddleCenter;
                lblCategory.BackColor = Color.DarkGray;
                lblCategory.Margin = new Padding(3, 3, 3, 3);
                lblCategory.Click += LblCategory_Click;
                //Add a click event to delete and display options

                flpCategories.Controls.Add(lblCategory);
            }
        }

        private void LblCategory_Click(object sender, EventArgs e)
        {
            if (currentSelectedCategory == "")
            {
                currentSelectedCategory = (sender as Label).Text;
                (sender as Label).BackColor = Color.Gray;
                DisplayMenuOptions(currentSelectedCategory);
            }
            else if (currentSelectedCategory == (sender as Label).Text)
            {
                currentSelectedCategory = "";
                (sender as Label).BackColor = Color.DarkGray;
                DisplayMenuOptions("");
            }
            else
            {
                flpCategories.Controls.Find(currentSelectedCategory, false).First().BackColor = Color.DarkGray;

                currentSelectedCategory = (sender as Label).Text;
                (sender as Label).BackColor = Color.Gray;
                DisplayMenuOptions(currentSelectedCategory);
            }

            currentSelectedOption = "";
            ConfigureMenuButtons();
        }

        private void DisplayMenuOptions(string CategoryToDisplay)
        {            
            if (CategoryToDisplay != "")
            {
                flpOptions.Controls.Clear();

                foreach (string option in menuCategoryAndOptions[CategoryToDisplay])
                {
                    Label lblOption = new Label();
                    lblOption.Size = new Size(97, 40);
                    lblOption.Font = new Font("Microsoft Sans Serif", 12);
                    lblOption.Name = option;
                    lblOption.Text = option;
                    lblOption.TextAlign = ContentAlignment.MiddleCenter;
                    lblOption.BackColor = Color.DarkGray;
                    lblOption.Margin = new Padding(3, 3, 3, 3);
                    lblOption.Click += LblOption_Click;


                    //Changing the color of the text if showing a special offer
                    if (CategoryToDisplay == "Special Offers")
                    {
                        SpecialOffer spo = specialOffers.Where(x => x.name == option).First();
                        DateTime now = new DateTime();
                        now = DateTime.Now;
                        if (spo.startDate.Date <= now.Date && spo.endDate >= now.Date)
                        {
                            lblOption.ForeColor = Color.Green; //Green is is available
                        }
                        else
                        {
                            lblOption.ForeColor = Color.Red; //Red if not available
                        }                        
                    }

                    flpOptions.Controls.Add(lblOption);
                }
            }
            else
            {
                flpOptions.Controls.Clear();
            }
        }

        private void LblOption_Click(object sender, EventArgs e)
        {
            if (currentSelectedOption == "")
            {
                currentSelectedOption = (sender as Label).Text;
                (sender as Label).BackColor = Color.Gray;                
            }
            else if (currentSelectedOption == (sender as Label).Text)
            {
                currentSelectedOption = "";
                (sender as Label).BackColor = Color.DarkGray;                
            }
            else
            {
                flpOptions.Controls.Find(currentSelectedOption, false).First().BackColor = Color.DarkGray;

                currentSelectedOption = (sender as Label).Text;
                (sender as Label).BackColor = Color.Gray;                
            }

            ConfigureMenuButtons();
        }

        private void UpdateCustomisations()
        {
            if (Properties.Settings.Default.askNote == true)
            {
                cmbAskNote.SelectedItem = "Yes";                
            }
            else
            {
                cmbAskNote.SelectedItem = "No";                
            }
            
            cmbAccentColor.SelectedItem = Properties.Settings.Default.AccentColor;            
        }            

        private void SettingsChanged(object sender, EventArgs args)
        {
            if ((sender is ComboBox))
            {
                if ((sender as ComboBox).Name == "cmbAskNote")
                {
                    if ((sender as ComboBox).GetItemText((sender as ComboBox).SelectedItem) == "Yes")
                    {
                        settings.SetAskNote(true);
                    }
                    else
                    {
                        settings.SetAskNote(false);
                    }
                }
                else if ((sender as ComboBox).Name == "cmbAccentColor")
                {
                    settings.SetAccentColor((sender as ComboBox).GetItemText((sender as ComboBox).SelectedItem));
                }                
            }            

            btnSave.Text = "Save Changes*";
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            switch ((sender as PictureBox).Name)
            {
                case "pcbAskNote":
                    MessageBox.Show(this, "Whether or not the program will ask \nfor a note to be sent to the kitchen \nright after the 'Complete' button is pressed.", "Note After Completion", MessageBoxButtons.OK);
                    break;
                case "pcbAccentColor":
                    MessageBox.Show(this, "The background color of the price \nthat is shown in the menu items.", "Accent Color", MessageBoxButtons.OK);
                    break;

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!settings.AreSettingsSaved())
            {
                if (settings.settingsChanged == true)
                {
                    Properties.Settings.Default.askNote = settings.GetAskNote();
                    Properties.Settings.Default.AccentColor = settings.GetAccentColor();
                }                
                Properties.Settings.Default.Save();

                tmrAnimation.Enabled = true;
                pcbSavingIcon.Visible = true;

                btnSave.Text = "Save Changes";

                settings.SettingsSaved();
            }                        
        }

        int animationCount = 1;

        private System.Resources.ResourceManager RM = new System.Resources.ResourceManager(typeof(Properties.Resources));

        private void tmrAnimation_Tick(object sender, EventArgs e)
        {
            if (animationCount <= 33)
            {
                if (animationCount != 25)
                {
                    pcbSavingIcon.Image = (Image)RM.GetObject("_" + animationCount.ToString());
                }
                else
                {
                    pcbSavingIcon.Image = (Image)RM.GetObject("_1");
                }

                animationCount++;
            }
            else
            {
                animationCount = 1;
                tmrAnimation.Enabled = false;
                pcbSavingIcon.Visible = false;
            }

            if (animationCount % 10 == 0)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            string itemName = "";

            using (AddToMenu addToMenuForm = new AddToMenu(menuCategoryAndOptions , true, false, "The category you have entered already exists!"))
            {
                addToMenuForm.TopMost = true;
                var result = addToMenuForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    itemName = addToMenuForm.itemName;                    
                }
            }

            if (itemName != "")
            {
                clsDBConnector dBConnector = new clsDBConnector();
                dBConnector.Connect();                

                if (CheckIfThatNameExistsInDatabase(true, itemName)) //if that name already exists
                {
                    string dmlStr = "UPDATE Categories" +
                                    " SET isDeleted = false" +
                                    " WHERE(Category = '" + itemName + "')";

                    dBConnector.DoDML(dmlStr);
                    dBConnector.Close();
                }
                else
                {
                    string dmlStr = "INSERT INTO Categories" +
                                " (Category) " +
                                " VALUES('" + itemName + "')";      
                    
                    dBConnector.DoDML(dmlStr);
                    dBConnector.Close();
                }
                

                menuCategoryAndOptions.Add(itemName, new List<string>());
                
                DisplayMenuCategories();
                ConfigureMenuButtons();

                menuChanged = true;
            }            
        }

        private void btnAddOption_Click(object sender, EventArgs e)
        {
            string itemName = "";
            double itemPrice = -1;

            if (currentSelectedCategory == "Special Offers")
            {
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();

                using (AddToMenu addToMenuForm = new AddToMenu(menuCategoryAndOptions,false, true, "The item you have entered already exists!"))
                {
                    addToMenuForm.TopMost = true;
                    var result = addToMenuForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        itemName = addToMenuForm.itemName;
                        itemPrice = addToMenuForm.itemPrice;
                        startDate = addToMenuForm.startDate;
                        endDate = addToMenuForm.endDate;
                    }
                }

                if (itemName != "" && itemPrice != -1)
                {
                    clsDBConnector dBConnector = new clsDBConnector();

                    OleDbDataReader dr;

                    int categoryID = -1;

                    string sqlStr = "SELECT categoryID" +
                                    " FROM Categories" +
                                    " WHERE(Category = '" + currentSelectedCategory + "')";

                    dBConnector.Connect();
                    dr = dBConnector.DoSQL(sqlStr);
                    while (dr.Read())
                    {
                        categoryID = Convert.ToInt32(dr[0]);
                    }                    

                    if (CheckIfThatNameExistsInDatabase(false, itemName))
                    {
                        string dmlStr = "UPDATE [Menu Items]" +
                                        " SET startDate ='" + startDate + "' , endDate = '" + endDate + "', Cost = " + itemPrice + ", isDeleted = false, categoryID = " + categoryID +
                                        " WHERE(Name = '" + itemName + "')";                                    
                        
                        dBConnector.DoDML(dmlStr);                        
                    }
                    else
                    {                        
                        string dmlStr = "INSERT INTO[Menu Items]" +
                                       " (Name, startDate, endDate, Cost, categoryID)" +
                                       " VALUES('" + itemName + "', '" + startDate + "', '" + endDate + "', " + itemPrice + "," + categoryID + ")";
                        
                        dBConnector.DoDML(dmlStr);                        
                    }
                    dBConnector.Close();

                    SpecialOffer newSpecialOffer = new SpecialOffer(itemName, itemPrice, startDate, endDate);
                    specialOffers.Add(newSpecialOffer);

                    menuCategoryAndOptions[currentSelectedCategory].Add(itemName);
                    menuOptionsAndPrices.Add(itemName, itemPrice);

                    DisplayMenuOptions(currentSelectedCategory);
                    ConfigureMenuButtons();

                    menuChanged = true;
                }

            }
            else
            {
                using (AddToMenu addToMenuForm = new AddToMenu(menuCategoryAndOptions, false,false, "The item you have entered already exists!"))
                {
                    addToMenuForm.TopMost = true;
                    var result = addToMenuForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        itemName = addToMenuForm.itemName;
                        itemPrice = addToMenuForm.itemPrice;
                    }
                }

                if (itemName != "" && itemPrice != -1)
                {
                    //Get the categoryID with the name
                    clsDBConnector dBConnector = new clsDBConnector();
                    OleDbDataReader dr;

                    int categoryID = -1;

                    string sqlStr = "SELECT categoryID" +
                                    " FROM Categories" +
                                    " WHERE(Category = '" + currentSelectedCategory + "')";

                    dBConnector.Connect();
                    dr = dBConnector.DoSQL(sqlStr);
                    while (dr.Read())
                    {
                        categoryID = Convert.ToInt32(dr[0]);
                    }

                    if (CheckIfThatNameExistsInDatabase(false, itemName)) //if that name already exists
                    {
                        string dmlStr = "UPDATE [Menu Items]" +
                                        " SET isDeleted = false, categoryID = " + categoryID + ",Cost = " + itemPrice +
                                        ",startDate = NULL, endDate = NULL " +
                                        " WHERE([Menu Items].Name = '" + itemName + "')";

                        dBConnector.DoDML(dmlStr);                        
                    }
                    else
                    {
                        //Then add the items to that category
                        string dmlStr = "INSERT INTO [Menu Items]" +
                                        " (Name, categoryID, Cost)" +
                                        " VALUES('" + itemName + "'," + categoryID + ", " + itemPrice + ")";

                        dBConnector.DoDML(dmlStr);                        
                    }
                    dBConnector.Close();

                    menuCategoryAndOptions[currentSelectedCategory].Add(itemName);
                    menuOptionsAndPrices.Add(itemName, itemPrice);

                    DisplayMenuOptions(currentSelectedCategory);
                    ConfigureMenuButtons();

                    menuChanged = true;
                }
            }            
        }        

        private bool CheckIfThatNameExistsInDatabase(bool isCategory, string name)
        {
            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;
            bool itExists = false;

            if (isCategory)
            {
                string sqlStr = "SELECT COUNT(Category)" +
                                " FROM Categories" +
                                " WHERE(Category = '" + name + "')" +
                                " AND isDeleted = true";

                dBConnector.Connect();
                dr = dBConnector.DoSQL(sqlStr);                
                while (dr.Read())
                {
                    if (Convert.ToInt32(dr[0]) == 1)
                    {
                        itExists = true;
                    }
                }
                dBConnector.Close();
            }
            else
            {
                string sqlStr = "SELECT COUNT(Name)" +
                                " FROM [Menu Items]" +
                                " WHERE(Name = '" + name + "')" +
                                " AND isDeleted = true";

                dBConnector.Connect();
                dr = dBConnector.DoSQL(sqlStr);
                while (dr.Read())
                {
                    if (Convert.ToInt32(dr[0]) == 1)
                    {
                        itExists = true;
                    }                    
                }
                dBConnector.Close();
            }

            return itExists;
        }

        private void btnDeleteCategory_Click(object sender, EventArgs e)
        {            
            if (currentSelectedCategory != "Special Offers")
            {
                clsDBConnector dBConnector = new clsDBConnector();

                string dmlStr = "UPDATE Categories" +
                            " SET isDeleted = true" +
                            " WHERE(Categories.Category = '" + currentSelectedCategory + "')";

                dBConnector.Connect();
                dBConnector.DoDML(dmlStr);
                dBConnector.Close();

                menuCategoryAndOptions.Remove(currentSelectedCategory);
                currentSelectedCategory = "";
                currentSelectedOption = "";
                DisplayMenuOptions("");
                DisplayMenuCategories();
                ConfigureMenuButtons();

                menuChanged = true;
            }            
        }

        private void btnDeleteOption_Click(object sender, EventArgs e)
        {
            clsDBConnector dBConnector = new clsDBConnector();

            string dmlStr = "UPDATE [Menu Items]" +
                            " SET isDeleted = true" +
                            " WHERE([Menu Items].Name = '" + currentSelectedOption + "')";

            dBConnector.Connect();
            dBConnector.DoDML(dmlStr);
            dBConnector.Close();

            if (currentSelectedCategory == "Special Offers")
            {
                specialOffers.Remove(specialOffers.Where(x => x.name == currentSelectedOption).First());
            }
            menuCategoryAndOptions[currentSelectedCategory].Remove(currentSelectedOption);
            menuOptionsAndPrices.Remove(currentSelectedOption);

            currentSelectedOption = "";
            DisplayMenuOptions(currentSelectedCategory);
            ConfigureMenuButtons();

            menuChanged = true;
        }

        private void btnUpdatePrice_Click(object sender, EventArgs e)
        {
            string itemName = "";
            double itemPrice = -1;
            bool isSuccess = false;

            using (AddToMenu addToMenuForm = new AddToMenu(menuCategoryAndOptions ,currentSelectedOption , true))
            {
                addToMenuForm.TopMost = true;
                var result = addToMenuForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    itemName = addToMenuForm.itemName;
                    itemPrice = addToMenuForm.itemPrice;
                    isSuccess = true;
                }
            }

            if (isSuccess)
            {
                clsDBConnector dBConnector = new clsDBConnector();

                string dmlStr = "UPDATE[Menu Items]" +
                                " SET Name = '" + itemName + "', Cost = " + itemPrice +
                                " WHERE([Menu Items].Name = '" + currentSelectedOption + "')";
                dBConnector.Connect();
                dBConnector.DoDML(dmlStr);
                dBConnector.Close();

                int indexOfSelectedOption = menuCategoryAndOptions[currentSelectedCategory].IndexOf(currentSelectedOption);
                menuCategoryAndOptions[currentSelectedCategory][indexOfSelectedOption] = itemName;

                menuOptionsAndPrices.Remove(currentSelectedOption);
                menuOptionsAndPrices.Add(itemName, itemPrice);

                currentSelectedOption = "";

                DisplayMenuOptions(currentSelectedCategory);
                ConfigureMenuButtons();

                menuChanged = true;
            }            
        }

        private void btnMenuLock_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.menuLocked)
            {
                Properties.Settings.Default.menuLocked = false;
                Properties.Settings.Default.Save();
                btnMenuLock.Text = "Lock Menu";
                pcbMenuLock.Image = Properties.Resources.Unlocked_Icon;
            }
            else
            {
                Properties.Settings.Default.menuLocked = true;
                Properties.Settings.Default.Save();
                btnMenuLock.Text = "Unlock Menu";
                pcbMenuLock.Image = Properties.Resources.Locked_Icon;
            }

            ConfigureMenuButtons();
        }

        private void btnStats_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.Count == 2)
            {
                StatisticsForm statsForm = new StatisticsForm();
                statsForm.Show();
            }                        
        }
    }
}
