﻿namespace Till_Machine
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.flpCurrentOrder = new System.Windows.Forms.FlowLayoutPanel();
            this.flpCategories = new System.Windows.Forms.FlowLayoutPanel();
            this.flpOptions = new System.Windows.Forms.FlowLayoutPanel();
            this.btnOrderComplete = new System.Windows.Forms.Button();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.flpMain = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCategories = new System.Windows.Forms.Label();
            this.lblMenuItems = new System.Windows.Forms.Label();
            this.lblOrderTitle = new System.Windows.Forms.Label();
            this.btnManage = new System.Windows.Forms.Button();
            this.btnCancelUpdate = new System.Windows.Forms.Button();
            this.btnSaveOrder = new System.Windows.Forms.Button();
            this.btnSavedOrders = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.tmrSpecialOffer = new System.Windows.Forms.Timer(this.components);
            this.flpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // flpCurrentOrder
            // 
            this.flpCurrentOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpCurrentOrder.AutoScroll = true;
            this.flpCurrentOrder.BackColor = System.Drawing.SystemColors.Control;
            this.flpCurrentOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpCurrentOrder.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpCurrentOrder.Location = new System.Drawing.Point(1392, 76);
            this.flpCurrentOrder.Name = "flpCurrentOrder";
            this.flpCurrentOrder.Size = new System.Drawing.Size(500, 828);
            this.flpCurrentOrder.TabIndex = 1;
            this.flpCurrentOrder.WrapContents = false;
            // 
            // flpCategories
            // 
            this.flpCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpCategories.AutoSize = true;
            this.flpCategories.BackColor = System.Drawing.SystemColors.Control;
            this.flpCategories.Location = new System.Drawing.Point(3, 51);
            this.flpCategories.Name = "flpCategories";
            this.flpCategories.Size = new System.Drawing.Size(230, 0);
            this.flpCategories.TabIndex = 2;
            // 
            // flpOptions
            // 
            this.flpOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpOptions.AutoSize = true;
            this.flpOptions.BackColor = System.Drawing.SystemColors.Control;
            this.flpOptions.Location = new System.Drawing.Point(3, 105);
            this.flpOptions.MaximumSize = new System.Drawing.Size(1370, 0);
            this.flpOptions.Name = "flpOptions";
            this.flpOptions.Size = new System.Drawing.Size(230, 0);
            this.flpOptions.TabIndex = 3;
            // 
            // btnOrderComplete
            // 
            this.btnOrderComplete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOrderComplete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrderComplete.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderComplete.Location = new System.Drawing.Point(1392, 910);
            this.btnOrderComplete.MaximumSize = new System.Drawing.Size(253, 70);
            this.btnOrderComplete.MinimumSize = new System.Drawing.Size(253, 70);
            this.btnOrderComplete.Name = "btnOrderComplete";
            this.btnOrderComplete.Size = new System.Drawing.Size(253, 70);
            this.btnOrderComplete.TabIndex = 5;
            this.btnOrderComplete.Text = "Complete";
            this.btnOrderComplete.UseVisualStyleBackColor = true;
            this.btnOrderComplete.Click += new System.EventHandler(this.btnOrderComplete_Click);
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalPrice.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPrice.Location = new System.Drawing.Point(1651, 910);
            this.lblTotalPrice.MaximumSize = new System.Drawing.Size(241, 70);
            this.lblTotalPrice.MinimumSize = new System.Drawing.Size(241, 70);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(241, 70);
            this.lblTotalPrice.TabIndex = 6;
            this.lblTotalPrice.Text = "£0.00";
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flpMain
            // 
            this.flpMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpMain.AutoScroll = true;
            this.flpMain.AutoSize = true;
            this.flpMain.BackColor = System.Drawing.SystemColors.Control;
            this.flpMain.Controls.Add(this.lblCategories);
            this.flpMain.Controls.Add(this.flpCategories);
            this.flpMain.Controls.Add(this.lblMenuItems);
            this.flpMain.Controls.Add(this.flpOptions);
            this.flpMain.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpMain.Location = new System.Drawing.Point(13, 76);
            this.flpMain.Name = "flpMain";
            this.flpMain.Size = new System.Drawing.Size(1373, 913);
            this.flpMain.TabIndex = 7;
            this.flpMain.WrapContents = false;
            // 
            // lblCategories
            // 
            this.lblCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategories.Location = new System.Drawing.Point(3, 0);
            this.lblCategories.Name = "lblCategories";
            this.lblCategories.Size = new System.Drawing.Size(230, 48);
            this.lblCategories.TabIndex = 0;
            this.lblCategories.Text = "Categories:";
            // 
            // lblMenuItems
            // 
            this.lblMenuItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMenuItems.Location = new System.Drawing.Point(3, 54);
            this.lblMenuItems.Name = "lblMenuItems";
            this.lblMenuItems.Size = new System.Drawing.Size(230, 48);
            this.lblMenuItems.TabIndex = 3;
            this.lblMenuItems.Text = "Menu Items:";
            // 
            // lblOrderTitle
            // 
            this.lblOrderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOrderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderTitle.Location = new System.Drawing.Point(1392, 9);
            this.lblOrderTitle.Name = "lblOrderTitle";
            this.lblOrderTitle.Size = new System.Drawing.Size(264, 64);
            this.lblOrderTitle.TabIndex = 8;
            this.lblOrderTitle.Text = "New Order";
            this.lblOrderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnManage
            // 
            this.btnManage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManage.Location = new System.Drawing.Point(242, 9);
            this.btnManage.Name = "btnManage";
            this.btnManage.Size = new System.Drawing.Size(220, 57);
            this.btnManage.TabIndex = 9;
            this.btnManage.Text = "Manage Orders";
            this.btnManage.UseVisualStyleBackColor = true;
            this.btnManage.Click += new System.EventHandler(this.btnManage_Click);
            // 
            // btnCancelUpdate
            // 
            this.btnCancelUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnCancelUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelUpdate.Location = new System.Drawing.Point(1780, 13);
            this.btnCancelUpdate.Name = "btnCancelUpdate";
            this.btnCancelUpdate.Size = new System.Drawing.Size(112, 60);
            this.btnCancelUpdate.TabIndex = 10;
            this.btnCancelUpdate.Text = "Cancel";
            this.btnCancelUpdate.UseVisualStyleBackColor = false;
            this.btnCancelUpdate.Click += new System.EventHandler(this.btnCancelUpdate_Click);
            // 
            // btnSaveOrder
            // 
            this.btnSaveOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSaveOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveOrder.Location = new System.Drawing.Point(1662, 13);
            this.btnSaveOrder.Name = "btnSaveOrder";
            this.btnSaveOrder.Size = new System.Drawing.Size(112, 60);
            this.btnSaveOrder.TabIndex = 11;
            this.btnSaveOrder.Text = "Save";
            this.btnSaveOrder.UseVisualStyleBackColor = false;
            this.btnSaveOrder.Click += new System.EventHandler(this.btnSaveOrder_Click);
            // 
            // btnSavedOrders
            // 
            this.btnSavedOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSavedOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavedOrders.Location = new System.Drawing.Point(468, 9);
            this.btnSavedOrders.Name = "btnSavedOrders";
            this.btnSavedOrders.Size = new System.Drawing.Size(220, 57);
            this.btnSavedOrders.TabIndex = 12;
            this.btnSavedOrders.Text = "Saved Orders";
            this.btnSavedOrders.UseVisualStyleBackColor = true;
            this.btnSavedOrders.Click += new System.EventHandler(this.btnSavedOrders_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.Location = new System.Drawing.Point(16, 9);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(220, 57);
            this.btnSettings.TabIndex = 13;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // tmrSpecialOffer
            // 
            this.tmrSpecialOffer.Enabled = true;
            this.tmrSpecialOffer.Interval = 60000;
            this.tmrSpecialOffer.Tick += new System.EventHandler(this.tmrSpecialOffer_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 1001);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnSavedOrders);
            this.Controls.Add(this.btnSaveOrder);
            this.Controls.Add(this.btnCancelUpdate);
            this.Controls.Add(this.btnManage);
            this.Controls.Add(this.lblOrderTitle);
            this.Controls.Add(this.flpMain);
            this.Controls.Add(this.lblTotalPrice);
            this.Controls.Add(this.btnOrderComplete);
            this.Controls.Add(this.flpCurrentOrder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1040);
            this.MinimumSize = new System.Drawing.Size(1380, 720);
            this.Name = "Main";
            this.Text = "Till";
            this.Load += new System.EventHandler(this.Main_Load);
            this.flpMain.ResumeLayout(false);
            this.flpMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flpCurrentOrder;
        private System.Windows.Forms.FlowLayoutPanel flpCategories;
        private System.Windows.Forms.FlowLayoutPanel flpOptions;
        private System.Windows.Forms.Button btnOrderComplete;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.FlowLayoutPanel flpMain;
        private System.Windows.Forms.Label lblOrderTitle;
        private System.Windows.Forms.Button btnManage;
        private System.Windows.Forms.Button btnCancelUpdate;
        private System.Windows.Forms.Button btnSaveOrder;
        private System.Windows.Forms.Button btnSavedOrders;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Label lblCategories;
        private System.Windows.Forms.Label lblMenuItems;
        private System.Windows.Forms.Timer tmrSpecialOffer;
    }
}

