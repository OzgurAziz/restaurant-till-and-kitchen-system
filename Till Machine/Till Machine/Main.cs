﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Till_Machine
{
    public partial class Main : Form
    {
        public Main()
        {
            this.WindowState = FormWindowState.Maximized;
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;

            InitializeComponent();
        }

        //The Dictionary which has the category name as key and a list of menu items as value
        Dictionary<string , List<string>> Categories = new Dictionary<string, List<string>>();

        //The Dictionary which has the menu item as key and it's ID and the price as the value
        Dictionary<string , Tuple<int, double>> MenuItemsAndPrices = new Dictionary< string , Tuple<int ,double>>();

        //The Dictionary which holds the current order's menuItems as key and a list of toppings as value
        Dictionary<Label, List<Label>> currentOrder = new Dictionary<Label, List<Label>>();

        List<SpecialOffer> specialOffers = new List<SpecialOffer>();

        string currentShowingCategory = "";

        public int orderIDtoUpdate = - 1; //Default value is set to -1 showing that we are not updating an order

        int IndexOfSelectedMenuItem = -1; //Default value is set to -1 showing that there is no selected menu item at the moment

        //stores the total price of the current order
        double totalPrice = 0;


        private void Main_Load(object sender, EventArgs e) //**
        {
            CheckIfSpecialOfferExpired();

            //Getting categories and showing them in the UI
            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;
            string sqlStr;
            dbConnector.Connect();
            sqlStr = "SELECT Category" +
                     " FROM Categories" +
                     " WHERE isDeleted = false" +
                     " ORDER BY categoryID";            

            dr = dbConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                string currentCategory = dr[0].ToString();

                //BUTTON
                Button buttonCategory = new Button();
                buttonCategory.Size = new Size((flpMain.Width / 8) - 20, (flpMain.Width / 8) - 20);
                buttonCategory.Margin = new Padding(7, 10, 7, 5);
                buttonCategory.Font = new Font("Microsoft Sans Serif", 26);
                buttonCategory.FlatStyle = FlatStyle.Flat;
                buttonCategory.Tag = currentCategory;
                buttonCategory.Text = currentCategory;
                buttonCategory.Click += ButtonCategory_Click;

                flpCategories.Controls.Add(buttonCategory);

                //CATEGORIES DICTIONARY
                Categories.Add(dr[0].ToString(), null);
            }            

            //Getting menu items of specific categories
            for (int i = 0; i < Categories.Count(); i++)
            {
                sqlStr = "SELECT [Menu Items].Name" +
                         " FROM(Categories INNER JOIN" +
                         " [Menu Items] ON Categories.categoryID = [Menu Items].categoryID)" +
                         " WHERE(Categories.Category = '" + Categories.ElementAt(i).Key + "')" +
                         " AND([Menu Items].isDeleted = false)";

                List<string> MenuItems = new List<string>();
                
                dr = dbConnector.DoSQL(sqlStr);                
                while (dr.Read())
                {                    
                    MenuItems.Add(dr[0].ToString());
                }

                Categories[Categories.ElementAt(i).Key] = MenuItems;
            }

            //Getting the menuitems and prices
            sqlStr = "SELECT menuItemID, Name, Cost" +
                     " FROM[Menu Items]" +
                     " WHERE isDeleted = false";

            dr = dbConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                MenuItemsAndPrices.Add(dr[1].ToString() ,Tuple.Create( Convert.ToInt32(dr[0]) , Convert.ToDouble(dr[2])));
            }                        

            //Special Offers
            sqlStr = "SELECT Name, Cost, startDate, endDate" +
                    " FROM[Menu Items]" +
                    " WHERE(isDeleted = false) AND(startDate IS NOT NULL) AND(endDate IS NOT NULL)";
            dr = dbConnector.DoSQL(sqlStr);

            while (dr.Read())
            {
                specialOffers.Add(new SpecialOffer(dr[0].ToString(), Convert.ToDouble(dr[1]),
                                                   Convert.ToDateTime(dr[2]), Convert.ToDateTime(dr[3])));
            }

            dbConnector.Close();

            EnableOrDisableCompleteButton();
        }        

        private void ButtonCategory_Click(object sender, EventArgs e) //**
        {                        
            if (currentShowingCategory != (sender as Button).Tag.ToString())
            {
                ShowMenuItems((sender as Button).Tag.ToString(), (sender as Button).Size);

                currentShowingCategory = (sender as Button).Tag.ToString();
            }

            if (menuChanged)
            {
                flpOptions.Enabled = false;
            }
            else
            {
                flpOptions.Enabled = true;
            }
        }

        private void ShowMenuItems(string categoryName, Size size) //**
        {
            //Showing the desired menu items (Custom Control)
            flpOptions.Controls.Clear();

            List<string> MenuItems = Categories[categoryName];

            for (int i = 0; i < MenuItems.Count(); i++)
            {
                string currentMenuItem = MenuItems[i];
                SpecialOffer spo = null;
                if (categoryName == "Special Offers")
                {
                    spo = specialOffers.Where(x => x.name == currentMenuItem).First();
                }                

                if (categoryName == "Special Offers" && (DateTime.Now.Date < spo.startDate.Date || DateTime.Now.Date > spo.endDate.Date))
                {
                    //Do nothing
                }
                else
                {                    
                    OptionButton customBtnMenuItem = new OptionButton();
                    customBtnMenuItem.Margin = new Padding(4, 7, 4, 2);
                    Button btnMenuItem = customBtnMenuItem.Controls.Find("btnOption", false).First() as Button;
                    Label lblMenuItem = customBtnMenuItem.Controls.Find("lblPrice", false).First() as Label;
                    //Accent Color
                    Color accentColor = GetColorFromString(Properties.Settings.Default.AccentColor);
                    lblMenuItem.BackColor = accentColor;
                    if (categoryName == "Special Offers")
                    {
                        Label lblDates = customBtnMenuItem.Controls.Find("lblDates", false).First() as Label;                        
                        lblDates.Visible = true;
                        lblDates.Text = spo.startDate.ToString("d") + " - " + spo.endDate.ToString("d");
                        lblDates.BackColor = accentColor;
                        btnMenuItem.Text = MenuItems[i];
                    }
                    else
                    {
                        btnMenuItem.Text = MenuItems[i];
                    }

                    if (MenuItemsAndPrices[MenuItems[i]].Item2.ToString() == "0")
                    {
                        lblMenuItem.Text = "Free";
                    }
                    else
                    {
                        lblMenuItem.Text = "£" + MenuItemsAndPrices[MenuItems[i]].Item2.ToString("N");
                    }
                    btnMenuItem.Size = size;
                    lblMenuItem.Location = new Point(size.Width + 3 - lblMenuItem.Width, 3);
                    btnMenuItem.Tag = categoryName;
                    btnMenuItem.Click += ButtonMenuItem_Click;

                    flpOptions.Controls.Add(customBtnMenuItem);
                }                
            }            
        }

        private void ButtonMenuItem_Click(object sender, EventArgs e) //**
        {
            if ((sender as Button).Tag.ToString() != "Topping")
            {
                CreateOrderItem((sender as Button).Text , 1);                
            }
            else
            {                
                if (IndexOfSelectedMenuItem != -1)
                {
                    AddTopping((sender as Button).Text);                    
                }
            }

            if (savedOrderReloaded == true || orderIDtoUpdate != -1)
            {
                reloadedOrderChanged = true;
            }

            //Updating the PRICE
            UpdatePrice();
            EnableOrDisableCompleteButton();
        }

        private void AddTopping(string toppingName) //**
        {
            int flpOrderItemWidth = currentOrder.Keys.ToList().ElementAt(IndexOfSelectedMenuItem).Parent.Size.Width;

            FlowLayoutPanel flpTopping = new FlowLayoutPanel();
            flpTopping.Parent = currentOrder.Keys.ToList().ElementAt(IndexOfSelectedMenuItem).Parent; //flpOrderItem
            flpTopping.Name = toppingName;
            //flpTopping.BorderStyle = BorderStyle.FixedSingle;
            flpTopping.FlowDirection = FlowDirection.LeftToRight;
            flpTopping.MaximumSize = new Size(flpOrderItemWidth - 10, 0);
            flpTopping.AutoSize = true;
            flpTopping.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            Button deleteButton = new Button();
            deleteButton.Parent = flpTopping;
            deleteButton.UseCompatibleTextRendering = true;
            deleteButton.FlatStyle = FlatStyle.Flat;
            deleteButton.FlatAppearance.MouseOverBackColor = DefaultBackColor;
            deleteButton.Text = "-";
            deleteButton.Font = new Font("Microsoft Sans Serif", 14);
            deleteButton.AutoSize = false;
            deleteButton.Size = new Size(35, 35);
            deleteButton.Click += DeleteButton_ClickTopping;

            Label topping = new Label();
            topping.Name = "topping";
            topping.Parent = flpTopping;
            topping.Text = toppingName;
            topping.TextAlign = ContentAlignment.MiddleCenter;
            topping.Font = new Font("Microsoft Sans Serif", 13);
            topping.AutoSize = true;
            topping.MaximumSize = new Size(flpOrderItemWidth - 60, 0);
            topping.MinimumSize = new Size(flpOrderItemWidth - 60, 0);
            //topping.BackColor = Color.Red;
            topping.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            flpTopping.Controls.Add(deleteButton);
            flpTopping.Controls.Add(topping);

            currentOrder.Keys.ToList().ElementAt(IndexOfSelectedMenuItem).Parent.Controls.Add(flpTopping);

            currentOrder[currentOrder.Keys.ElementAt(IndexOfSelectedMenuItem)].Add(topping); //Back-End
        }

        private void CreateOrderItem(string orderItemName , int quantityToAdd) //**
        {
            FlowLayoutPanel flpOrderItem = new FlowLayoutPanel();
            flpOrderItem.Parent = flpCurrentOrder;
            //flpOrderItem.BorderStyle = BorderStyle.FixedSingle;
            flpOrderItem.FlowDirection = FlowDirection.LeftToRight;
            flpOrderItem.MaximumSize = new Size(flpCurrentOrder.Width - 10, 0);
            flpOrderItem.MinimumSize = new Size(flpCurrentOrder.Width - 10, 0);
            flpOrderItem.AutoSize = true;
            flpOrderItem.Margin = new Padding(5, 5, 0, 5);
            flpOrderItem.Anchor = AnchorStyles.Left | AnchorStyles.Right;

            Button addButton = new Button();
            addButton.UseCompatibleTextRendering = true;
            addButton.FlatStyle = FlatStyle.Flat;
            addButton.FlatAppearance.MouseOverBackColor = DefaultBackColor;
            addButton.Text = "+";
            addButton.Font = new Font("Microsoft Sans Serif", 16);
            addButton.Size = new Size(50, 50);
            addButton.Click += AddButton_Click;

            Label quantity = new Label();            
            quantity.Text = quantityToAdd.ToString();
            quantity.TextAlign = ContentAlignment.MiddleCenter;
            quantity.Height = addButton.Height;
            quantity.Width = 20;
            quantity.Font = new Font("Microsoft Sans Serif", 18);
            quantity.Margin = new Padding(0, 3, 0, 0);

            Button deleteButton = new Button();
            deleteButton.Tag = addButton;
            addButton.Tag = deleteButton;
            deleteButton.UseCompatibleTextRendering = true;
            deleteButton.FlatStyle = FlatStyle.Flat;
            deleteButton.FlatAppearance.MouseOverBackColor = DefaultBackColor;
            deleteButton.Text = "-";
            deleteButton.Font = new Font("Microsoft Sans Serif", 16);
            deleteButton.Size = new Size(50, 50);
            deleteButton.Click += DeleteButton_Click;

            Label itemName = new Label();
            itemName.Name = "itemName";
            itemName.Tag = quantity;
            itemName.Parent = flpOrderItem;
            itemName.Text = orderItemName;
            itemName.TextAlign = ContentAlignment.MiddleCenter;
            itemName.Font = new Font("Microsoft Sans Serif", 24);
            itemName.MaximumSize = new Size(flpCurrentOrder.Width - 40 - deleteButton.Width - addButton.Width - quantity.Width, 0);
            itemName.MinimumSize = new Size(flpCurrentOrder.Width - 40 - deleteButton.Width - addButton.Width - quantity.Width, 0);
            itemName.AutoSize = true;
            itemName.Anchor = AnchorStyles.Top | AnchorStyles.Bottom;
            itemName.Click += ItemName_Click;
            //itemName.BackColor = Color.Red;


            flpOrderItem.Controls.Add(addButton);
            flpOrderItem.Controls.Add(quantity);
            flpOrderItem.Controls.Add(deleteButton);
            flpOrderItem.Controls.Add(itemName);

            flpCurrentOrder.Controls.Add(flpOrderItem);

            currentOrder.Add(itemName, new List<Label>()); //Back-End            
        }

        private void AddButton_Click(object sender, EventArgs e) //**
        {
            int newQuantity = Convert.ToInt32((((sender as Button).Parent.Controls.Find("itemName", false).First() as Label).Tag as Label).Text) + 1;
            if (newQuantity < 9)
            {
                (((sender as Button).Parent.Controls.Find("itemName", false).First() as Label).Tag as Label).Text = newQuantity.ToString();                
            }
            else if (newQuantity == 9)
            {
                (((sender as Button).Parent.Controls.Find("itemName", false).First() as Label).Tag as Label).Text = newQuantity.ToString();

                (sender as Button).Enabled = false;
            }

            UpdatePrice();
            EnableOrDisableCompleteButton();
        }

        private void DeleteButton_Click(object sender, EventArgs e) //**
        {
            if (Convert.ToInt32((((sender as Button).Parent.Controls.Find("itemName", false).First() as Label).Tag as Label).Text) == 1)
            {
                if (IndexOfSelectedMenuItem == flpCurrentOrder.Controls.IndexOf((sender as Button).Parent))
                {
                    IndexOfSelectedMenuItem = -1;
                }
                else if (IndexOfSelectedMenuItem > flpCurrentOrder.Controls.IndexOf((sender as Button).Parent))
                {
                    IndexOfSelectedMenuItem--;
                }

                flpCurrentOrder.Controls.Remove((sender as Button).Parent);

                currentOrder.Remove((sender as Button).Parent.Controls.Find("itemName", false).First() as Label);
            }
            else
            {
                int newQuantity = Convert.ToInt32((((sender as Button).Parent.Controls.Find("itemName", false).First() as Label).Tag as Label).Text) - 1;
                (((sender as Button).Parent.Controls.Find("itemName", false).First() as Label).Tag as Label).Text = newQuantity.ToString();
                ((sender as Button).Tag as Button).Enabled = true;
            }                     

            UpdatePrice();
            EnableOrDisableCompleteButton();
        }

        private void DeleteButton_ClickTopping(object sender, EventArgs e) //**
        {
            List<Label> listOfToppings = currentOrder[(sender as Button).Parent.Parent.Controls.Find("itemName" , false).First() as Label];

            foreach (Label topping in listOfToppings)
            {
                if (topping == ((sender as Button).Parent.Controls.Find("topping" , false).First() as Label))
                {
                    currentOrder[(sender as Button).Parent.Parent.Controls.Find("itemName", false).First() as Label].Remove(topping);
                    break;
                }
            }

            (sender as Button).Parent.Parent.Controls.Remove((sender as Button).Parent);

            UpdatePrice();
            EnableOrDisableCompleteButton();
        }        

        private void UpdatePrice() //**
        {
            totalPrice = 0;

            foreach (var orderItem in currentOrder)
            {
                for (int i = 0; i < Convert.ToInt32((orderItem.Key.Tag as Label).Text); i++)
                {
                    totalPrice = totalPrice + MenuItemsAndPrices[orderItem.Key.Text].Item2; //Item2 gives us the price of the item

                    foreach (var topping in orderItem.Value)
                    {
                        totalPrice = totalPrice + MenuItemsAndPrices[topping.Text].Item2;
                    }
                }                                
            }

            lblTotalPrice.Text = "£" + totalPrice.ToString("N2");
        }

        private void ItemName_Click(object sender, EventArgs e) //**
        {
            //if IndexOfSelectedMenuItem equals the index of the sender's representative order item
            if (IndexOfSelectedMenuItem == currentOrder.Keys.ToList().IndexOf((sender as Label)))
            {                
                IndexOfSelectedMenuItem = -1;
                (sender as Label).BorderStyle = BorderStyle.None;
                (sender as Label).BackColor = DefaultBackColor;
            }
            else if (IndexOfSelectedMenuItem == -1)
            {                
                IndexOfSelectedMenuItem = currentOrder.Keys.ToList().IndexOf((sender as Label));
                (sender as Label).BorderStyle = BorderStyle.FixedSingle;
                (sender as Label).BackColor = ColorTranslator.FromHtml("#A1DAFA");
            }
            else
            {
                currentOrder.Keys.ToList().ElementAt(IndexOfSelectedMenuItem).BorderStyle = BorderStyle.None;
                currentOrder.Keys.ToList().ElementAt(IndexOfSelectedMenuItem).BackColor = DefaultBackColor;

                IndexOfSelectedMenuItem = currentOrder.Keys.ToList().IndexOf((sender as Label));                
                (sender as Label).BorderStyle = BorderStyle.FixedSingle;
                (sender as Label).BackColor = ColorTranslator.FromHtml("#A1DAFA");
            }

            //label1.Text = IndexOfSelectedMenuItem.ToString();
                      
            ShowMenuItems("Topping", flpCategories.Controls[0].Size);
        }       

        private void btnOrderComplete_Click(object sender, EventArgs e) //**
        {
            DateTime now = new DateTime();
            now = DateTime.Now;

            string note = "";
            if (Properties.Settings.Default.askNote == true)
            {
                note = AskForANote();
            }

            clsDBConnector dBConnector = new clsDBConnector();
            if (orderIDtoUpdate == -1 && !savedOrderReloaded)
            {                                
                int currentOrderID = CurrentOrderToDatabase(now, false, note);

                CurrentOrderItemsToDatabase(currentOrderID);
            }
            else if (savedOrderReloaded)
            {
                string cmdStr = "DELETE FROM[Order Items]" +
                                " WHERE([Order Items].orderID = " + orderIDtoUpdate + ")";
                dBConnector.Connect();
                dBConnector.DoDML(cmdStr);
                

                cmdStr = "UPDATE Orders" +
                         " SET Price = " + totalPrice +
                         " , isSaved = false, orderDate = '" + now + "'" +
                         " , [note] = '" + note + "'" +
                         " WHERE(Orders.orderID = " + orderIDtoUpdate + ")";
                dBConnector.DoDML(cmdStr);
                dBConnector.Close();

                CurrentOrderItemsToDatabase(orderIDtoUpdate);
            }
            else
            {
                string cmdStr = "DELETE FROM[Order Items]" +
                                " WHERE([Order Items].orderID = " + orderIDtoUpdate +")";
                dBConnector.Connect();
                dBConnector.DoDML(cmdStr);

                cmdStr = "UPDATE Orders" +
                         " SET Price = " + totalPrice +", isUpdated = true" +
                         " , [note] = '" + note + "'" +
                         " WHERE(Orders.orderID = " + orderIDtoUpdate + ")";
                dBConnector.DoDML(cmdStr);
                dBConnector.Close();

                CurrentOrderItemsToDatabase(orderIDtoUpdate);
            }

            flpCurrentOrder.Controls.Clear();
            currentOrder.Clear();
            lblOrderTitle.Text = "New Order";
            IndexOfSelectedMenuItem = -1;
            savedOrderReloaded = false;
            orderIDtoUpdate = -1;

            UpdatePrice();
            EnableOrDisableCompleteButton();
        }

        private string AskForANote() //**
        {
            string note = "";

            using (UserInputForm userInputForm = new UserInputForm("Note For Kitchen", "The note can only include letters and numbers!", true))
            {
                var result = userInputForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    note = userInputForm.userInput;
                }
            }

            return note;
        }

        private int CurrentOrderToDatabase(DateTime now , bool shouldSave, string note) //** 
        //shouldSave = true if you want to save the order instead of sending it to the kitchen
        {
            clsDBConnector dBConnector = new clsDBConnector();

            string cmdStr = $"INSERT INTO Orders ( Price, orderDate, isComplete, isSaved, [note])" +
                            " VALUES (" + totalPrice.ToString() + " , '" + now + "' , NO, " + shouldSave + ", '" + note + "')";
            dBConnector.Connect();
            dBConnector.DoDML(cmdStr);
            dBConnector.Close();

            //Retrieving the ID automatically created by the database   
            OleDbDataReader dr;
            string sqlStr = "SELECT MAX(orderID)" +
                            " FROM Orders";
            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            int currentOrderID = -1;
            while (dr.Read())
            {
                currentOrderID = Convert.ToInt32(dr[0].ToString());
            }
            dBConnector.Close();

            return currentOrderID;
        }

        private void CurrentOrderItemsToDatabase(int orderID) //**
        {
            clsDBConnector dBConnector = new clsDBConnector();
            dBConnector.Connect();
            foreach (var item in currentOrder)
            {
                string cmdStr = "INSERT INTO [Order Items] (orderID, menuItemID, Quantity)" +
                         " VALUES (" + orderID.ToString() + " , " + MenuItemsAndPrices[item.Key.Text].Item1.ToString() + " , " +
                           Convert.ToInt32((item.Key.Tag as Label).Text) + ")";
                
                dBConnector.DoDML(cmdStr);

                int currentOrderItemID = -1;

                //Retrieving the orderItemID automatically created by the database
                string sqlStr = "SELECT MAX(orderItemID)" +
                         " FROM [Order Items]";
                OleDbDataReader dr = dBConnector.DoSQL(sqlStr);
                while (dr.Read())
                {
                    currentOrderItemID = Convert.ToInt32(dr[0]);
                }

                foreach (var topping in item.Value.ToList())
                {
                    cmdStr = "INSERT INTO Toppings (orderItemID, menuItemID)" +
                             " VALUES (" + currentOrderItemID + " , " + MenuItemsAndPrices[topping.Text].Item1.ToString() + ")";

                    dBConnector.DoDML(cmdStr);
                }
            }
            dBConnector.Close();
        }

        private void btnManage_Click(object sender, EventArgs e) //**
        {
            ManageOrders form = new ManageOrders(this); //I parse in the instance of the current form so I don't need to use static
            if (Application.OpenForms.Count == 1)
            {
                form.Show();
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        bool savedOrderReloaded = false;
        bool reloadedOrderChanged = false;

        private void ChangeOrderTitle(Order orderToUpdate, bool needToBeUpdated) //**
        {
            if (needToBeUpdated)
            {
                lblOrderTitle.Text = "Update Order " + orderToUpdate.flpOrder.Tag;
            }
            else
            {
                lblOrderTitle.Text = orderToUpdate.flpOrder.Tag.ToString();
                savedOrderReloaded = true;
            }
        }

        public void ReloadOrder(Order orderToUpdate, bool needToBeUpdated) //**
        {            
            if (flpCurrentOrder.Controls.Count != 0)
            {
                DialogResult result = MessageBox.Show("There is a not completed order. \nWould you like to save the order for later?", "Uncomplete Order!", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    //gotta check if savedOrderReloaded is true and stuff
                    ChangeOrderTitle(orderToUpdate, needToBeUpdated);
                    SaveUncompleteOrder(currentOrder);
                    flpCurrentOrder.Controls.Clear();
                    currentOrder.Clear();
                    RestoreOrder(orderToUpdate);                   
                    if (needToBeUpdated)
                    {
                        orderIDtoUpdate = orderToUpdate.orderID;
                    }
                }
                else if (result == DialogResult.No)
                {
                    ChangeOrderTitle(orderToUpdate, needToBeUpdated);
                    flpCurrentOrder.Controls.Clear();
                    currentOrder.Clear();
                    RestoreOrder(orderToUpdate);                    
                    if (needToBeUpdated)
                    {
                        orderIDtoUpdate = orderToUpdate.orderID;
                    }
                }
            }
            else
            {
                ChangeOrderTitle(orderToUpdate, needToBeUpdated);
                RestoreOrder(orderToUpdate);                                
            }
            UpdatePrice();
            EnableOrDisableCompleteButton();
        }

        private void SaveUncompleteOrder(Dictionary<Label , List<Label>> order) //**
        {
            DateTime now = new DateTime();
            now = DateTime.Now;
            int currentOrderID = CurrentOrderToDatabase(now, true, "");
            CurrentOrderItemsToDatabase(currentOrderID);
        }

        Order reloadedOrder;

        public void RestoreOrder(Order order) //**
        {
            reloadedOrder = order;
            orderIDtoUpdate = order.orderID;

            for (int i = 0; i < order.items.Count; i++)
            {
                CreateOrderItem(order.items[i].menuItem , order.items[i].quantity);

                if (order.items[i].toppings.Count != 0)
                {
                    IndexOfSelectedMenuItem = i;

                    for (int a = 0; a < order.items[i].toppings.Count; a++)
                    {
                        AddTopping(order.items[i].toppings[a]);
                    }
                }
            }
        }

        private void btnCancelUpdate_Click(object sender, EventArgs e) //**
        {
            if (currentShowingCategory == "Topping")
            {
                flpOptions.Controls.Clear();
                currentShowingCategory = "";
            }            

            ClearTheCurrentOrderContainer();            
        }

        private void ClearTheCurrentOrderContainer() //**
        {
            flpCurrentOrder.Controls.Clear();
            currentOrder.Clear();
            UpdatePrice();
            EnableOrDisableCompleteButton();

            orderIDtoUpdate = -1;
            savedOrderReloaded = false;
            reloadedOrderChanged = false;
            IndexOfSelectedMenuItem = -1;

            reloadedOrder = null;

            lblOrderTitle.Text = "New Order";

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void btnSavedOrders_Click(object sender, EventArgs e) //**
        {
            SavedOrders form = new SavedOrders(this); //I parse in the instance of the current form so I don't need to use static
            if (Application.OpenForms.Count == 1)
            {
                form.Show();
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public Dictionary<int, Tuple<string, double>> GetMenuItemsAndPrices() //**
        {
            Dictionary<int, Tuple<string, double>> result = new Dictionary<int, Tuple<string, double>>();

            clsDBConnector dbConnector = new clsDBConnector();
            OleDbDataReader dr;

            dbConnector.Connect();
            string sqlStr = "SELECT menuItemID, Name, Cost" +
                            " FROM[Menu Items]";

            dr = dbConnector.DoSQL(sqlStr);
            while (dr.Read())
            {
                result.Add(Convert.ToInt32(dr[0]), Tuple.Create(dr[1].ToString(), Convert.ToDouble(dr[2])));
            }
            dbConnector.Close();

            return result;
        }

        private void btnSaveOrder_Click(object sender, EventArgs e) //**
        {
            if (currentOrder.Count != 0)
            {
                if (savedOrderReloaded)
                {
                    if (reloadedOrderChanged)
                    {
                        clsDBConnector dBConnector = new clsDBConnector();

                        string cmdStr = "DELETE FROM[Order Items]" +
                                    " WHERE([Order Items].orderID = " + reloadedOrder.orderID + ")";
                        dBConnector.Connect();
                        dBConnector.DoDML(cmdStr);

                        cmdStr = "UPDATE Orders" +
                                 " SET Price = " + totalPrice +
                                 " WHERE(Orders.orderID = " + orderIDtoUpdate + ")";
                        dBConnector.DoDML(cmdStr);
                        dBConnector.Close();

                        CurrentOrderItemsToDatabase(orderIDtoUpdate);
                    }
                    else
                    {
                        MessageBox.Show("This order is already saved!" , "Warning!");
                    }
                }
                else
                {
                    SaveUncompleteOrder(currentOrder);                    
                }                
            }

            ClearTheCurrentOrderContainer();
        }

        private void btnSettings_Click(object sender, EventArgs e) //**
        {
            SettingsForm newform = new SettingsForm(this);
            if (Application.OpenForms.Count == 1)
            {
                newform.Show();
            }
        }

        bool menuChanged = false; //if true then program needs to be restarted 

        public void MenuChanged(SettingsForm settingsForm) //**
        {
            menuChanged = true;

            btnOrderComplete.Enabled = false;
            btnSaveOrder.Enabled = false;

            MessageBox.Show(settingsForm, "You have made some changes to the menu. \nFor those changes to take place you need to \nrestart the program.", "Warning!");
        }

        private void EnableOrDisableCompleteButton() //**
        {
            if (!menuChanged)
            {
                if (flpCurrentOrder.Controls.Count == 0)
                {
                    btnOrderComplete.Enabled = false;
                    btnSaveOrder.Enabled = false;
                }
                else
                {
                    btnOrderComplete.Enabled = true;
                    btnSaveOrder.Enabled = true;
                }
            }
            else
            {
                btnOrderComplete.Enabled = false;
                btnSaveOrder.Enabled = false;
            }

            if (orderIDtoUpdate != -1)
            {
                btnSaveOrder.Enabled = false;
            }         
        }

        private void tmrSpecialOffer_Tick(object sender, EventArgs e) //**
        {
            if (DateTime.Now.Hour == 0)
            {
                if (DateTime.Now.Minute == 0)
                {
                    CheckIfSpecialOfferExpired();
                }
            }            
 
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void CheckIfSpecialOfferExpired() //**
        {            
            CheckIfSettingsFormIsOpen(); //CLOSE THE SETTINGS FORM IF OPEN AT THE TIME OF CHECKING

            clsDBConnector dBConnector = new clsDBConnector();
            OleDbDataReader dr;

            string sqlStr = "SELECT Name, startDate, endDate" +
                            " FROM[Menu Items]" +
                            " WHERE startDate IS NOT NULL" +
                            " AND endDate IS NOT NULL" +
                            " AND isDeleted = false";
            dBConnector.Connect();
            dr = dBConnector.DoSQL(sqlStr);
            bool specialOfferDeleted = false;
            while (dr.Read())
            {
                if (Convert.ToDateTime(dr[2]) < DateTime.Now.Date)
                {
                    //DELETE IF EXPIRED
                    string dmlStr = "UPDATE [Menu Items]" +
                                    " SET isDeleted = true" +
                                    " WHERE(Name = '" + dr[0].ToString() + "')" +
                                    " AND(startDate IS NOT NULL)" +
                                    " AND(endDate IS NOT NULL)";

                    dBConnector.DoDML(dmlStr);
                    specialOfferDeleted = true;                    
                }
            }

            if (specialOfferDeleted)
            {
                menuChanged = true;
                MessageBox.Show(this, "A Special Offer has expired you need to restart the program \nfor the menu changes to apply.", "Special Offer Expired", MessageBoxButtons.OK);                
            }
        }

        private void CheckIfSettingsFormIsOpen() //**
        {
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Name == "SettingsForm")
                {
                    frm.Close();
                    MessageBox.Show(this, "Checking if a special offer has expired... \nYou can open settings form again right after pressing OK. \n\nThis is done every day at 00:00", "Special Offer Check", MessageBoxButtons.OK);
                    break;
                }
            }
        }

        private Color GetColorFromString(string colorName) //**
        {
            switch (colorName)
            {
                case "Red":
                    return Color.Red;
                case "Dark Red":
                    return Color.DarkRed;
                case "Green":
                    return Color.Green;
                case "Dark Green":
                    return Color.DarkGreen;
                case "Blue":
                    return Color.Blue;
                case "Dark Blue":
                    return Color.DarkBlue;
                case "Yellow":
                    return Color.Yellow;
                case "Purple":
                    return Color.Purple;
                case "Transparent":
                    return Color.Transparent;
                default:
                    return Color.Transparent;
            }
        }
    }
}
