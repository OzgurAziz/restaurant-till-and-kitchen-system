﻿
namespace Till_Machine
{
    partial class OptionButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOption = new System.Windows.Forms.Button();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblDates = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOption
            // 
            this.btnOption.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOption.Location = new System.Drawing.Point(3, 3);
            this.btnOption.Name = "btnOption";
            this.btnOption.Size = new System.Drawing.Size(180, 180);
            this.btnOption.TabIndex = 8;
            this.btnOption.Text = "Option Name";
            this.btnOption.UseVisualStyleBackColor = true;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.Maroon;
            this.lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.Location = new System.Drawing.Point(133, 3);
            this.lblPrice.MaximumSize = new System.Drawing.Size(0, 50);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(50, 24);
            this.lblPrice.TabIndex = 9;
            this.lblPrice.Text = "£999";
            this.lblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDates
            // 
            this.lblDates.BackColor = System.Drawing.Color.Maroon;
            this.lblDates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblDates.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDates.Location = new System.Drawing.Point(0, 162);
            this.lblDates.MaximumSize = new System.Drawing.Size(0, 50);
            this.lblDates.Name = "lblDates";
            this.lblDates.Size = new System.Drawing.Size(186, 24);
            this.lblDates.TabIndex = 10;
            this.lblDates.Text = "11/12/2002 - 11/12/2003";
            this.lblDates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDates.Visible = false;
            // 
            // OptionButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.lblDates);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.btnOption);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "OptionButton";
            this.Size = new System.Drawing.Size(186, 186);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOption;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblDates;
    }
}
