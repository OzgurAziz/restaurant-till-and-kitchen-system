﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Till_Machine
{
    public partial class AddToMenu : Form
    {
        Dictionary<string, List<string>> categoriesAndOptions = new Dictionary<string, List<string>>();        
        bool isAddingCategory = false;
        bool isUpdatingItem = false;
        bool isAddingSpecialOffer = false;
        string errorText = "";
        string menuOptionToUpdate;
        
        public string itemName { get; set; }
        public double itemPrice { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }

        public AddToMenu(Dictionary<string, List<string>> CategoriesAndOptions_, bool isAddingCategory_, bool isAddingSpecialOffer_, string ErrorText_) //if isAddingCategory is false then we are adding an option
        {
            categoriesAndOptions = CategoriesAndOptions_;

            isAddingCategory = isAddingCategory_;
            isAddingSpecialOffer = isAddingSpecialOffer_;

            errorText = ErrorText_;            

            InitializeComponent();
        }        

        public AddToMenu(Dictionary<string, List<string>> CategoriesAndOptions_, string menuOptionToUpdate_ , bool isUpdatingItem_) //This constructor is used for updating the menu item (menu option)
        {
            categoriesAndOptions = CategoriesAndOptions_;

            menuOptionToUpdate = menuOptionToUpdate_;

            isUpdatingItem = isUpdatingItem_;

            errorText = "The item name you have entered already exists!";

            InitializeComponent();
        }


        private void AddToMenu_Load(object sender, EventArgs e)
        {
            dtpStartDate.Value = DateTime.Now.AddDays(1);

            if (isAddingCategory && !isAddingSpecialOffer)
            {
                lblHeader.Text = "Add Menu Category";
                lblPrice.Visible = false;
                txtPrice.Visible = false;
                lblStartDate.Visible = false;
                dtpStartDate.Visible = false;
                lblEndDate.Visible = false;
                dtpEndDate.Visible = false;
            }
            else if (!isAddingCategory && !isAddingSpecialOffer)
            {
                if (isUpdatingItem)
                {
                    lblHeader.Text = "Update Menu Option";
                    txtName.Text = menuOptionToUpdate;
                    lblStartDate.Visible = false;
                    dtpStartDate.Visible = false;
                    lblEndDate.Visible = false;
                    dtpEndDate.Visible = false;
                }
                else
                {
                    lblHeader.Text = "Add Menu Option";
                    lblStartDate.Visible = false;
                    dtpStartDate.Visible = false;
                    lblEndDate.Visible = false;
                    dtpEndDate.Visible = false;
                }                
            }
            else if (isAddingSpecialOffer)
            {
                lblHeader.Text = "Add Special Offer";
            }

            lblErrorText.Text = errorText;

            CheckIfTextBoxesAreFull();
        }

        private void CheckIfTextBoxesAreFull()
        {
            if (isAddingCategory && !isAddingSpecialOffer)
            {
                if (txtName.Text != "")
                {
                    btnSubmit.Enabled = true;
                }
                else
                {
                    btnSubmit.Enabled = false;
                }
            }
            else if(!isAddingCategory && !isAddingSpecialOffer)
            {
                if (txtName.Text != "" && txtPrice.Text != "")
                {
                    btnSubmit.Enabled = true;
                }
                else
                {
                    btnSubmit.Enabled = false;
                }
            }
            else if (isAddingSpecialOffer)
            {
                if (txtName.Text != "" && txtPrice.Text != "" && dtpStartDate.Value.Date < dtpEndDate.Value.Date)
                {
                    btnSubmit.Enabled = true;
                }
                else
                {
                    btnSubmit.Enabled = false;
                }
            }
            
        }

        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (isAddingCategory)
            {
                if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            else
            {
                if (!char.IsLetterOrDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && !char.IsPunctuation(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != '.')
            {                    
                e.Handled = true;
            }                                           
        }

        private void txtBox_TextChanged(object sender, EventArgs args)
        {
            CheckIfTextBoxesAreFull();            
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            bool isOk = true;

            if (isAddingCategory && !isAddingSpecialOffer)
            {
                string categoryName = txtName.Text;
                categoryName = categoryName[0].ToString().ToUpper() + categoryName.Substring(1);

                if (categoriesAndOptions.Keys.Contains(categoryName))
                {
                    isOk = false;
                    lblErrorText.Text = errorText;
                }                 
            }
            else if(!isAddingCategory) //if adding menu item or adding special offer
            {                
                string[] words = txtName.Text.Split(' ');
                string optionName = "";
                foreach (string word in words)
                {
                    optionName += word[0].ToString().ToUpper() + word.Substring(1) + " ";
                }
                optionName = optionName.Remove(optionName.Length - 1, 1);

                if (isUpdatingItem)
                {
                    if (CheckIfMenuItemExists(optionName) && optionName != menuOptionToUpdate)
                    {
                        isOk = false;
                        lblErrorText.Text = errorText;
                    }
                }
                else
                {                    
                    if (CheckIfMenuItemExists(optionName))
                    {
                        isOk = false;
                        lblErrorText.Text = errorText;
                    }
                }                
            }

            //Check if price written is valid
            if (!isAddingCategory)
            {
                try
                {
                    itemPrice = Convert.ToDouble(txtPrice.Text);
                }
                catch (Exception)
                {
                    lblErrorText.Text = "The price you have entered is not valid!";
                    isOk = false;
                }

                if (itemPrice < 0)
                {
                    isOk = false;
                }
            }            

            if (!isOk)
            {                
                lblErrorText.Visible = true;
            }
            else
            {
                this.itemName = txtName.Text;
                if (!isAddingCategory)
                {
                    this.itemPrice = Convert.ToDouble(txtPrice.Text);
                }
                if (isAddingSpecialOffer)
                {
                    this.startDate = Convert.ToDateTime(dtpStartDate.Value);
                    this.endDate = Convert.ToDateTime(dtpEndDate.Value);
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private bool CheckIfMenuItemExists(string menuItem)
        {
            bool itExists = false;

            foreach (List<string> menuItems in categoriesAndOptions.Values)
            {
                if (menuItems.Contains(menuItem))
                {
                    itExists = true;
                    break;
                }                
            }

            return itExists;
        }

        private void dtpStartDate_ValueChanged(object sender, EventArgs e)
        {            
            if (dtpStartDate.Value.Date <= DateTime.Now.Date.AddDays(1))
            {
                dtpStartDate.Value = DateTime.Now.Date.AddDays(1);
            }

            dtpEndDate.Value = dtpStartDate.Value;
        }

        private void dtpEndDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtpEndDate.Value.Date < dtpStartDate.Value.Date)
            {
                dtpEndDate.Value = dtpStartDate.Value;
            }

            CheckIfTextBoxesAreFull();
        }
    }
}
