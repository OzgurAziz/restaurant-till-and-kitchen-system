﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Till_Machine
{
    class SpecialOffer
    {
        public string name { get; set; }
        public double price { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

        public SpecialOffer(string name_, double price_, DateTime startDate_, DateTime endDate_)
        {
            name = name_;

            price = price_;

            startDate = startDate_;

            endDate = endDate_;
        }

    }
}
