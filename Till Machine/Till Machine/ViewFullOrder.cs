﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Till_Machine
{
    public partial class ViewFullOrder : Form
    {
        Order orderToDisplay;

        int orderNumber;

        string savedOrderName = null;

        public ViewFullOrder(Order orderToDisplay_ , int orderNumber_)
        {
            orderToDisplay = orderToDisplay_;

            orderNumber = orderNumber_;

            this.FormClosing += ViewFullOrder_FormClosing;

            InitializeComponent();
        }

        public ViewFullOrder(Order orderToDisplay_, string saveOrderName_)
        {
            orderToDisplay = orderToDisplay_;

            savedOrderName = saveOrderName_;

            this.FormClosing += ViewFullOrder_FormClosing;

            InitializeComponent();
        }

        private void ViewFullOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Form form in Application.OpenForms)
            {
                form.Enabled = true;
            }
        }

        private void ViewFullOrder_Load(object sender, EventArgs e)
        {
            //Disabling other forms
            foreach (Form form in Application.OpenForms)
            {
                if (!(form is ViewFullOrder))
                {
                    form.Enabled = false;
                }
            }

            //Displaying the order
            flpOrder.Name = "flpOrder";
            flpOrder.Tag = orderNumber;
            flpOrder.Margin = new Padding(2, 2, 2, 2);
            flpOrder.MaximumSize = new Size(330, 500);
            flpOrder.ClientSize = new Size(330, 500);
            flpOrder.AutoSize = true;
            flpOrder.FlowDirection = FlowDirection.TopDown;
            flpOrder.WrapContents = false;
            flpOrder.AutoScroll = true;            

            Label lblOrderNumber = new Label();
            lblOrderNumber.Name = "orderNumber";
            if (savedOrderName == null)
            {
                lblOrderNumber.Text = "Order " + orderNumber;
            }
            else
            {
                lblOrderNumber.Text = savedOrderName;
            }
            
            lblOrderNumber.TextAlign = ContentAlignment.MiddleCenter;
            lblOrderNumber.Parent = flpOrder;
            lblOrderNumber.BackColor = Color.Gray;
            lblOrderNumber.Margin = new Padding(2, 2, 2, 2);
            lblOrderNumber.Size = new Size(300, 46);
            lblOrderNumber.Font = new Font("Microsoft Sans Serif", 32);

            flpOrder.Controls.Add(lblOrderNumber);

            for (int a = 0; a < orderToDisplay.items.Count; a++)
            {
                FlowLayoutPanel item = new FlowLayoutPanel();
                item.Name = "orderItems";
                item.Parent = flpOrder;
                item.Margin = new Padding(2, 2, 2, 2);
                item.BackColor = Color.Transparent;
                item.BorderStyle = BorderStyle.FixedSingle;
                item.FlowDirection = FlowDirection.TopDown;
                item.MinimumSize = new Size(300, 0);
                item.MaximumSize = new Size(300, 0);
                item.AutoSize = true;
                item.Anchor = AnchorStyles.Left;

                //Menu Item
                Label line = new Label();
                line.Margin = new Padding(0, 2, 0, 2);
                line.Tag = "OrderItem";
                line.Parent = item;
                line.Text = orderToDisplay.items[a].quantity + "x " + (orderToDisplay.items[a]).menuItem;
                line.Font = new Font("Microsoft Sans Serif", 20);
                line.AutoSize = true;
                line.MinimumSize = new Size(300, 0);

                item.Controls.Add(line);

                //Toppings
                List<string> toppingsList = orderToDisplay.items[a].toppings;
                if (toppingsList.Count != 0)
                {
                    for (int b = 0; b < toppingsList.Count; b++)
                    {
                        Label topping = new Label();
                        topping.Parent = item;
                        topping.Name = "Topping";
                        topping.Margin = new Padding(0, 2, 0, 2);
                        //topping.BackColor = Color.Green;
                        topping.Size = new Size(300, 20);
                        topping.Font = new Font("Microsoft Sans Serif", 12);
                        topping.Text = "+ " + toppingsList[b];

                        item.Controls.Add(topping);
                    }
                }

                flpOrder.Controls.Add(item);
            }           
        }

        private void btnCloseTab_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
