﻿namespace Till_Machine
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.pnlCustomisations = new System.Windows.Forms.Panel();
            this.cmbAccentColor = new System.Windows.Forms.ComboBox();
            this.lblAccentColor = new System.Windows.Forms.Label();
            this.pcbAccentColor = new System.Windows.Forms.PictureBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbAskNote = new System.Windows.Forms.ComboBox();
            this.pcbSavingIcon = new System.Windows.Forms.PictureBox();
            this.lblAskNote = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pcbAskNote = new System.Windows.Forms.PictureBox();
            this.tmrAnimation = new System.Windows.Forms.Timer(this.components);
            this.lblMenu = new System.Windows.Forms.Label();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlSpecialOffers = new System.Windows.Forms.Panel();
            this.lblSpecialOffersInfo = new System.Windows.Forms.Label();
            this.lblSubtextMenu = new System.Windows.Forms.Label();
            this.pcbMenuLock = new System.Windows.Forms.PictureBox();
            this.btnMenuLock = new System.Windows.Forms.Button();
            this.lblWarning = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.btnUpdatePrice = new System.Windows.Forms.Button();
            this.btnDeleteOption = new System.Windows.Forms.Button();
            this.btnDeleteCategory = new System.Windows.Forms.Button();
            this.btnAddOption = new System.Windows.Forms.Button();
            this.btnAddCategory = new System.Windows.Forms.Button();
            this.flpOptions = new System.Windows.Forms.FlowLayoutPanel();
            this.lblOptions = new System.Windows.Forms.Label();
            this.lblCategories = new System.Windows.Forms.Label();
            this.flpCategories = new System.Windows.Forms.FlowLayoutPanel();
            this.btnStats = new System.Windows.Forms.Button();
            this.pnlCustomisations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbAccentColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbSavingIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbAskNote)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.pnlSpecialOffers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMenuLock)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCustomisations
            // 
            this.pnlCustomisations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlCustomisations.Controls.Add(this.cmbAccentColor);
            this.pnlCustomisations.Controls.Add(this.lblAccentColor);
            this.pnlCustomisations.Controls.Add(this.pcbAccentColor);
            this.pnlCustomisations.Controls.Add(this.btnSave);
            this.pnlCustomisations.Controls.Add(this.cmbAskNote);
            this.pnlCustomisations.Controls.Add(this.pcbSavingIcon);
            this.pnlCustomisations.Controls.Add(this.lblAskNote);
            this.pnlCustomisations.Controls.Add(this.label1);
            this.pnlCustomisations.Controls.Add(this.pcbAskNote);
            this.pnlCustomisations.Location = new System.Drawing.Point(559, 64);
            this.pnlCustomisations.Name = "pnlCustomisations";
            this.pnlCustomisations.Size = new System.Drawing.Size(313, 485);
            this.pnlCustomisations.TabIndex = 0;
            // 
            // cmbAccentColor
            // 
            this.cmbAccentColor.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccentColor.DropDownHeight = 100;
            this.cmbAccentColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAccentColor.DropDownWidth = 120;
            this.cmbAccentColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAccentColor.FormattingEnabled = true;
            this.cmbAccentColor.IntegralHeight = false;
            this.cmbAccentColor.ItemHeight = 16;
            this.cmbAccentColor.Items.AddRange(new object[] {
            "Red",
            "Dark Red",
            "Green",
            "Dark Green",
            "Blue",
            "Dark Blue",
            "Yellow",
            "Purple",
            "Transparent"});
            this.cmbAccentColor.Location = new System.Drawing.Point(202, 89);
            this.cmbAccentColor.Name = "cmbAccentColor";
            this.cmbAccentColor.Size = new System.Drawing.Size(96, 24);
            this.cmbAccentColor.TabIndex = 25;
            this.cmbAccentColor.TabStop = false;
            // 
            // lblAccentColor
            // 
            this.lblAccentColor.AutoSize = true;
            this.lblAccentColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccentColor.Location = new System.Drawing.Point(42, 92);
            this.lblAccentColor.Name = "lblAccentColor";
            this.lblAccentColor.Size = new System.Drawing.Size(87, 16);
            this.lblAccentColor.TabIndex = 24;
            this.lblAccentColor.Text = "Accent Color:";
            // 
            // pcbAccentColor
            // 
            this.pcbAccentColor.ErrorImage = global::Till_Machine.Properties.Resources.InfoIcon;
            this.pcbAccentColor.Image = global::Till_Machine.Properties.Resources.InfoIcon;
            this.pcbAccentColor.InitialImage = global::Till_Machine.Properties.Resources.InfoIcon;
            this.pcbAccentColor.Location = new System.Drawing.Point(11, 88);
            this.pcbAccentColor.Name = "pcbAccentColor";
            this.pcbAccentColor.Size = new System.Drawing.Size(25, 25);
            this.pcbAccentColor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbAccentColor.TabIndex = 23;
            this.pcbAccentColor.TabStop = false;
            this.pcbAccentColor.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(59, 432);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(251, 50);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbAskNote
            // 
            this.cmbAskNote.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAskNote.DropDownHeight = 100;
            this.cmbAskNote.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAskNote.DropDownWidth = 120;
            this.cmbAskNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAskNote.FormattingEnabled = true;
            this.cmbAskNote.IntegralHeight = false;
            this.cmbAskNote.ItemHeight = 16;
            this.cmbAskNote.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbAskNote.Location = new System.Drawing.Point(202, 50);
            this.cmbAskNote.Name = "cmbAskNote";
            this.cmbAskNote.Size = new System.Drawing.Size(96, 24);
            this.cmbAskNote.TabIndex = 10;
            this.cmbAskNote.TabStop = false;
            // 
            // pcbSavingIcon
            // 
            this.pcbSavingIcon.Image = ((System.Drawing.Image)(resources.GetObject("pcbSavingIcon.Image")));
            this.pcbSavingIcon.InitialImage = global::Till_Machine.Properties.Resources._1;
            this.pcbSavingIcon.Location = new System.Drawing.Point(5, 432);
            this.pcbSavingIcon.Name = "pcbSavingIcon";
            this.pcbSavingIcon.Size = new System.Drawing.Size(50, 50);
            this.pcbSavingIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbSavingIcon.TabIndex = 22;
            this.pcbSavingIcon.TabStop = false;
            this.pcbSavingIcon.Visible = false;
            // 
            // lblAskNote
            // 
            this.lblAskNote.AutoSize = true;
            this.lblAskNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAskNote.Location = new System.Drawing.Point(42, 53);
            this.lblAskNote.Name = "lblAskNote";
            this.lblAskNote.Size = new System.Drawing.Size(141, 16);
            this.lblAskNote.TabIndex = 9;
            this.lblAskNote.Text = "Note After Completion:";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Customisations";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcbAskNote
            // 
            this.pcbAskNote.ErrorImage = global::Till_Machine.Properties.Resources.InfoIcon;
            this.pcbAskNote.Image = global::Till_Machine.Properties.Resources.InfoIcon;
            this.pcbAskNote.InitialImage = global::Till_Machine.Properties.Resources.InfoIcon;
            this.pcbAskNote.Location = new System.Drawing.Point(11, 49);
            this.pcbAskNote.Name = "pcbAskNote";
            this.pcbAskNote.Size = new System.Drawing.Size(25, 25);
            this.pcbAskNote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbAskNote.TabIndex = 0;
            this.pcbAskNote.TabStop = false;
            this.pcbAskNote.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // tmrAnimation
            // 
            this.tmrAnimation.Interval = 25;
            this.tmrAnimation.Tick += new System.EventHandler(this.tmrAnimation_Tick);
            // 
            // lblMenu
            // 
            this.lblMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMenu.Location = new System.Drawing.Point(0, 0);
            this.lblMenu.Name = "lblMenu";
            this.lblMenu.Size = new System.Drawing.Size(541, 25);
            this.lblMenu.TabIndex = 23;
            this.lblMenu.Text = "Menu";
            this.lblMenu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlMenu.Controls.Add(this.pnlSpecialOffers);
            this.pnlMenu.Controls.Add(this.lblSubtextMenu);
            this.pnlMenu.Controls.Add(this.pcbMenuLock);
            this.pnlMenu.Controls.Add(this.btnMenuLock);
            this.pnlMenu.Controls.Add(this.lblWarning);
            this.pnlMenu.Controls.Add(this.lblPrice);
            this.pnlMenu.Controls.Add(this.btnUpdatePrice);
            this.pnlMenu.Controls.Add(this.btnDeleteOption);
            this.pnlMenu.Controls.Add(this.btnDeleteCategory);
            this.pnlMenu.Controls.Add(this.btnAddOption);
            this.pnlMenu.Controls.Add(this.btnAddCategory);
            this.pnlMenu.Controls.Add(this.flpOptions);
            this.pnlMenu.Controls.Add(this.lblOptions);
            this.pnlMenu.Controls.Add(this.lblCategories);
            this.pnlMenu.Controls.Add(this.flpCategories);
            this.pnlMenu.Controls.Add(this.lblMenu);
            this.pnlMenu.Location = new System.Drawing.Point(12, 12);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(541, 534);
            this.pnlMenu.TabIndex = 24;
            // 
            // pnlSpecialOffers
            // 
            this.pnlSpecialOffers.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlSpecialOffers.Controls.Add(this.lblSpecialOffersInfo);
            this.pnlSpecialOffers.Location = new System.Drawing.Point(376, 381);
            this.pnlSpecialOffers.Name = "pnlSpecialOffers";
            this.pnlSpecialOffers.Size = new System.Drawing.Size(160, 110);
            this.pnlSpecialOffers.TabIndex = 39;
            this.pnlSpecialOffers.Visible = false;
            // 
            // lblSpecialOffersInfo
            // 
            this.lblSpecialOffersInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSpecialOffersInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpecialOffersInfo.ForeColor = System.Drawing.Color.White;
            this.lblSpecialOffersInfo.Location = new System.Drawing.Point(0, 0);
            this.lblSpecialOffersInfo.Name = "lblSpecialOffersInfo";
            this.lblSpecialOffersInfo.Size = new System.Drawing.Size(160, 110);
            this.lblSpecialOffersInfo.TabIndex = 0;
            this.lblSpecialOffersInfo.Text = "Green means that the offer is available right now.\r\nRed means that the offer is n" +
    "ot available right now.";
            // 
            // lblSubtextMenu
            // 
            this.lblSubtextMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSubtextMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtextMenu.Location = new System.Drawing.Point(0, 25);
            this.lblSubtextMenu.Name = "lblSubtextMenu";
            this.lblSubtextMenu.Size = new System.Drawing.Size(541, 21);
            this.lblSubtextMenu.TabIndex = 38;
            this.lblSubtextMenu.Text = "Menu can\'t be changed because there are ongoing or saved orders.";
            this.lblSubtextMenu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSubtextMenu.Visible = false;
            // 
            // pcbMenuLock
            // 
            this.pcbMenuLock.Image = global::Till_Machine.Properties.Resources.Locked_Icon;
            this.pcbMenuLock.InitialImage = global::Till_Machine.Properties.Resources.Locked_Icon;
            this.pcbMenuLock.Location = new System.Drawing.Point(3, 418);
            this.pcbMenuLock.Name = "pcbMenuLock";
            this.pcbMenuLock.Size = new System.Drawing.Size(95, 73);
            this.pcbMenuLock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbMenuLock.TabIndex = 37;
            this.pcbMenuLock.TabStop = false;
            // 
            // btnMenuLock
            // 
            this.btnMenuLock.BackColor = System.Drawing.Color.Gray;
            this.btnMenuLock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenuLock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuLock.Location = new System.Drawing.Point(104, 450);
            this.btnMenuLock.Name = "btnMenuLock";
            this.btnMenuLock.Size = new System.Drawing.Size(130, 33);
            this.btnMenuLock.TabIndex = 35;
            this.btnMenuLock.Text = "Lock Menu";
            this.btnMenuLock.UseVisualStyleBackColor = false;
            this.btnMenuLock.Click += new System.EventHandler(this.btnMenuLock_Click);
            // 
            // lblWarning
            // 
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.Location = new System.Drawing.Point(0, 494);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(541, 40);
            this.lblWarning.TabIndex = 33;
            this.lblWarning.Text = "Need to restart the program for your MENU changes to take effect!";
            this.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPrice
            // 
            this.lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.Location = new System.Drawing.Point(181, 381);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(189, 66);
            this.lblPrice.TabIndex = 32;
            this.lblPrice.Text = "Nothing Selected";
            this.lblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUpdatePrice
            // 
            this.btnUpdatePrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUpdatePrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdatePrice.Location = new System.Drawing.Point(59, 381);
            this.btnUpdatePrice.Name = "btnUpdatePrice";
            this.btnUpdatePrice.Size = new System.Drawing.Size(60, 30);
            this.btnUpdatePrice.TabIndex = 31;
            this.btnUpdatePrice.Text = "Update";
            this.btnUpdatePrice.UseVisualStyleBackColor = false;
            this.btnUpdatePrice.Click += new System.EventHandler(this.btnUpdatePrice_Click);
            // 
            // btnDeleteOption
            // 
            this.btnDeleteOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDeleteOption.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteOption.Location = new System.Drawing.Point(125, 381);
            this.btnDeleteOption.Name = "btnDeleteOption";
            this.btnDeleteOption.Size = new System.Drawing.Size(50, 30);
            this.btnDeleteOption.TabIndex = 30;
            this.btnDeleteOption.Text = "Delete";
            this.btnDeleteOption.UseVisualStyleBackColor = false;
            this.btnDeleteOption.Click += new System.EventHandler(this.btnDeleteOption_Click);
            // 
            // btnDeleteCategory
            // 
            this.btnDeleteCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDeleteCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteCategory.Location = new System.Drawing.Point(59, 185);
            this.btnDeleteCategory.Name = "btnDeleteCategory";
            this.btnDeleteCategory.Size = new System.Drawing.Size(50, 30);
            this.btnDeleteCategory.TabIndex = 29;
            this.btnDeleteCategory.Text = "Delete";
            this.btnDeleteCategory.UseVisualStyleBackColor = false;
            this.btnDeleteCategory.Click += new System.EventHandler(this.btnDeleteCategory_Click);
            // 
            // btnAddOption
            // 
            this.btnAddOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnAddOption.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddOption.Location = new System.Drawing.Point(3, 381);
            this.btnAddOption.Name = "btnAddOption";
            this.btnAddOption.Size = new System.Drawing.Size(50, 30);
            this.btnAddOption.TabIndex = 28;
            this.btnAddOption.Text = "Add";
            this.btnAddOption.UseVisualStyleBackColor = false;
            this.btnAddOption.Click += new System.EventHandler(this.btnAddOption_Click);
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnAddCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCategory.Location = new System.Drawing.Point(3, 185);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(50, 30);
            this.btnAddCategory.TabIndex = 27;
            this.btnAddCategory.Text = "Add";
            this.btnAddCategory.UseVisualStyleBackColor = false;
            this.btnAddCategory.Click += new System.EventHandler(this.btnAddCategory_Click);
            // 
            // flpOptions
            // 
            this.flpOptions.AutoScroll = true;
            this.flpOptions.Location = new System.Drawing.Point(5, 275);
            this.flpOptions.Name = "flpOptions";
            this.flpOptions.Size = new System.Drawing.Size(533, 100);
            this.flpOptions.TabIndex = 25;
            // 
            // lblOptions
            // 
            this.lblOptions.AutoSize = true;
            this.lblOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOptions.Location = new System.Drawing.Point(3, 252);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(68, 20);
            this.lblOptions.TabIndex = 26;
            this.lblOptions.Text = "Options:";
            // 
            // lblCategories
            // 
            this.lblCategories.AutoSize = true;
            this.lblCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategories.Location = new System.Drawing.Point(3, 56);
            this.lblCategories.Name = "lblCategories";
            this.lblCategories.Size = new System.Drawing.Size(90, 20);
            this.lblCategories.TabIndex = 25;
            this.lblCategories.Text = "Categories:";
            // 
            // flpCategories
            // 
            this.flpCategories.AutoScroll = true;
            this.flpCategories.Location = new System.Drawing.Point(3, 79);
            this.flpCategories.Name = "flpCategories";
            this.flpCategories.Size = new System.Drawing.Size(535, 100);
            this.flpCategories.TabIndex = 24;
            // 
            // btnStats
            // 
            this.btnStats.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnStats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStats.Location = new System.Drawing.Point(559, 12);
            this.btnStats.Name = "btnStats";
            this.btnStats.Size = new System.Drawing.Size(313, 46);
            this.btnStats.TabIndex = 25;
            this.btnStats.Text = "Statistics";
            this.btnStats.UseVisualStyleBackColor = false;
            this.btnStats.Click += new System.EventHandler(this.btnStats_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.btnStats);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.pnlCustomisations);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(900, 600);
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.pnlCustomisations.ResumeLayout(false);
            this.pnlCustomisations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbAccentColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbSavingIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbAskNote)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu.PerformLayout();
            this.pnlSpecialOffers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbMenuLock)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCustomisations;
        private System.Windows.Forms.PictureBox pcbAskNote;
        private System.Windows.Forms.ComboBox cmbAskNote;
        private System.Windows.Forms.Label lblAskNote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.PictureBox pcbSavingIcon;
        private System.Windows.Forms.Timer tmrAnimation;
        private System.Windows.Forms.Label lblMenu;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnStats;
        private System.Windows.Forms.Label lblCategories;
        private System.Windows.Forms.FlowLayoutPanel flpCategories;
        private System.Windows.Forms.Button btnDeleteCategory;
        private System.Windows.Forms.Button btnAddOption;
        private System.Windows.Forms.Button btnAddCategory;
        private System.Windows.Forms.FlowLayoutPanel flpOptions;
        private System.Windows.Forms.Label lblOptions;
        private System.Windows.Forms.Button btnDeleteOption;
        private System.Windows.Forms.Button btnUpdatePrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Button btnMenuLock;
        private System.Windows.Forms.PictureBox pcbMenuLock;
        private System.Windows.Forms.Label lblSubtextMenu;
        private System.Windows.Forms.ComboBox cmbAccentColor;
        private System.Windows.Forms.Label lblAccentColor;
        private System.Windows.Forms.PictureBox pcbAccentColor;
        private System.Windows.Forms.Panel pnlSpecialOffers;
        private System.Windows.Forms.Label lblSpecialOffersInfo;
    }
}