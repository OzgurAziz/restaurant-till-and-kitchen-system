﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Till_Machine
{
    public partial class UserInputForm : Form
    {
        string header;
        string errorText;

        public string userInput { get; set; }

        bool allowSpace;


        public UserInputForm(string header_, string errorText_, bool allowSpace_)
        {
            header = header_;
            errorText = errorText_;
            allowSpace = allowSpace_;

            this.FormClosed += UserInputForm_FormClosed;

            InitializeComponent();
        }

        private void UserInputForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (!(form is UserInputForm))
                {
                    form.Enabled = true;
                }
            }
        }

        private void UserInputForm_Load(object sender, EventArgs e)
        {
            this.TopMost = true;

            foreach (Form form in Application.OpenForms)
            {
                if (!(form is UserInputForm))
                {
                    form.Enabled = false;
                }
            }

            lblHeader.Text = header;
            lblErrorText.Text = errorText;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            bool isOk = true;

            foreach (char character in txtInputBox.Text)
            {
                if (!Char.IsNumber(character) && !Char.IsLetter(character))
                {
                    if (allowSpace)
                    {
                        if (!Char.IsWhiteSpace(character))
                        {
                            isOk = false;
                        }                        
                    }
                    else
                    {
                        isOk = false;
                    }
                }
            }

            if (!isOk)
            {
                lblErrorText.Visible = true;
            }
            else
            {
                this.userInput = txtInputBox.Text.Replace("\n", "@!@"); //means new line but writing it like this so c# won't actually make the sql a new line
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
