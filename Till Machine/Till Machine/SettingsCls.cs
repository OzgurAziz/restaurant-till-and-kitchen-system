﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Till_Machine
{
    class SettingsCls
    {
        public bool isSaved = true;

        private bool askNote;
        private string accentColor;
        public bool settingsChanged = false;
        
        public SettingsCls(bool askNote_, string accentColor_)
        {
            askNote = askNote_;

            accentColor = accentColor_;
        }

        public bool AreSettingsSaved()
        {
            if (isSaved)
            {
                return true;
            }
            else
            {
                return false;
            }           
        }      
        
        public void SettingsSaved()
        {
            isSaved = true;
            settingsChanged = false;
        }

        public void SetAskNote(bool askNote_)
        {
            askNote = askNote_;
            isSaved = false;
            settingsChanged = true;
        }

        public void SetAccentColor(string color_)
        {
            accentColor = color_;
            isSaved = false;
            settingsChanged = true;
        }

        public bool GetAskNote()
        {
            return askNote;
        }

        public string GetAccentColor()
        {
            return accentColor;
        }

        //Add getters and setters for settings so I can save them later when the button is clicked
    }
}
