﻿namespace Till_Machine
{
    partial class ViewFullOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewFullOrder));
            this.btnCloseTab = new System.Windows.Forms.Button();
            this.flpOrder = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // btnCloseTab
            // 
            this.btnCloseTab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnCloseTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseTab.Location = new System.Drawing.Point(12, 517);
            this.btnCloseTab.Name = "btnCloseTab";
            this.btnCloseTab.Size = new System.Drawing.Size(310, 48);
            this.btnCloseTab.TabIndex = 0;
            this.btnCloseTab.Text = "Close";
            this.btnCloseTab.UseVisualStyleBackColor = false;
            this.btnCloseTab.Click += new System.EventHandler(this.btnCloseTab_Click);
            // 
            // flpOrder
            // 
            this.flpOrder.Location = new System.Drawing.Point(12, 11);
            this.flpOrder.Name = "flpOrder";
            this.flpOrder.Size = new System.Drawing.Size(310, 500);
            this.flpOrder.TabIndex = 1;
            // 
            // ViewFullOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 577);
            this.Controls.Add(this.flpOrder);
            this.Controls.Add(this.btnCloseTab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(350, 616);
            this.MinimumSize = new System.Drawing.Size(350, 616);
            this.Name = "ViewFullOrder";
            this.Text = "ViewFullOrder";
            this.Load += new System.EventHandler(this.ViewFullOrder_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCloseTab;
        private System.Windows.Forms.FlowLayoutPanel flpOrder;
    }
}