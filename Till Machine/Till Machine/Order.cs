﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Till_Machine
{
    public class Order
    {
        public int orderID { get; }
        public DateTime orderDate { get; }
        public bool isDisabled { get; }
        public bool isUpdated { get; }
        public List<OrderItem> items { get; }
        public FlowLayoutPanel flpOrder { get; set; }

        public Order(int orderID_, DateTime orderDate_, List<OrderItem> items_, bool isDisabled_, bool isUpdated_) //For adding to orderQueue
        {
            orderID = orderID_;

            orderDate = orderDate_;

            items = items_;

            isDisabled = isDisabled_;

            isUpdated = isUpdated_;
        }
    }
}
