﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Till_Machine
{
    public partial class ManageOrders : Form
    {
        Queue<Order> orderQueue = new Queue<Order>();

        int selectedOrderNumber = -1;

        ////The Dictionary which has the ID as key and it's name and the price as the value
        //Dictionary<int, Tuple<string, double>> MenuItemsAndPrices = new Dictionary<int, Tuple<string, double>>();

        Main mainForm { get; }

        OrderViewUI orderViewCls;

        public ManageOrders(Main form)
        {
            mainForm = form;

            InitializeComponent();
        }

        private void ManageOrders_Load(object sender, EventArgs e)
        {                        
            ButtonsEnabled(false, false);

            orderViewCls = new OrderViewUI(mainForm.GetMenuItemsAndPrices(), this, null);        

            InitializeOrderQueue();

            CheckIfEmpty();
        }

        public void Order_Click(object sender, EventArgs e)
        {
            //Getting clicked flow layout panel
            FlowLayoutPanel flpOrder = new FlowLayoutPanel();

            if ((sender as Control).Name != "flpOrder")
            {
                if ((sender as Control).Parent.Name == "flpOrder")
                {
                    flpOrder = (sender as Control).Parent as FlowLayoutPanel;
                }
                else if ((sender as Control).Parent.Parent.Name == "flpOrder")
                {
                    flpOrder = (sender as Control).Parent.Parent as FlowLayoutPanel;
                }
            }
            else
            {
                flpOrder = (sender as FlowLayoutPanel);
            }

            int IndexOfClickedOrder = -1;
            var match = orderQueue.ToList().Where(x => x.flpOrder == flpOrder);
            IndexOfClickedOrder = orderQueue.ToList().IndexOf(match.ToList().First());

            //Determinin which order is selected
            if (selectedOrderNumber == -1)
            {
                flpOrder.BackColor = ColorTranslator.FromHtml("#C2D5FF");
                flpOrder.Controls.Find("orderNumber", false).First().BackColor = ColorTranslator.FromHtml("#6386AD");
                flpOrder.Controls.Find("timerOrder", false).First().BackColor = ColorTranslator.FromHtml("#AAC5F2");

                selectedOrderNumber = IndexOfClickedOrder;

                if (flpOrder.Controls.Count == 5)
                {
                    ButtonsEnabled(true, true);
                }
                else
                {
                    ButtonsEnabled(true, false);
                }
            }
            else if (selectedOrderNumber == IndexOfClickedOrder)
            {
                selectedOrderNumber = -1;

                flpOrder.BackColor = Color.Transparent;
                flpOrder.Controls.Find("orderNumber", false).First().BackColor = Color.Gray;
                flpOrder.Controls.Find("timerOrder", false).First().BackColor = Color.LightGray;

                ButtonsEnabled(false, false);
            }
            else
            {
                Order alreadySelectedOrder = orderQueue.ElementAt(selectedOrderNumber);

                alreadySelectedOrder.flpOrder.BackColor = Color.Transparent;
                alreadySelectedOrder.flpOrder.Controls.Find("orderNumber", false).First().BackColor = Color.Gray;
                alreadySelectedOrder.flpOrder.Controls.Find("timerOrder", false).First().BackColor = Color.LightGray;

                flpOrder.BackColor = ColorTranslator.FromHtml("#C2D5FF");
                flpOrder.Controls.Find("orderNumber", false).First().BackColor = ColorTranslator.FromHtml("#6386AD");
                flpOrder.Controls.Find("timerOrder", false).First().BackColor = ColorTranslator.FromHtml("#AAC5F2");

                selectedOrderNumber = IndexOfClickedOrder;

                if (flpOrder.Controls.Count == 5)
                {
                    ButtonsEnabled(true, true);
                }
                else
                {
                    ButtonsEnabled(true, false);
                }
            }
        }

        private void ButtonsEnabled(bool enabled, bool fullOrderEnabled)
        {
            //fullOrderEnabled is false if the order is already fully shown
            btnCancel.Enabled = enabled;
            btnView.Enabled = fullOrderEnabled;
            btnUpdate.Enabled = enabled;
        }

        private void CheckIfEmpty()
        {
            if (orderQueue.Count == 0)
            {
                lblNoOrdersMessage.Visible = true;
            }
            else
            {
                lblNoOrdersMessage.Visible = false;
            }
        }

        private void InitializeOrderQueue()
        {
            orderQueue = orderViewCls.InitializeOrderQueue(false);

            orderViewCls.UpdateFlowLayoutPanel(flpOrderView);

            DateTime now = new DateTime();
            now = DateTime.Now;

            lblRefreshInfo.Text = "Last Refresh: " + now.TimeOfDay.ToString("hh':'mm':'ss");
        }                          

        private void btnUpdate_Click(object sender, EventArgs e)
        {            
            mainForm.ReloadOrder(orderQueue.ElementAt(selectedOrderNumber), true);
            this.Close();
        }

        private void btnView_Click(object sender, EventArgs e)
        {            
            ViewFullOrder form = new ViewFullOrder(orderQueue.ElementAt(selectedOrderNumber), (int)orderQueue.ElementAt(selectedOrderNumber).flpOrder.Tag);
            if (Application.OpenForms.Count == 2)
            {
                form.Show();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            clsDBConnector dBConnector = new clsDBConnector();
            string dmlStr = "UPDATE Orders" +
                            " SET isDisabled = true" +
                            " WHERE(Orders.orderID = " + orderQueue.ElementAt(selectedOrderNumber).orderID + ")";
            dBConnector.Connect();
            dBConnector.DoDML(dmlStr);
            dBConnector.Close();
        }

        int animationCount = 1;
        private System.Resources.ResourceManager RM = new System.Resources.ResourceManager(typeof(Properties.Resources));

        private void tmrAnimation_Tick(object sender, EventArgs e)
        {
            orderViewCls.AnimationTick(ref animationCount, pcbRefreshIcon, RM, tmrAnimation, bkwComplete);
        }

        private void pcbRefreshIcon_Click(object sender, EventArgs e)
        {
            if (!bkwRefresh.IsBusy)
            {
                flpOrderView.Controls.Clear();
                flpOrderView.Visible = false;

                bkwRefresh.RunWorkerAsync();
                tmrAnimation.Enabled = true;
            }                                  
        }

        bool bkwComplete = false;

        private void bkwRefresh_DoWork(object sender, DoWorkEventArgs e)
        {
            bkwComplete = false;            

            orderViewCls = new OrderViewUI(mainForm.GetMenuItemsAndPrices(), this, null);
            orderQueue = orderViewCls.InitializeOrderQueue(false);            
        }

        private void bkwRefresh_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            orderViewCls.UpdateFlowLayoutPanel(flpOrderView);
            flpOrderView.Visible = true;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            CheckIfEmpty();

            DateTime now = new DateTime();
            now = DateTime.Now;           

            lblRefreshInfo.Text = "Last Refresh: " + now.TimeOfDay.ToString("hh':'mm':'ss");            

            bkwComplete = true;
        }

        private void ManageOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
